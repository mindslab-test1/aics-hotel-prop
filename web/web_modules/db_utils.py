import pymysql

## DB Configuration ##
db_host = '182.162.19.9'
db_port = 32796
db_user = "root"
db_pass = "ejnahc"
db_tabl = 'hoss'

def db_connect(host, port, user, passwd, db):
    return pymysql.connect(host=host, port=port, user=user, passwd=passwd, db=db)


def select_sql(sql_str):
    try:
        db = db_connect(db_host, db_port, db_user, db_pass, db_tabl)
        cursor = db.cursor()
        cursor.execute(sql_str)
        result = cursor.fetchall()
        db.close()
    except:
        db.close()
        return []
    return result


def commit_sql(sql_str):
    try:
        db = db_connect(db_host, db_port, db_user, db_pass, db_tabl)
        cursor = db.cursor()
        cursor.execute(sql_str)
        _ = cursor.fetchall()
        db.commit()
        db.close()
    except:
        db.close()
        return False
    return True