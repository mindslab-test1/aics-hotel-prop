import os
import sys
import pymysql
import datetime
from datetime import timedelta
import calendar
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
from web.web_modules.db_utils import *


def del_float_0(floatstring):
    if (len(floatstring) > 2):
        if (floatstring[-2:] == ".0"):
            return floatstring[:-2]
    return floatstring


def check_id(str_id, str_pw):
    sql = """
                            SELECT *
                            FROM USER
                            WHERE user_id = '%s' AND user_pw = '%s'
                        """ % (str(str_id), str(str_pw))
    result = select_sql(sql)
    return result


def check_id_validation(str_id):
    sql = """
                            SELECT *
                            FROM USER
                            WHERE user_id = '%s'
                        """ % (str(str_id))
    result = select_sql(sql)
    if len(result) != 0 or str(result)=="FALSE":
        return False
    return True


def update_id_date(str_id, str_pw):
    now = datetime.datetime.now()
    try:
        nowDatetime = str(now.strftime('%Y%2m%2d%2H%2M'))
    except:
        nowDatetime = "999999999999"

    sql = """
                              UPDATE USER
                              SET recent_date='%s'
                              WHERE user_id = '%s' AND user_pw = '%s'
                          """ % (nowDatetime, str(str_id), str(str_pw))
    commit_sql(sql)

    return True


def get_all_hotel(user_pk):
    sql = """
                     SELECT *
                     FROM HOTEL
                     WHERE user_pk = '%s'
                         """ % user_pk
    result = select_sql(sql)
    returnstr = ""
    for T in result:
        if (T == result[0]):
            returnstr += ""
        else:
            returnstr += "<SPLIT>"
        returnstr += str(T)[1:-1].replace("'", "")

    return returnstr


def add_hotel(user_pk, hotel_kor_name, hotel_eng_name, hotel_call, hotel_email, hotel_category, hotel_head_name,
              hotel_con1, hotel_con2, hotel_con3, hotel_addr):
    sql = """
                  INSERT INTO HOTEL(user_pk, hotel_kor_name, hotel_eng_name, hotel_call, hotel_email, hotel_category, hotel_head_name, hotel_con1, hotel_con2, hotel_con3, hotel_addr)
                  VALUES(%s,'%s','%s','%s','%s',%s,'%s','%s','%s','%s','%s')
                            """ % (
        user_pk, hotel_kor_name, hotel_eng_name, hotel_call, hotel_email, hotel_category, hotel_head_name,
        hotel_con1,
        hotel_con2, hotel_con3, hotel_addr)
    if not commit_sql(sql) : return "FALSE"
    return "TRUE"

def get_all_rooms(hotel_pk):
    sql = """
                     SELECT *
                     FROM ROOM
                     WHERE hotel_pk = %s
                         """ % hotel_pk
    result = select_sql(sql)
    returnstr = ""
    for T in result:
        if (T == result[0]):
            returnstr += ""
        else:
            returnstr += "<SPLIT>"
        returnstr += str(T)[1:-1].replace("'", "")

    return returnstr


def add_rooms(add_hotel_pk, room_category, room_typename, room_people, room_max_people, room_how_many
                     , room_name, room_names, room_price_sun,room_price_mon,room_price_tue,
                     room_price_wed, room_price_thu, room_price_fri, room_price_sat):
    sql = """
                     INSERT INTO ROOM(hotel_pk, room_category,room_typename, room_people, room_max_people, room_how_many, room_name, room_price_sun,room_price_mon,room_price_tue,
                         room_price_wed, room_price_thu, room_price_fri, room_price_sat)
                     VALUES(%s,%s,'%s',%s,%s,%s,'%s',%s,%s,%s,%s,%s,%s,%s)
                               """ % (
        add_hotel_pk, room_category, room_typename, room_people, room_max_people, room_how_many
        , room_name, room_price_sun, room_price_mon, room_price_tue,
        room_price_wed, room_price_thu, room_price_fri, room_price_sat)

    if not commit_sql(sql): return "FAILED"
    return "SUCCEES"


def add_rule(user_pk, rule_type, rule_name, rule_time_apply, rule_before_day, rule_auto_apply, rule_sun, rule_mon,
             rule_tue, rule_wed, rule_thu, rule_fri, rule_sat, rule_apply_room, rule_uuid, rule_now_apply,
             rule_start_day, rule_end_day):
    sql = """
                  INSERT INTO RULE(user_pk,rule_type, rule_name, rule_time_apply, rule_before_day, rule_auto_apply, rule_sun, rule_mon, rule_tue, rule_wed, rule_thu, rule_fri, rule_sat, rule_apply_room, rule_uuid, rule_now_apply, rule_start_day, rule_end_day)
                  VALUES(%s,%s,'%s',%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,'%s','%s',%s,'%s','%s')
                            """ % (
        user_pk, rule_type, rule_name, rule_time_apply, rule_before_day, rule_auto_apply, rule_sun, rule_mon,
        rule_tue,
        rule_wed, rule_thu, rule_fri, rule_sat, rule_apply_room, rule_uuid, rule_now_apply, rule_start_day,
        rule_end_day)
    if not commit_sql(sql) : return "FAILED"
    return "TRUE"


def uuid_to_rule_pk(rule_uuid):
    sql = """
                                   SELECT *
                                   FROM RULE
                                   WHERE rule_uuid = '%s'
                               """ % (str(rule_uuid))
    result = select_sql(sql)
    try:
        return int(result[0][0])
    except:
        return -1


def uuid_to_OCC_pk(rule_occ_uuid):
    sql = """
                                   SELECT *
                                   FROM RULE_OCC
                                   WHERE rule_occ_uuid = '%s'
                               """ % (str(rule_occ_uuid))
    result = select_sql(sql)
    try:
        return int(result[0][0])
    except:
        return -1


def add_OCC(rule_pk, occ_start, occ_end, occ_val_up, occ_lt_rule, occ_val_percent, occ_val, rule_occ_uuid):
    sql = """
                  INSERT INTO RULE_OCC(rule_pk, occ_start, occ_end, occ_val_up, occ_lt_rule, occ_val_percent, occ_val, rule_occ_uuid)
                  VALUES(%s,%s,%s,%s,%s,%s,%s,'%s')
                            """ % (
        rule_pk, occ_start, occ_end, occ_val_up, occ_lt_rule, occ_val_percent, occ_val, rule_occ_uuid)
    if not commit_sql(sql) : return "FAILED"
    return "TRUE"


def add_LT(occ_pk, lt_start, lt_end, lt_val_up, lt_val_percent, lt_val):
    sql = """
                  INSERT INTO RULE_LT(occ_pk, lt_start, lt_end, lt_val_up, lt_val_percent, lt_val)
                  VALUES(%s,%s,%s,%s,%s,%s)
                            """ % (occ_pk, lt_start, lt_end, lt_val_up, lt_val_percent, lt_val)
    if not commit_sql(sql): return "FAILED"

    return "TRUE"


def get_alarm(user_pk):
    sql = """
                                   SELECT *
                                   FROM ALARM
                                   WHERE user_pk = %s
                               """ % (str(user_pk))
    result = select_sql(sql)
    returnstr = ""
    for T in result:
        if str(T[4]) == '0':
            returnstr += str(T[0]) + "//" + str(T[2]) + "//" + str(T[3]) + "<SPLIT>"
    if (len(returnstr) > 6):
        returnstr = returnstr[:-7]

    return returnstr


def del_alarm_toggle(alarm_pk):
    sql = """
                              UPDATE ALARM
                              SET alarm_deleted=%s
                              WHERE alarm_pk = %s
                          """ % ('true', str(alarm_pk))
    commit_sql(sql)


def get_time_scope_db(user_pk):
    sql = """
                                      SELECT *
                                      FROM TIME_SCOPE
                                      WHERE user_pk = %s
                                  """ % (str(user_pk))
    result = select_sql(sql)

    returnstr = ""
    for T in result:
        for M in T:
            returnstr += str(M) + ","
        returnstr = returnstr[:-1]
        returnstr += "<SPLIT>"
    returnstr = returnstr[:-7]

    return returnstr


def add_time_scope_db(ts_scope_name, user_pk, start_date_year, start_date_month, start_date_day, end_date_year,
                      end_date_month, end_date_day,now_add_room_pk, start_days_db,end_days_db,scope_price_sun,scope_price_mon,scope_price_tue,scope_price_wed,scope_price_thu,scope_price_fri,scope_price_sat):
    sql = """
                                        INSERT INTO TIME_SCOPE(ts_scope_name, user_pk, start_date_year, start_date_month, start_date_day, end_date_year, end_date_month, end_date_day, room_pk,start_days_db,end_days_db,price_sun,price_mon,price_tue,price_wed,price_thu,price_fri,price_sat)
                                        VALUES('%s',%s,%s,%s,%s,%s,%s,%s,%s,'%s','%s',%s,%s,%s,%s,%s,%s,%s)
                                                  """ % (
        ts_scope_name, user_pk, start_date_year, start_date_month, start_date_day, end_date_year, end_date_month,
        end_date_day, now_add_room_pk, start_days_db, end_days_db, scope_price_sun, scope_price_mon,
        scope_price_tue,
        scope_price_wed,
        scope_price_thu, scope_price_fri, scope_price_sat)
    if not (commit_sql(sql)) : return "FAILED"
    return "TRUE"


def del_time_scope_db(ts_key):
    sql = """
                  DELETE FROM TIME_SCOPE
                  WHERE ts_pk='%s'
                            """ % str(ts_key)
    if not commit_sql(sql) : return "FAILED"
    return "TRUE"


def del_room_name_db(room_pk):
    sql = """
                  DELETE FROM ROOM_NAME
                  WHERE room_pk='%s'
                            """ % str(room_pk)
    if not commit_sql(sql) : return "FAILED"
    return "TRUE"


def del_hotel_db(hotel_pk):
    sql = """
                  DELETE FROM HOTEL
                  WHERE hotel_pk='%s'
                            """ % str(hotel_pk)
    if not commit_sql(sql) : return "FAILED"

    sql = """
                                         SELECT *
                                         FROM ROOM
                                         WHERE hotel_pk = %s
                                     """ % (str(hotel_pk))
    result = select_sql(sql)

    for T in result:
        del_room_name_db(T[0])
    sql = """
                     DELETE FROM ROOM
                     WHERE hotel_pk='%s'
                               """ % str(hotel_pk)
    if not commit_sql(sql) : return "FAILED"
    return "TRUE"


def del_room_db(room_pk):
    sql = """
                                         DELETE FROM ROOM
                                         WHERE room_pk = %s
                                     """ % (str(room_pk))
    if not commit_sql(sql) : return "FALSE"
    if del_room_name_db(room_pk) == "FAILED":
        return "FALSE"
    return "SUCCEES"


def modify_get_room_contents_db(check_room_pk):
    sql = """
                                   SELECT *
                                   FROM ROOM
                                   WHERE room_pk = %s
                               """ % (str(check_room_pk))
    result = select_sql(sql)
    try:
        return str(result[0])
    except:
        return "FALSE"


def modify_get_room_name_contents_db(check_room_pk):
    sql = """
                                   SELECT *
                                   FROM ROOM_NAME
                                   WHERE room_pk = %s
                               """ % (str(check_room_pk))
    result = select_sql(sql)
    try:
        return_names = ""
        for T in result:
            return_names += "<RN>" + str(T[0])
            return_names += "<R>" + str(T[2])
        return return_names
    except:
        return "FALSE"


def get_rules_in_rule_config_page_db(user_pk):
    sql = """
                                   SELECT *
                                   FROM RULE
                                   WHERE user_pk = %s
                               """ % (str(user_pk))
    result = select_sql(sql)
    returnresult = ""
    for M in result:
        result_at = M
        returnresult += str(result_at[0]) + "<STRSPLIT>" + str(result_at[16]) + "," + str(result_at[3]) + ","
        if (str(result_at[2]) == "1"):
            returnresult += "점유율,"
        else:
            returnresult += "점유율+리드타임,"
        if (str(result_at[14]) == 'all'):
            returnresult += "<STRSPLIT>all<STRSPLIT>"
        else:
            tmpsplited = str(result_at[14]).split("<SPLIT>")
            returnresult += "<STRSPLIT>"
            for tsl in tmpsplited:
                try:
                    returnresult += tsl + ","
                except:
                    {}
            returnresult = returnresult[:-1] + "<STRSPLIT>"

        condition_text = ""
        if (str(result_at[7]) == "1"):
            condition_text += "일 "
        if (str(result_at[8]) == "1"):
            condition_text += "월 "
        if (str(result_at[9]) == "1"):
            condition_text += "화 "
        if (str(result_at[10]) == "1"):
            condition_text += "수 "
        if (str(result_at[11]) == "1"):
            condition_text += "목 "
        if (str(result_at[12]) == "1"):
            condition_text += "금 "
        if (str(result_at[13]) == "1"):
            condition_text += "토 "
        condition_text += "요일에 <ENTER>"

        condition_text += make_rule_condition(result_at[0])

        if (str(result_at[5]) != "-1"):
            condition_text += "<ENTER>단 체크인 날짜 %s일 전부터 적용하지 않는다" % (str(result_at[5]))
        condition_text += "<ENTER>"

        returnresult += condition_text  # ~요일에, OCC와 LT에 대해 추가 설명 #줄바꿈 <ENTER>
        returnresult += ","

        returnresult += str(result_at[17]) + ","
        returnresult += str(result_at[18])
        returnresult += "<STRSPLIT>"
        if (str(result_at[6]) == "0"):
            returnresult += "자동적용"
        else:
            returnresult += "수동적용"
        returnresult += "<OVERALL>"
    returnresult = returnresult[:-9]

    return returnresult


def del_rules_and_occ_and_lt_db(rule_pk):
    sql = """
                                                DELETE FROM RULE
                                                WHERE rule_pk = %s
                                            """ % (str(rule_pk))
    if not commit_sql(sql) :return "FALSE"

    sql = """
                                      SELECT *
                                      FROM RULE_OCC
                                      WHERE rule_pk = %s
                                  """ % (str(rule_pk))

    result = select_sql(sql)

    del_occ_rule_pk = list()
    for rs in result:
        del_occ_rule_pk.append(str(rs[1]))

    sql = """
                                                 DELETE FROM RULE_OCC
                                                 WHERE rule_pk = %s
                                             """ % (str(rule_pk))
    if not commit_sql(sql) : return "FALSE"
    for dorp in del_occ_rule_pk:
        if not commit_sql(sql) : return "FALSE"
    return "SUCCESS"


def on_rule_db(rule_pk):
    sql = """
                                          UPDATE RULE
                                          SET rule_now_apply=true
                                          WHERE rule_pk='%s'
                                      """ % str(rule_pk)
    if not commit_sql(sql) : return "FALSE"
    return "SUCCESS"


def off_rule_db(rule_pk):
    sql = """
                                          UPDATE RULE
                                          SET rule_now_apply=false
                                          WHERE rule_pk='%s'
                                      """ % str(rule_pk)
    if not commit_sql(sql) : return "FALSE"
    return "SUCCESS"


def make_rule_condition(rule_pk):
    sql = """
                                      SELECT *
                                      FROM RULE_OCC
                                      WHERE rule_pk = %s
                                  """ % (str(rule_pk))
    result = select_sql(sql)

    OCC_pks = list()
    for T in result:
        if (str(T[4]) == "1"):
            updown = "올린다"
        else:
            updown = "내린다"
        if (str(T[6]) == "1"):
            unit = "%"
        else:
            unit = "원"

        OCCtext = "⠀OCC가 %s%%~%s%%일 때 %s%s을 %s" % (str(T[2]), str(T[3]), str(T[7]), unit, updown)
        OCC_pks.append([T[0], OCCtext, []])

    for OP in OCC_pks:
        sql = """
                                              SELECT *
                                              FROM RULE_LT
                                              WHERE occ_pk = %s
                                          """ % (str(OP[0]))
        result_tmp = select_sql(sql)

        for T in result_tmp:
            if (str(T[4]) == "1"):
                updown = "올린다"
            else:
                updown = "내린다"
            if (str(T[5]) == "1"):
                unit = "%"
            else:
                unit = "원"

            LTtext = "⠀⠀LT가 %s일~%s일일 때 %s%s을 %s" % (str(T[2]), str(T[3]), str(T[6]), unit, updown)
            OP[2].append(LTtext)

    returnStr = ""
    for T in OCC_pks:
        returnStr += "⠀<ENTER>" + T[1]
        for M in T[2]:
            returnStr += "⠀<ENTER>" + M
        returnStr += "⠀<ENTER>"
    return returnStr


def modify_room_not_del(del_room_pk,add_hotel_pk, room_category, room_typename, room_people, room_max_people, room_how_many
                     ,   room_name, room_price_sun,room_price_mon,room_price_tue,
                     room_price_wed, room_price_thu, room_price_fri, room_price_sat):
    if str(room_category) == '1':
        room_cat_str = "true"
    else:
        room_cat_str = "false"
    sql = """
                                              UPDATE ROOM
                                              SET hotel_pk=%s,
                                                  room_category=%s,
                                                  room_typename='%s',
                                                  room_people=%s,
                                                  room_max_people=%s,
                                                  room_how_many=%s,
                                                  room_name='%s',
                                                  room_price_sun=%s,
                                                  room_price_mon=%s,
                                                  room_price_tue=%s,
                                                  room_price_wed=%s,
                                                  room_price_thu=%s,
                                                  room_price_fri=%s, 
                                                  room_price_sat=%s
                                              WHERE room_pk=%s
                                          """ % (
        add_hotel_pk, room_cat_str, room_typename, room_people, room_max_people, room_how_many
        , room_name, room_price_sun, room_price_mon, room_price_tue,
        room_price_wed, room_price_thu, room_price_fri, room_price_sat, str(del_room_pk))
    if not commit_sql(sql) : return "FALSE"
    return "SUCCEES"


def add_assign_db(room_pk, start_year, start_month, start_day, assign_person_name, price):
    sql = """
                         INSERT INTO ASSIGN(room_pk, start_year, start_month, start_day, assign_person_name, price)
                         VALUES(%s,%s,%s,%s,'%s',%s)
                                   """ % (
        str(room_pk), str(start_year), str(start_month), str(start_day), str(assign_person_name), str(price))
    if not commit_sql(sql) : return "FALSE"
    return "SUCCEES"


def get_dashboard_vals_compare(user_pk, select_year, select_month):
    if select_month == 1:
        prev_year = select_year - 1
        prev_month = 12
    else:
        prev_year = select_year
        prev_month = select_month - 1

        #  매출(현재값 , 1(up)2(down), 변동폭), 숙박일 (현재값 , 1(up)2(down), 변동폭)
        #  OCC (현재값 , 1(up)2(down), 변동폭), ADR   (현재값 , 1(up)2(down), 변동폭)

    now_val = get_dashboard_vals(user_pk, select_year, select_month)
    prev_val = get_dashboard_vals(user_pk, prev_year, prev_month)
        # [ret_price, ret_days, ret_occ, ret_adr]

    returnstr = ""
    for i in range(4):
        str_now_val = str(now_val[i])
        if (str_now_val[-2:] == ".0"): str_now_val = str_now_val[:-2]
        try:
            if len(str_now_val.split(".")[1]) > 2:
                str_now_val = str_now_val[0:2 - len(str_now_val.split(".")[1])]
        except:
            pass
        str_now_val += ["원", "일", "%", "원"][i]
        returnstr += str_now_val + "//"
        if (now_val[i] > prev_val[i]):
            returnstr += "1//"
        else:
            returnstr += "2//"
        try:
            str_abs_val = str(abs(now_val[i] - prev_val[i]))
        except:
            return "None//None//None//None//None//None//None//None//None//None//None//None//None//None//None//None//None//None//None"
        if str_abs_val[-2:] == ".0": str_abs_val = str_abs_val[:-2]
        try:
            if len(str_abs_val.split(".")[1]) > 2:
                str_abs_val = str_abs_val[: 2 - len(str_abs_val.split(".")[1])]
        except:
            pass
        returnstr += str_abs_val + "//"
    returnstr = returnstr[:-2]
    return returnstr


def get_dashboard_vals(user_pk, select_year, select_month):
    sql = """
                                  SELECT *
                                  FROM HOTEL
                                  WHERE user_pk = %s
                              """ % (str(user_pk))
    now_hotel_pk = select_sql(sql)[0][0]
    sql = """
                                  SELECT *
                                  FROM ROOM
                                  WHERE hotel_pk = %s
                              """ % (str(now_hotel_pk))
    now_rooms = select_sql(sql)

    assign_room_pk_str = ""  # Query에 들어갈 room_pk str
    overall_room_many = 0  # 총 객실 개수
    for nr in now_rooms:
        overall_room_many += int(nr[6])
        assign_room_pk_str += " assign_room_pk = " + str(nr[0]) + " OR "
    assign_room_pk_str = assign_room_pk_str[:-4]

    final_day = calendar.monthrange(int(select_year), int(select_month))[1]
    occ_list = list()

    len_adr = 0

    for i in range(final_day):
        push_element = [0.0, 0, 0, 0]  # OCC계산값, Price 계산값, Assign 개수, Price 총 입력값
        sql = """
                                          SELECT *
                                          FROM ASSIGN
                                          WHERE (%s) AND end_date_db > '%s-%s-%s' AND start_date_db <= '%s-%s-%s'
                                      """ % (
            str(assign_room_pk_str), str(select_year), str(select_month), str(i + 1), str(select_year),
            str(select_month),
            str(i + 1))

        assign_tuple_list = select_sql(sql)

        push_element[2] = len(assign_tuple_list)

        for atl in assign_tuple_list:
            push_element[3] += int(int(atl[5]) / int(atl[11]))
            len_adr += int(atl[11])

        push_element[0] = push_element[2] / overall_room_many * 100
        push_element[1] = push_element[3]

        occ_list.append([push_element[0], push_element[1], push_element[2]])  # OCC계산값, Price 계산값

    # 매출 숙박일 OCC ADR
    ret_price = 0
    ret_days = len_adr
    ret_occ = 0

    for ol_tmp in occ_list:
        ret_price += ol_tmp[1]
        ret_occ += ol_tmp[0]
    ret_occ = len_adr/overall_room_many*100/final_day
    try:
        ret_adr = ret_price / len_adr
    except:
        ret_adr = 0
    ret_price = str(ret_price)
    ret_occ = str(ret_occ)
    ret_adr = str(ret_adr)
    try:
        if (len(ret_price.split(".")[1]) > 2):
            ret_price = ret_price[:-1 * len(ret_price.split(".")[1]) + 2]
    except:
        pass
    try:
        if (len(ret_occ.split(".")[1]) > 2):
            ret_occ = ret_occ[:-1 * len(ret_occ.split(".")[1]) + 2]
    except:
        pass
    try:
        if (len(ret_adr.split(".")[1]) > 2):
            ret_adr = ret_adr[:-1 * len(ret_adr.split(".")[1]) + 2]
    except:
        pass
    try:
        if (ret_price[-2:] == ".0"): ret_price = ret_price[:-2]
    except:
        pass
    try:
        if (ret_occ[-2:] == ".0"): ret_occ = ret_occ[:-2]
    except:
        pass
    try:
        if (ret_adr[-2:] == ".0"): ret_adr = ret_adr[:-2]
    except:
        pass

    ret_price = float(ret_price)
    ret_occ = float(ret_occ)
    ret_adr = float(ret_adr)

    return [ret_price, ret_days, ret_occ, ret_adr]


def dashboard_table_get(user_pk, select_year, select_month):
    returnstr = ""
    second_month = select_month + 1
    if (second_month == 13):
        second_year = select_year + 1
        second_month = 1
    else:
        second_year = select_year
    third_month = second_month + 1
    if (third_month == 13):
        third_year = second_year + 1
        third_month = 1
    else:
        third_year = second_year

    returnstr = str(select_year) + "//" + str(select_month) + "//"
    tmpline = get_dashboard_vals(user_pk, select_year, select_month)
    for M in tmpline:
        returnstr += str(M) + "//"
    returnstr = returnstr[:-2] + "<LINE>"

    returnstr += str(second_year) + "//" + str(second_month) + "//op//op//op//op<LINE>"
    returnstr += str(third_year) + "//" + str(third_month) + "//op//op//op//op<LINE>"

    returnstr += str(select_year - 1) + "//" + str(select_month) + "//"
    tmpline = get_dashboard_vals(user_pk, select_year - 1, select_month)
    for M in tmpline:
        returnstr += str(M) + "//"
    returnstr = returnstr[:-2] + "<LINE>"

    returnstr += str(second_year - 1) + "//" + str(second_month) + "//"
    tmpline = get_dashboard_vals(user_pk, second_year - 1, second_month)
    for M in tmpline:
        returnstr += str(M) + "//"
    returnstr = returnstr[:-2] + "<LINE>"

    returnstr += str(third_year - 1) + "//" + str(third_month) + "//"
    tmpline = get_dashboard_vals(user_pk, third_year - 1, third_month)
    for M in tmpline:
        returnstr += str(M) + "//"
    returnstr = returnstr[:-2] + "<LINE>"

    return returnstr


# 현재 연, 월을 넣고 돌리는 함수
def dashboard_chart_val_get(user_pk, select_year, select_month):
    first_list = []
    first_list.append([select_year, select_month])
    for i in range(14):
        if (first_list[i][1] == 1):
            first_list.append([first_list[i][0] - 1, 12])
        else:
            first_list.append([first_list[i][0], first_list[i][1] - 1])
    first_list.reverse()

    # get_dashboard_vals
    # return [(int(all_assign_val_sum)), int(all_assign_len), int(occ), int(adr)]

    returnlist = []
    for fl in first_list:
        tmp = get_dashboard_vals(user_pk, fl[0], fl[1])
        returnlist.append([fl[0], fl[1], tmp[0], tmp[2]])

    return_str = ""
    for rl in returnlist:
        for M in rl:
            return_str += str(M) + "//"
        return_str = return_str[:-2] + "<LINE>"

    #  연, 월, 매출, OCC

    return_str = return_str[:-6]
    return return_str


def dashboard_table_and_chart_db(user_pk, now_year, now_month):
    return dashboard_table_get(user_pk, now_year, now_month) + "<DASHBOARDSPLIT>" + dashboard_chart_val_get(user_pk,
                                                                                                            now_year,
                                                                                                            now_month)


def calendar_get_hotel_rooms(user_pk):
    returnStr = ""
    sql = """
                                         SELECT *
                                         FROM HOTEL
                                         WHERE user_pk = %s
                                     """ % (str(user_pk))
    result = select_sql(sql)

    for rs in result:
        returnStr += str(rs[2]) + "//"
        sql = """
                                                 SELECT *
                                                 FROM ROOM
                                                 WHERE hotel_pk = %s
                                             """ % (str(rs[0]))
        result_rooms = select_sql(sql)
        for rsr in result_rooms:
            returnStr += str(rsr[0]) + "," + str(rsr[3]) + "//"
        returnStr = returnStr[:-2] + "<SPLIT>"
    returnStr = returnStr[:-7]
    return returnStr


def get_alert_percent_db(user_pk):
    sql = """
                            SELECT *
                            FROM USER
                            WHERE user_pk = %s
                        """ % str(user_pk)
    result = select_sql(sql)

    try:
        return str(result[0][7]) + "," + str(result[0][8]) + "," + str(result[0][9]) + "," + str(
            result[0][10]) + "," + str(result[0][11]) + "," + str(result[0][12])
    except:
        pass
    return "0,0,0,0,0,0"


def set_alert_percent_db(user_pk, occ1, occ2, occ3, occ4, occ5, occ6):
    sql = """
                              UPDATE USER
                              SET occ_1_start=%s, occ_1_end=%s, occ_2_start=%s, occ_2_end=%s, occ_3_start=%s, occ_3_end=%s
                              WHERE user_pk=%s
                          """ % (str(occ1), str(occ2), str(occ3), str(occ4), str(occ5), str(occ6), str(user_pk))
    if not commit_sql(sql) : return "FALSE"
    return "SUCCEES"


def check_hotel_user(user_pk):
    sql = """
                            SELECT *
                            FROM HOTEL
                            WHERE user_pk = %s
                        """ % str(user_pk)
    result = select_sql(sql)
    if (len(result) == 0):
        return "NO_HOTEL"
    else:
        return str(result[0]).replace(",", "<SPLIT>").replace("'", "").replace("> ", ">").replace(" <", "<")[1:-1]


def modify_in_user_hotel(user_pk, hotel_kor_name, hotel_eng_name, hotel_call, hotel_email, hotel_category,
                         hotel_head_name, hotel_con1, hotel_con2, hotel_con3, hotel_addr):
    sql = """
                          UPDATE HOTEL
                          SET hotel_kor_name='%s', hotel_eng_name='%s', hotel_call='%s', hotel_email='%s', hotel_category=%s, hotel_head_name='%s',
                                hotel_con1='%s',
                                hotel_con2='%s', hotel_con3='%s', hotel_addr='%s'
                          WHERE user_pk=%s
                                    """ % (
        hotel_kor_name, hotel_eng_name, hotel_call, hotel_email, hotel_category, hotel_head_name,
        hotel_con1,
        hotel_con2, hotel_con3, hotel_addr, user_pk)
    if not commit_sql(sql) : return "FAILED"
    return "TRUE"


def get_room_names_db(user_pk):
    returnstr = ""
    sql = """
                            SELECT *
                            FROM HOTEL
                            WHERE user_pk = %s
                        """ % str(user_pk)
    result = select_sql(sql)
    if len(result) == 0:
        return "FALSE"

    now_hotel_pk = str(result[0][0]).strip()
    sql = """
                            SELECT *
                            FROM ROOM
                            WHERE hotel_pk = %s
                        """ % str(now_hotel_pk)
    result2 = select_sql(sql)

    for rt2 in result2:
        returnstr += str(rt2[0]) + "//" + str(rt2[3]) + "<SPLIT>"
    returnstr = returnstr[:-7]
    return returnstr


def cat9_add_assign_db(
        assign_room_pk, start_year, start_month, start_day,
        price, assign_person_name, end_year, end_month, end_day,
        channel_created, how_day, created_at
):
    sql = """
                            INSERT INTO ASSIGN(assign_room_pk,start_year,start_month,start_day,
                                               price,assign_person_name,end_year,end_month,end_day,
                                               channel_created,how_day,created_at,
                                               start_date_db, end_date_db, created_date_db)
                            VALUES(%s, %s, %s, %s, %s, '%s', %s, %s, %s, '%s', %s, '%s', '%s-%s-%s', '%s-%s-%s', '%s-%s-%s')
                                      """ % (assign_room_pk, start_year, start_month, start_day,
                                             price, assign_person_name, end_year, end_month, end_day,
                                             channel_created, how_day, created_at,
                                             start_year, start_month, start_day,
                                             end_year, end_month, end_day,
                                             created_at[0:4], created_at[4:6], created_at[6:8]
                                             )
    if not commit_sql(sql) :return "FAILED"
    return "SUCCEES"


def search_assign_db(
        now_user_pk,
        time_pivot, room_pk,
        start_year, start_month, start_day,
        end_year, end_month, end_day,
        search_keyword
):
    final_result = list()
    if (room_pk == "all"):
        sql = """
                                    SELECT *
                                    FROM HOTEL
                                    WHERE user_pk = %s
                                """ % str(now_user_pk)
        result = select_sql(sql)[0]
        now_hotel_pk = str(result[0])

        sql = """
                                    SELECT *
                                    FROM ROOM
                                    WHERE hotel_pk = %s
                                """ % str(now_hotel_pk)
        final_result = select_sql(sql)


    else:
        sql = """
                                               SELECT *
                                               FROM ROOM
                                               WHERE room_pk = %s
                                           """ % str(room_pk)
        final_result = select_sql(sql)

    if (time_pivot == "0"):
        pivot_str = "start_date_db"
    elif (time_pivot == "1"):
        pivot_str = "end_date_db"
    else:
        pivot_str = "created_date_db"

    after_assign_list = list()
    for fr in final_result:
        sql = """
                                    SELECT *
                                    FROM ASSIGN
                                    WHERE assign_room_pk = %s AND %s >= '%s-%s-%s' AND %s <= '%s-%s-%s'
                                """ % (
            str(fr[0]), pivot_str, start_year, start_month, start_day, pivot_str, end_year, end_month, end_day)
        overall_ended_tuple = select_sql(sql)
        for oet in overall_ended_tuple:
            oetmp = list()
            for oet2 in oet:
                oetmp.append(oet2)
            after_assign_list.append(oetmp)

    for aal in after_assign_list:
        sql = """
                                            SELECT *
                                            FROM ROOM
                                            WHERE room_pk = %s
                                        """ % (str(aal[1]))
        aal[1] = select_sql(sql)[0][3]

    returnstr = ""
    for aal in after_assign_list:
        returnstr += str(aal[0]) + "//" + str(aal[10]) + "//" + str(aal[6]) + "//" + str(aal[1]) + "//" + str(
            aal[13]) + "//" + str(aal[14]) + "//" + str(aal[5]) + "//" + str(aal[15]) + "<SPLIT>"
    returnstr = returnstr[:-7]
    return returnstr


def del_assign_in_table_db(del_assign_pk):
    sql = """
                     DELETE FROM ASSIGN
                     WHERE assign_pk='%s'
                               """ % str(del_assign_pk)
    if not commit_sql(sql) : return "FAILED"
    return "TRUE"


def get_assign_in_modify_on(assign_pk):
    sql = """
                            SELECT *
                            FROM ASSIGN
                            WHERE assign_pk=%s
                        """ % (str(assign_pk))
    oet = select_sql(sql)[0]

    returnstr = str(oet[2]) + "//" + str(oet[3]) + "//" + str(oet[4]) + "//" + str(oet[7]) + "//" + str(
        oet[8]) + "//" + str(oet[9]) + "//" + str(oet[6]) + "//" + str(oet[10]) + "//" + str(oet[5])
    return returnstr


def cat9_modify_assign_db(
        now_modify_assign_pk,
        start_year, start_month, start_day,
        price, assign_person_name, end_year, end_month, end_day,
        channel_created, how_day, created_at
):
    sql = """
                              UPDATE ASSIGN
                              SET start_year = %s, start_month = %s, start_day = %s,
                                    price = %s, assign_person_name = '%s', end_year = %s, end_month = %s, end_day = %s,
                                    channel_created = '%s', how_day = %s, created_at = '%s',
                                    start_date_db ='%s-%s-%s', end_date_db = '%s-%s-%s'
                              WHERE assign_pk=%s
                                        """ % (
        start_year, start_month, start_day,
        price, assign_person_name, end_year, end_month, end_day,
        channel_created, how_day, created_at,
        start_year, start_month, start_day,
        end_year, end_month, end_day
        , now_modify_assign_pk)

    if not commit_sql(sql) : return "FAILED"
    return "TRUE"


def add_user_in_dbdb(user_id, user_pw, user_name, tel_num, hotel_name, marketing):
    if not (check_id_validation(user_id)):
        return "VAL NOT"

    now = datetime.datetime.now()
    try:
        nowDatetime = str(now.strftime('%Y%2m%2d%2H%2M'))
    except:
        nowDatetime = "999999999999"

    sql = """
                                    INSERT INTO USER(user_id,user_pw,user_name,tel_num,hotel_name,marketing,status,start_date)
                                    VALUES('%s','%s','%s','%s','%s',%s,1,'%s')
                                              """ % (
        user_id, user_pw, user_name, tel_num, hotel_name, marketing, nowDatetime)
    if not commit_sql(sql) : return "FALSE"
    return "True"


def getAllUserDB():
    sql = """
                            SELECT *
                            FROM USER
                        """
    all_result = select_sql(sql)
    returnstr = ""

    for i in all_result:
        returnstr += "%s//%s//%s//%s//%s//%s//%s//%s<SPLIT>" % (
            str(i[0]), str(i[1]), str(i[3]), str(i[13]), str(i[14]), str(i[15]),
            str(i[4]), str(i[6])
        )  # pk, id, name, tel_num, hotel_name, marketing, start_date, status
    returnstr = returnstr[:-7]
    return returnstr


def admin_del_user_dbdb(del_user_pk):
    sql = """
                                      UPDATE USER
                                      SET status = 99
                                      WHERE user_pk = %s
                                                """ % (str(del_user_pk))
    if not commit_sql(sql) : return "FALSE"
    return "TRUE"


def admin_check_user_dbdb(check_user_pk):
    sql = """
                                      UPDATE USER
                                      SET status = 0
                                      WHERE user_pk = %s
                                                """ % (str(check_user_pk))
    if not commit_sql(sql) : return "FALSE"
    return "TRUE"


def search_all_assign_db(now_user_pk):
    final_result = list()
    sql = """
                                SELECT *
                                FROM HOTEL
                                WHERE user_pk = %s
                            """ % str(now_user_pk)
    result = select_sql(sql)
    if len(result) == 0:
        return "FALSE"

    now_hotel_pk = str(result[0][0]).strip()
    room_list = list()
    sql = """
                                SELECT *
                                FROM ROOM
                                WHERE hotel_pk = %s
                            """ % str(now_hotel_pk)
    result = select_sql(sql)
    for rs in result:
        tmplist = list()
        for rss in rs:
            tmplist.append(rss)
        room_list.append(tmplist)

    after_assign_list = list()
    for fr in room_list:
        sql = """
                                        SELECT *
                                        FROM ASSIGN
                                        WHERE assign_room_pk = %s
                                    """ % (str(fr[0]))
        overall_ended_tuple = select_sql(sql)
        for oet in overall_ended_tuple:
            tmptmplist = list()
            for oett in oet:
                tmptmplist.append(oett)
            after_assign_list.append(tmptmplist)

    for afl in after_assign_list:
        sql = """
                                               SELECT *
                                               FROM ROOM
                                               WHERE room_pk = %s
                                           """ % (str(afl[1]))
        tmp_room = select_sql(sql)[0]
        afl[1] = str(tmp_room[3])

    returnstr = ""
    for aal in after_assign_list:
        returnstr += str(aal[0]) + "//" + str(aal[10]) + "//" + str(aal[6]) + "//" + str(aal[1]) + "//" + str(
            aal[13]) + "//" + str(aal[14]) + "//" + str(aal[5]) + "//" + str(aal[15]) + "<SPLIT>"
    returnstr = returnstr[:-7]
    return returnstr


def check_rule_with_rulepk(rule_pk):
    sql = """
                                   SELECT *
                                   FROM RULE
                                   WHERE rule_pk = %s
                               """ % (str(rule_pk))
    result = select_sql(sql)[0]
    returnstr = str(result[0]) + "//" + str(result[1]) + "//" + str(result[2]) + "//" + \
                str(result[3]) + "//" + str(result[4]) + "//" + str(result[5]) + "//" + \
                str(result[6]) + "//" + str(result[7]) + "//" + str(result[8]) + "//" + \
                str(result[9]) + "//" + str(result[10]) + "//" + str(result[11]) + "//" + \
                str(result[12]) + "//" + str(result[13]) + "//" + str(result[14]) + "//" + \
                str(result[15]) + "//" + str(result[16]) + "//" + str(result[17]) + "//" + \
                str(result[18]) + "<SUPER>"

    sql = """
                                   SELECT *
                                   FROM RULE_OCC
                                   WHERE rule_pk = %s
                               """ % (str(rule_pk))
    occ_result = select_sql(sql)

    for orr in occ_result:
        returnstr += "<OCC>" + del_float_0(str(orr[0])) + "//" \
                     + del_float_0(str(orr[1])) + "//" \
                     + del_float_0(str(orr[2])) + "//" + del_float_0(str(orr[3])) + "//" \
                     + del_float_0(str(orr[4])) + "//" + del_float_0(str(orr[5])) + "//" \
                     + del_float_0(str(orr[6])) + "//" + del_float_0(str(orr[7]))
        sql = """
                                           SELECT *
                                           FROM RULE_LT
                                           WHERE occ_pk = %s
                                       """ % (str(orr[0]))
        lt_result = select_sql(sql)

        for ltt in lt_result:
            returnstr += "<LT>" + del_float_0(str(ltt[0])) + "//" + del_float_0(str(ltt[1])) + "//" \
                         + del_float_0(str(ltt[2])) + "//" + del_float_0(str(ltt[3])) + "//" \
                         + del_float_0(str(ltt[4])) + "//" + del_float_0(str(ltt[5])) + "//" \
                         + del_float_0(str(ltt[6]))
    return returnstr


def modif_rule(now_modify_rule_pk, user_pk, rule_type, rule_name, rule_time_apply, rule_before_day, rule_auto_apply,
               rule_sun,
               rule_mon, rule_tue,
               rule_wed, rule_thu, rule_fri, rule_sat, rule_apply_room, rule_uuid, rule_now_apply, rule_start_day,
               rule_end_day):
    sql = """
                                  UPDATE RULE
                                  SET user_pk=%s, rule_type=%s, rule_name='%s', rule_time_apply=%s, rule_before_day=%s, rule_auto_apply=%s, rule_sun=%s,
                          rule_mon=%s, rule_tue=%s,
                          rule_wed=%s, rule_thu=%s, rule_fri=%s, rule_sat=%s, rule_apply_room='%s', rule_uuid='%s', rule_now_apply=%s, rule_start_day='%s',
                          rule_end_day='%s'
                                  WHERE rule_pk=%s
                                            """ % (
        user_pk, rule_type, rule_name, rule_time_apply, rule_before_day, rule_auto_apply, rule_sun,
        rule_mon, rule_tue,
        rule_wed, rule_thu, rule_fri, rule_sat, rule_apply_room, rule_uuid, rule_now_apply, rule_start_day,
        rule_end_day, now_modify_rule_pk)
    if not commit_sql(sql) : return "FAILED"
    return "TRUE"


def del_occ_and_lt(now_modify_rule_pk):
    sql = """
                               SELECT *
                               FROM RULE_OCC
                               WHERE rule_pk = %s
                           """ % (str(now_modify_rule_pk))
    result =  select_sql(sql)

    for rocc in result:
        sql = """
                          DELETE FROM RULE_LT
                          WHERE occ_pk=%s
                                    """ % str(rocc[0])
        if not commit_sql(sql) : return "FAILED"

    sql = """
                         DELETE FROM RULE_OCC
                         WHERE rule_pk = %s
                                   """ % (str(now_modify_rule_pk))
    if not commit_sql(sql): return "FAILED"
    return "TRUE"


def calendar_get_occ_overall(now_user_pk, now_year, now_month):
    sql = """
                                  SELECT *
                                  FROM HOTEL
                                  WHERE user_pk = %s
                              """ % (str(now_user_pk))
    now_hotel_pk = select_sql(sql)[0][0]
    sql = """
                                  SELECT *
                                  FROM ROOM
                                  WHERE hotel_pk = %s
                              """ % (str(now_hotel_pk))
    now_rooms = select_sql(sql)

    assign_room_pk_str = ""  # Query에 들어갈 room_pk str
    overall_room_many = 0  # 총 객실 개수
    for nr in now_rooms:
        overall_room_many += int(nr[6])
        assign_room_pk_str += " assign_room_pk = " + str(nr[0]) + " OR "
    assign_room_pk_str = assign_room_pk_str[:-4]

    final_day = calendar.monthrange(int(now_year), int(now_month))[1]
    occ_list = list()
    for i in range(final_day):
        push_element = [0.0, 0, 0, 0]  # OCC계산값, Price 계산값, Assign 개수, Price 총 입력값
        sql = """
                                          SELECT *
                                          FROM ASSIGN
                                          WHERE (%s) AND end_date_db > '%s-%s-%s' AND start_date_db <= '%s-%s-%s'
                                      """ % (
            str(assign_room_pk_str), str(now_year), str(now_month), str(i + 1), str(now_year), str(now_month),
            str(i + 1))
        assign_tuple_list = select_sql(sql)

        push_element[2] = len(assign_tuple_list)

        for atl in assign_tuple_list:
            push_element[3] += int(int(atl[5]) / int(atl[11]))

        push_element[0] = push_element[2] / overall_room_many * 100
        push_element[1] = push_element[3]

        occ_list.append([push_element[0], push_element[1]])  # OCC계산값, Price 계산값, Price 총 입력값

    rss = ""
    for ol in occ_list:
        returnocc = str(ol[0])
        if (len(returnocc.split(".")[1]) > 2):
            returnocc = returnocc[:2 - len(returnocc.split(".")[1])]
        rss += str(returnocc) + "//" + str(ol[1]) + "<SPLIT>"
    rss = rss[:-7]

    return rss


def save_min_max_price_db(param_list):
    for pl in param_list:
        sql = """
                                      UPDATE ROOM
                                      SET room_min_price='%s', room_max_price='%s'
                                      WHERE room_pk=%s
                                  """ % (str(pl[1]), str(pl[2]), str(pl[0]))
        if not commit_sql(sql) : return "FALSE"
    return "TRUE"


def calendar_get_occ_overall_typename(now_user_pk, now_year, now_month, room_pk):
    sql = """
                     SELECT *
                     FROM ROOM
                     WHERE room_pk = %s
                         """ % str(room_pk)
    result = select_sql(sql)
    overall_room_many = int(result[0][6])


    final_day = calendar.monthrange(int(now_year), int(now_month))[1]
    occ_list = list()
    for i in range(final_day):
        push_element = [0.0, 0, 0, 0]  # OCC계산값, Price 계산값, Assign 개수, Price 총 입력값
        sql = """
                                          SELECT *
                                          FROM ASSIGN
                                          WHERE assign_room_pk = %s AND end_date_db > '%s-%s-%s' AND start_date_db <= '%s-%s-%s'
                                      """ % (
            str(room_pk), str(now_year), str(now_month), str(i + 1), str(now_year), str(now_month),
            str(i + 1))
        assign_tuple_list = select_sql(sql)

        push_element[2] = len(assign_tuple_list)

        for atl in assign_tuple_list:
            push_element[3] += int(int(atl[5]) / int(atl[11]))

        push_element[0] = push_element[2] / overall_room_many * 100
        push_element[1] = push_element[3]

        occ_list.append([push_element[0], push_element[1]])  # OCC계산값, Price 계산값, Price 총 입력값

    rss = ""
    for ol in occ_list:
        returnocc = str(ol[0])
        if (len(returnocc.split(".")[1]) > 2):
            returnocc = returnocc[:2 - len(returnocc.split(".")[1])]
        rss += str(returnocc) + "//" + str(ol[1]) + "<SPLIT>"
    rss = rss[:-7]

    # rss -> 0.0 // 0 < SPLIT > 0.0 // 0 < SPLIT > 0.0 // 0 < SPLIT > ....... < SPLIT > 0.0 // 0
    return rss


def calendar_get_occ_overall_typename_real_calculate(now_user_pk, now_year, now_month, room_pk):
    final_day = calendar.monthrange(int(now_year), int(now_month))[1]
    rss = ""
    for i in range(final_day):
        rss += calculate_price_and_occ(now_user_pk, room_pk, now_year, now_month, i+1) + "<SPLIT>"
    rss = rss[:-7]
    # rss -> 0.0 // 0 < SPLIT > 0.0 // 0 < SPLIT > 0.0 // 0 < SPLIT > ....... < SPLIT > 0.0 // 0
    return rss


def scopePageOnDB(user_pk):
    returnstr = ""
    sql = """
                            SELECT *
                            FROM HOTEL
                            WHERE user_pk = %s
                        """ % str(user_pk)
    result = select_sql(sql)
    if len(result) == 0:
        return "FALSE"

    now_hotel_pk = str(result[0][0]).strip()
    sql = """
                            SELECT *
                            FROM ROOM
                            WHERE hotel_pk = %s
                        """ % str(now_hotel_pk)
    result2 = select_sql(sql)

    for rt2 in result2:
        returnstr += str(rt2[0]) + "//" + str(rt2[3]) + "<SPLIT>"
    returnstr = returnstr[:-7]
    return returnstr


def modif_screen_on_db(modif_scope_pk):
    sql = """
                               SELECT *
                               FROM TIME_SCOPE
                               WHERE ts_pk = %s
                           """ % str(modif_scope_pk)
    result = select_sql(sql)
    if len(result) == 0:
        return "FALSE"
    returnstr = ""
    for rss in result[0]:
        try:
            if(str(rss)[-2:]==".0"):
                returnstr += str(rss)[:-2] + "//"
            else:
                returnstr += str(rss) + "//"
        except:
            returnstr += str(rss) + "//"
    return returnstr[:-2]


def mod_time_scope_db(ts_scope_name, user_pk, start_date_year, start_date_month, start_date_day, end_date_year,
                      end_date_month, end_date_day,now_add_room_pk, start_days_db,end_days_db,scope_price_sun,scope_price_mon,scope_price_tue,scope_price_wed,scope_price_thu,scope_price_fri,scope_price_sat,modif_scope_pk):
    sql = """
                              UPDATE TIME_SCOPE
                              SET ts_scope_name='%s', user_pk=%s, start_date_year=%s, start_date_month=%s, start_date_day=%s, end_date_year=%s,
                          end_date_month=%s, end_date_day=%s,room_pk=%s, start_days_db='%s',end_days_db='%s',price_sun=%s,price_mon=%s,price_tue=%s,price_wed=%s,price_thu=%s,price_fri=%s,price_sat=%s
                              WHERE ts_pk = %s
                          """ % (
        ts_scope_name, user_pk, start_date_year, start_date_month, start_date_day, end_date_year,
        end_date_month, end_date_day, now_add_room_pk, start_days_db, end_days_db, scope_price_sun, scope_price_mon,
        scope_price_tue, scope_price_wed, scope_price_thu, scope_price_fri, scope_price_sat, modif_scope_pk
    )
    if not commit_sql(sql) : return "FALSE"
    return "TRUE"




def calculate_price_and_occ(user_pk, room_pk, selected_year, selected_month, selected_day_param):
    sql = """
                            SELECT *
                            FROM ROOM
                            WHERE room_pk = %s
                        """ % str(room_pk)
    room_result = select_sql(sql)
    now_room_how_many = int(room_result[0][6])
    sql = """
                              SELECT *
                              FROM ASSIGN
                              WHERE assign_room_pk = %s AND start_date_db<='%s-%s-%s' AND end_date_db>'%s-%s-%s'
                          """ % (
    str(room_pk), str(selected_year), str(selected_month), str(selected_day_param), str(selected_year),
    str(selected_month), str(selected_day_param))
    assign_result = select_sql(sql)

    assign_how_many = len(assign_result)
    print("assign how many = " + str(assign_how_many))

    if(assign_how_many==0): occ_val = 0
    else : occ_val = assign_how_many / now_room_how_many * 100
    # OCC -> Percentageselected_day_param
    today_dat = datetime.date.today()
    selected_day = datetime.date(int(selected_year), int(selected_month), int(selected_day_param))
    lead_time = int((selected_day - today_dat).days)
    days = ["mon","tue","wed","thu","fri","sat","sun"][selected_day.weekday()]
    days_index = (selected_day.weekday()+1) % 7 # 일0 월1 화2 수3 목4 금5 토6

    now_days_value = float(room_result[0][11+days_index])

    sql = """
                              SELECT *
                              FROM TIME_SCOPE
                              WHERE room_pk = %s AND start_days_db<='%s-%s-%s' AND end_days_db>'%s-%s-%s'
                          """ % (
    str(room_pk), str(selected_year), str(selected_month), str(selected_day_param), str(selected_year),
    str(selected_month), str(selected_day_param))
    scope_result = select_sql(sql)

    for sr in scope_result:
        now_days_value = float(sr[12+days_index])

    sql = """
                              SELECT *
                              FROM RULE
                              WHERE user_pk = %s
                          """ % (str(user_pk))
    rule_result = select_sql(sql)
    new_rule_list = list()
    for rr in rule_result:
        if(str(rr[14])=="all"):
            new_rule_list.append(rr)
        else:
            new_split = str(rr[14]).split("<SPLIT>")
            for ns in new_split:
                if(str(ns)==str(room_pk)):
                    new_rule_list.append(rr)

    for nrl in new_rule_list:
        sql = """
                                      SELECT *
                                      FROM RULE_OCC
                                      WHERE rule_pk = %s
                                  """ % (str(nrl[0]))
        occ_result = select_sql(sql)
        overall_return_occ_result = []
        occ_after_value = now_days_value
        for oc_r in occ_result:
            if float(oc_r[2])<=occ_val and float(oc_r[3])>=occ_val:
                # oc_r[4] -> occ_val_up
                # oc_r[6] -> occ_val_percent
                # oc_r[7] -> occ_val
                if(str(oc_r[4])=="1" and str(oc_r[6])=="1"):
                    occ_after_value = now_days_value * (100+float(oc_r[7])) / 100
                    break
                if (str(oc_r[4]) == "1" and str(oc_r[6]) == "0"):
                    occ_after_value = now_days_value + float(oc_r[7])
                    break
                if (str(oc_r[4]) == "0" and str(oc_r[6]) == "1"):
                    occ_after_value = now_days_value * (100-float(oc_r[7])) / 100
                    break
                else: # 0, 0
                    occ_after_value = now_days_value - float(oc_r[7])
                    break
                overall_return_occ_result = oc_r
        # print("occ_after_value = %s"%str(occ_after_value))
        now_days_value = occ_after_value

        if len(overall_return_occ_result)>0:
            if str(overall_return_occ_result[5])== "1":
                sql = """
                                                      SELECT *
                                                      FROM RULE_LT
                                                      WHERE occ_pk = %s
                                                  """ % (str(oc_r[0]))
                lt_result = select_sql(sql)


                for ltr in lt_result:
                    if(int(ltr[2]) <= lead_time and int(ltr[3]) >= lead_time):
                        lt_after_value = now_days_value
                        # ltr[4] -> occ_val_up
                        # ltr[5] -> occ_val_percent
                        # ltr[6] -> occ_val
                        if (str(ltr[4]) == "1" and str(ltr[5]) == "1"):
                            lt_after_value = now_days_value * (100 + float(ltr[6])) / 100
                        if (str(ltr[4]) == "1" and str(ltr[5]) == "0"):
                            lt_after_value = now_days_value + float(ltr[6])
                        if (str(ltr[4]) == "0" and str(ltr[5]) == "1"):
                            lt_after_value = now_days_value * (100 - float(ltr[6])) / 100
                        else:  # 0, 0
                            lt_after_value = now_days_value - float(ltr[6])
                    now_days_value = lt_after_value
    return_str_occ = str(occ_val)
    return_str_now_val = str(now_days_value)
    try:
        if len(return_str_occ.split(".")[1])>2 :
            return_str_occ = return_str_occ[:2 - len(return_str_occ.split(".")[1])]
    except:
        pass
    if return_str_now_val[-2:] == ".0":
        return_str_now_val = return_str_now_val[:-2]
    return return_str_occ + "//" + return_str_now_val

# print(calculate_price_and_occ(2, 23, 2020, 9, 30))


def get_calendar_at_val_table(user_pk, year, month, room_pk):
    sql = """
                               SELECT *
                               FROM ROOM
                               WHERE room_pk=%s
                           """ % (str(room_pk))
    room_pk_result = select_sql(sql)
    if (str(room_pk_result[0][8]) == "None"):
        min_price = -9999999999
    else:
        min_price = float(room_pk_result[0][8])
    if (str(room_pk_result[0][9]) == "None"):
        max_price = 9999999999
    else:
        max_price = float(room_pk_result[0][9])

    now_cal_str = calendar_get_occ_overall_typename_real_calculate(int(user_pk), int(year), int(month),
                                                                   int(room_pk))
    ncs_list = now_cal_str.split("<SPLIT>")
    val_warn = ""
    for ncs in range(len(ncs_list)):
        if (min_price > float(ncs_list[ncs].split("//")[1])):
            val_warn += str(ncs + 1) + ","
        elif (max_price < float(ncs_list[ncs].split("//")[1])):
            val_warn += str(ncs + 1) + ","

    return now_cal_str


def schedular_all_room():
    sql = """
                               SELECT *
                               FROM USER
                           """

    user_result_all = select_sql(sql)

    for user_a in user_result_all:
        try:
            db = pymysql.connect(host='182.162.19.9', port=32796, user="root", passwd="ejnahc", db='hoss')
            cursor = db.cursor()
            sql = """
                              UPDATE ALARM
                              SET alarm_deleted=1
                              WHERE user_pk=%s AND alarm_catagory=3
                          """ % (str(user_a[0]))
            cursor.execute(sql)
            db.commit()
            db.close()
        except:
            db.close()
            return "FALSE"




    day_list = [datetime.datetime.today() - timedelta(days=20), datetime.datetime.today(), datetime.datetime.today()+ timedelta(days=40)]

    sql = """
                               SELECT *
                               FROM ROOM
                           """

    result = select_sql(sql)



    for room in result:
        if (str(room[8]) == "None"):
            min_price = -9999999999
        else:
            min_price = float(room[8])
        if (str(room[9]) == "None"):
            max_price = 9999999999
        else:
            max_price = float(room[9])

        sql = """
                                       SELECT *
                                       FROM HOTEL
                                       WHERE hotel_pk=%s
                                   """ % str(room[1])
        result_hotel = select_sql(sql)

        now_user_pk = str(result_hotel[0][1])

        for dl in day_list:
            print("Schedular \t room:%s - %s/%s"%(str(room[0]), str(dl.year), str(dl.month)))
            now_cal_str = calendar_get_occ_overall_typename_real_calculate(int(now_user_pk), int(dl.year), int(dl.month),
                                                             int(room[0]))

            ncs_list = now_cal_str.split("<SPLIT>")
            val_warn = ""
            for ncs in range(len(ncs_list)):
                if(min_price>float(ncs_list[ncs].split("//")[1])):
                    val_warn += str(ncs+1)+","
                elif(max_price<float(ncs_list[ncs].split("//")[1])):
                    val_warn += str(ncs+1)+","
            if(val_warn==""):
                val_warn = "0"
            else:
                val_warn = val_warn[:-1]
                ####### ALARM 추가 ######
                war_text = "%s년 %s월, 숙소 %s : 최저가&최고가 부적합날짜가 존재합니다.<DETAIL>[%s]일"%(str(dl.year), str(dl.month), str(room[3]), val_warn)
                sql = """
                                             INSERT INTO ALARM(user_pk, alarm_content, alarm_catagory, alarm_deleted)
                                             VALUES(%s,'%s',%s,%s)
                                                       """ % (
                    str(now_user_pk), war_text, "3", "0")
                if not commit_sql(sql) : return "FALSE"

            sql = """
                                               SELECT *
                                               FROM VAL_TABLE
                                               WHERE room_pk=%s AND val_year=%s AND val_month=%s
                                           """ % (str(room[0]), str(dl.year), str(dl.month))
            result_VAL_TABLE = select_sql(sql)

            if len(result_VAL_TABLE)==0:
                sql = """
                                             INSERT INTO VAL_TABLE(room_pk, val_year, val_month,val_str, val_warn)
                                             VALUES(%s,%s,%s,'%s','%s')
                                                       """ % (
                str(room[0]), str(dl.year), str(dl.month), str(now_cal_str), str(val_warn))
                if not commit_sql(sql) : return "FALSE"
            else:
                sql = """
                                                      UPDATE VAL_TABLE
                                                      SET val_str='%s', val_warn='%s'
                                                      WHERE room_pk = %s AND val_year=%s AND val_month=%s
                                                  """ % (
                str(now_cal_str), str(val_warn), str(room[0]), str(dl.year), str(dl.month))
                if not commit_sql(sql): return "FALSE"

def get_all_connections():
    sql = """
                     SELECT *
                     FROM ROOM_CONNECTION
                         """
    result = select_sql(sql)
    returnstr = ""
    for T in result:
        if (T == result[0]):
            returnstr += ""
        else:
            returnstr += "<SPLIT>"
        returnstr += str(T)[1:-1].replace("'", "")

    return returnstr

def get_connections_by_auth_id(auth_id):
    sql = """
                     SELECT *
                     FROM ROOM_CONNECTION
                     WHERE auth_id = '%s'
                         """ % str(auth_id)
    result = select_sql(sql)
    returnstr = ""
    for T in result:
        if (T == result[0]):
            returnstr += ""
        else:
            returnstr += "<SPLIT>"
        returnstr += str(T)[1:-1].replace("'", "")

    return returnstr

def insert_room_connections(room_pk, room_key, authen_key, hotel_name, auth_id):
    sql = """INSERT INTO ROOM_CONNECTION(room_pk, room_key, authen_key, hotel_name, auth_id) VALUES(%s,'%s','%s','%s', '%s')""" % (room_pk, room_key, authen_key, hotel_name, auth_id)
    if not commit_sql(sql) : return "FAILED"
    return "SUCCESS"

def update_room_connections(room_pk, room_key, authen_key, hotel_name, recordId):
    sql = """
            UPDATE ROOM_CONNECTION
            SET room_pk=%s,
                room_key=%s,
                authen_key='%s',
                hotel_name='%s'
            WHERE recordId=%s
            """ % (room_pk, room_key, authen_key, hotel_name, recordId)
    if not commit_sql(sql) : return "FAILED"
    return "SUCCESS"

def delete_room_connections(recordId):
    sql = """
                  DELETE FROM ROOM_CONNECTION
                  WHERE recordId='%s'
                            """ % str(recordId)
    if not commit_sql(sql) : return "FAILED"
    return "SUCCESS"