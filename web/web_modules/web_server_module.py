import datetime
from datetime import timedelta
import calendar
from flask import Flask, render_template, request, send_file
from flask_cors import CORS, cross_origin
from web.web_modules.db_select import *


def user_auth(request_param):
    str_id = request_param.form['id']
    str_pw = request_param.form['pw']
    result = check_id(str_id, str_pw)
    if len(result) == 0: return False
    else: return result[0][0]
