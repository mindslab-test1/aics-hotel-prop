 # -*- coding: utf-8 -*-
import os
import sys
import time
import schedule 

import uuid
import requests
import json
from random import *

from multiprocessing import Process,Queue
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
from web.web_modules.web_server_module import user_auth


from flask import Flask, render_template, request, send_file
from flask_cors import CORS, cross_origin
from web.web_modules.db_select import *


class MyFlask(Flask):
    jinja_options = Flask.jinja_options.copy()
    jinja_options.update(dict(
        block_start_string='{%',
        block_end_string='%}',
        variable_start_string='((',
        variable_end_string='))',
        comment_start_string='{#',
        comment_end_string='#}',
    ))


app = MyFlask(__name__, static_folder="./static")
CORS(app)
app.config['UPLOAD_FOLDER'] = "output"  # WEB->UPLOAD_FOLDER


def root_dir():  # pragma: no cover
    return os.path.abspath(os.path.dirname(__file__))


def get_file(filename):  # pragma: no cover
    try:
        src = os.path.join(root_dir(), filename)
        return open(src).read()
    except IOError as exc:
        return str(exc)



@app.route('/')
def index():
    return render_template('login.html')

@app.route('/login', methods=['GET'])
def login_index():
    return render_template('login.html')

@app.route('/login', methods=['POST'])
def login():
    try:
        str_id = request.form['id']
        str_pw = request.form['pw']
        print("Login access : " + str_id + "/" + str_pw)

        result = check_id(str_id, str_pw)

        if len(result) == 1:
            if result[0][6] == 0:
                update_tmp = update_id_date(str_id, str_pw)
                if (update_tmp):
                    return render_template('index.html', login_ID=str_id, login_KEY=str_pw)
            elif result[0][6] == 1:
                update_tmp = update_id_date(str_id, str_pw)
                if (update_tmp):
                    return render_template('login.html', message_alert="아직 가입 확인이 되지 않았습니다")
            elif result[0][6] == 3:
                update_tmp = update_id_date(str_id, str_pw)
                if (update_tmp):
                    return render_template('admin_page.html', login_ID=str_id, login_KEY=str_pw)
            elif result[0][6] == 99:
                update_tmp = update_id_date(str_id, str_pw)
                if (update_tmp):
                    return render_template('login.html', message_alert="관리자에 의해 삭제된 계정입니다.")
        return render_template('login.html', message_alert="계정 정보가 틀리거나 없는 계정입니다.")
    except:
        print("Exception occurred in login [POST]")
        return render_template('login.html')


@app.route('/js/<js>', methods=['GET'])
def get_js(js):
    return send_file("static/js/" + js)


@app.route('/login_asset/<image>', methods=['GET'])
def get_login_image(image):
    return send_file("static/images/login_asset/" + image)


@app.route('/login_css/<css>', methods=['GET'])
def get_login_css(css):
    return send_file("static/css/login_css/" + css)


@app.route('/font/<fontname>', methods=['GET'])
def get_font(fontname):
    return send_file("static/font/" + fontname)


@app.route('/asset/<asset>', methods=['GET'])
def get_asset(asset):
    return send_file("static/images/" + asset)


@app.route('/asset_leftside/<leftasset>', methods=['GET'])
def get_leftasset(leftasset):
    return send_file("static/images/side_bar_asset/" + leftasset)


@app.route('/side_bar_icons/<icons>', methods=['GET'])
def get_side_icon(icons):
    return send_file("static/images/side_bar_icons/" + icons)


@app.route('/top_bar_icons/<top_icons>', methods=['GET'])
def get_top_icon(top_icons):
    return send_file("static/images/top_bar_icons/" + top_icons)


def remove_all_file_in_folder(foldername):
    if os.path.exists(foldername):
        for file in os.scandir(foldername):
            os.remove(file.path)


@app.route('/gethotel', methods=['POST'])
def post_get_all_hotel():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    return str(get_all_hotel(now_user_pk))


@app.route('/getroom', methods=['POST'])
def post_get_room():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"

    hotel_pk = request.form['hotel_pk']
    return str(get_all_rooms(hotel_pk))


@app.route('/addhotel', methods=['POST'])
def post_add_hotel():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"

    hotel_kor_name = request.form['hotel_kor_name']
    hotel_eng_name = request.form['hotel_eng_name']
    hotel_call = request.form['hotel_call']
    hotel_email = request.form['hotel_email']
    hotel_category = request.form['hotel_category']
    hotel_head_name = request.form['hotel_head_name']
    hotel_con1 = request.form['hotel_con1']
    hotel_con2 = request.form['hotel_con2']
    hotel_con3 = request.form['hotel_con3']
    hotel_addr = request.form['hotel_addr']
    print("ADD_HOTEL -> " + str((now_user_pk, hotel_kor_name, hotel_eng_name, hotel_call, hotel_email, hotel_category,
                                 hotel_head_name, hotel_con1, hotel_con2, hotel_con3, hotel_addr)))
    return add_hotel(now_user_pk, hotel_kor_name, hotel_eng_name, hotel_call, hotel_email, hotel_category,
                     hotel_head_name, hotel_con1, hotel_con2, hotel_con3, hotel_addr)


@app.route('/addroom', methods=['POST'])
def post_add_rooms():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"

    add_hotel_pk = request.form['add_hotel_pk']
    room_category = request.form['room_category']
    room_typename = request.form['room_typename']
    room_people = request.form['room_people']
    room_max_people = request.form['room_max_people']
    room_how_many = request.form['room_how_many']
    room_name = request.form['roomName']
    room_names = request.form['room_names']
    room_price_sun = request.form["room_price_sun"]
    room_price_mon = request.form["room_price_mon"]
    room_price_tue = request.form["room_price_tue"]
    room_price_wed = request.form["room_price_wed"]
    room_price_thu = request.form["room_price_thu"]
    room_price_fri = request.form["room_price_fri"]
    room_price_sat = request.form["room_price_sat"]

    return add_rooms(add_hotel_pk, room_category, room_typename, room_people, room_max_people, room_how_many
                     , room_name, room_names, room_price_sun,room_price_mon,room_price_tue,
                     room_price_wed, room_price_thu, room_price_fri, room_price_sat)


@app.route('/addrule', methods=['POST'])
def addrule_overall():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"

    try:
        rule_type = request.form["rule_type"]
        rule_apply_room = request.form["rule_apply_room"]
        if(rule_apply_room.find("<SPLIT>")!=-1):
            if(rule_apply_room[-7:] == "<SPLIT>"):
                rule_apply_room = rule_apply_room[:-7]
        rule_name = request.form["rule_name"]
        rule_time_apply = request.form["rule_time_apply"]
        rule_before_day = request.form["rule_before_day"]
        rule_auto_apply = request.form["rule_auto_apply"]
        rule_sun = request.form["rule_sun"]
        rule_mon = request.form["rule_mon"]
        rule_tue = request.form["rule_tue"]
        rule_wed = request.form["rule_wed"]
        rule_thu = request.form["rule_thu"]
        rule_fri = request.form["rule_fri"]
        rule_sat = request.form["rule_sat"]
        rule_start_day = request.form["rule_start_day"]
        rule_end_day = request.form["rule_end_day"]
        overall_rule_process = request.form["overall_rule_process"]

        rule_uuid = str(uuid.uuid4())[:20]
        ar = add_rule(now_user_pk, rule_type, rule_name, rule_time_apply, rule_before_day, rule_auto_apply, rule_sun,
                      rule_mon, rule_tue,
                      rule_wed, rule_thu, rule_fri, rule_sat, rule_apply_room, rule_uuid, 'true', rule_start_day,
                      rule_end_day)
        if ar == "FAILED" : return "FALSE"
        rule_pk = uuid_to_rule_pk(rule_uuid)

        # print(overall_rule_process)
        overall_rule = str(overall_rule_process).split("<OCC>")[1:]
        for T in overall_rule:
            tmplist = T.split("<LT>")
            print("tmpList -> " + str(tmplist))
            if (len(tmplist) > 1):
                LT_toggle = 'true'
            else:
                LT_toggle = 'false'
            tmp_occ_uuid = str(uuid.uuid4())[:20]
            tmpListSplit = tmplist[0].split(",")
            ao = add_OCC(rule_pk, tmpListSplit[0], tmpListSplit[1], tmpListSplit[2], LT_toggle, tmpListSplit[4],
                         tmpListSplit[3], tmp_occ_uuid)
            if ao == "FAILED" : return "FALSE"
            print("OCC > " + tmplist[0])
            occ_pk = uuid_to_OCC_pk(tmp_occ_uuid)
            first = True
            for M in tmplist:
                if (first):
                    first = False
                    continue
                else:
                    tmpListSplit = M.split(",")
                    al = add_LT(occ_pk, tmpListSplit[0], tmpListSplit[1], tmpListSplit[2], tmpListSplit[4],
                                tmpListSplit[3])
                    if al == "FAILED" : return "FALSE"
                    print("LT > " + M)
    except:
        return "FALSE"

    return "SUCCEES"

@app.route('/startAlarm', methods=['POST'])
def start_alarm():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"

    return get_alarm(now_user_pk)


@app.route('/delAlarm', methods=['POST'])
def del_alarm():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    
    alarm_key = request.form['alarm_key']
    del_alarm_toggle(alarm_key)
    return "SUCCEES"


@app.route('/getTimeScope', methods=['POST'])
def get_time_scope():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"

    return get_time_scope_db(now_user_pk)


@app.route('/addScope', methods=['POST'])
def add_time_scope():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"

    return_overall_str = request.form['add_scope_str']
    overall_list = return_overall_str.split(",")
    start_days_db = overall_list[1] + "-" + overall_list[2] + "-" + overall_list[3]
    end_days_db = overall_list[4] + "-" + overall_list[5] + "-" + overall_list[6]
    now_add_room_pk = request.form["now_add_room_pk"]
    scope_price_sun = request.form["scope_price_sun"]
    scope_price_mon = request.form["scope_price_mon"]
    scope_price_tue = request.form["scope_price_tue"]
    scope_price_wed = request.form["scope_price_wed"]
    scope_price_thu = request.form["scope_price_thu"]
    scope_price_fri = request.form["scope_price_fri"]
    scope_price_sat = request.form["scope_price_sat"]
    # start_days_db,end_days_db,scope_price_sun,scope_price_mon,scope_price_tue,scope_price_wed,scope_price_thu,scope_price_fri,scope_price_sat
    return add_time_scope_db(overall_list[0],now_user_pk, overall_list[1], overall_list[2], overall_list[3], overall_list[4], overall_list[5], overall_list[6],now_add_room_pk,start_days_db,end_days_db,scope_price_sun,scope_price_mon,scope_price_tue,scope_price_wed,scope_price_thu,scope_price_fri,scope_price_sat)


@app.route('/deleteTimeScope', methods=['POST'])
def delete_time_scope():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    del_scope_pk = request.form['del_scope_pk']
    return del_time_scope_db(del_scope_pk)


@app.route('/deleteHotel', methods=['POST'])
def del_hotel_POST():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    del_hotel_pk = request.form['del_hotel_pk']
    return del_hotel_db(del_hotel_pk)

@app.route('/deleteRoom', methods=['POST'])
def del_room_POST():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    del_hotel_pk = request.form['del_room_pk']
    return del_room_db(del_hotel_pk)

@app.route('/modifyGet', methods=['POST'])
def modify_get_room_contents():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    check_room_pk = request.form['check_room_pk']
    returnStr = str(modify_get_room_contents_db(check_room_pk))[1:-1].replace("'","")
    addStr = str(modify_get_room_name_contents_db(check_room_pk))
    if addStr=='FALSE' :  return "FALSE"
    else:
        returnStr += addStr

    return returnStr


@app.route('/realModifyRoom', methods=['POST'])
def real_modify_room_contents():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    del_room_pk = request.form['del_room_pk']
    add_hotel_pk = request.form['add_hotel_pk']
    room_category = request.form['room_category']
    room_typename = request.form['room_typename']
    room_people = request.form['room_people']
    room_max_people = request.form['room_max_people']
    room_how_many = request.form['room_how_many']
    room_name = request.form['roomName']
    room_price_sun = request.form["room_price_sun"]
    room_price_mon = request.form["room_price_mon"]
    room_price_tue = request.form["room_price_tue"]
    room_price_wed = request.form["room_price_wed"]
    room_price_thu = request.form["room_price_thu"]
    room_price_fri = request.form["room_price_fri"]
    room_price_sat = request.form["room_price_sat"]

    return modify_room_not_del(del_room_pk,add_hotel_pk, room_category, room_typename, room_people, room_max_people,
                               room_how_many,  room_name, room_price_sun,room_price_mon,room_price_tue,
                     room_price_wed, room_price_thu, room_price_fri, room_price_sat)


@app.route('/getRulesAtRulesConfigPages', methods=['POST'])
def get_rules_at_rules_config_page():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    return get_rules_in_rule_config_page_db(now_user_pk)


@app.route('/deleteRuleInConfigPage', methods=['POST'])
def del_rules_at_rules_config_page():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    del_rule_pk = request.form['del_rule_pk']
    return del_rules_and_occ_and_lt_db(del_rule_pk)

@app.route('/onrule',methods=['POST'])
def onrule():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    on_rule_pk = request.form['on_rule_pk']
    return on_rule_db(on_rule_pk)

@app.route('/offrule',methods=['POST'])
def offrule():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    off_rule_pk = request.form['off_rule_pk']
    return off_rule_db(off_rule_pk)


@app.route('/addAssign', methods=['POST'])
def add_assign():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    room_pk = request.form['room_pk']
    start_year = request.form['start_year']
    start_month = request.form['start_month']
    start_day = request.form['start_day']
    assign_person_name= request.form['assign_person_name']
    price = request.form['price']

    return add_assign_db(room_pk, start_year, start_month, start_day, assign_person_name, price)


@app.route('/dashboardTableChart', methods=['POST'])
def dashboard_table_and_chart():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    now_year = int(request.form['now_year'])
    now_month = int(request.form['now_month'])
    return dashboard_table_and_chart_db(now_user_pk, now_year, now_month)

@app.route('/scopePageOn', methods=['POST'])
def scopePageOn():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    return scopePageOnDB(now_user_pk)


@app.route('/dashboardTop', methods=['POST'])
def dashboard_start_top():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    now_year = int(request.form['now_year'])
    now_month = int(request.form['now_month'])
    return get_dashboard_vals_compare(now_user_pk, now_year, now_month)


@app.route('/calendarGetRooms', methods=['POST'])
def get_rooms_in_calendar():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    return calendar_get_hotel_rooms(now_user_pk)


@app.route('/calendarOCCGet', methods=['POST'])
def get_OCC_in_calendar_overall():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    now_year= request.form['now_year']
    now_month= request.form['now_month']

    return calendar_get_occ_overall(now_user_pk, now_year, now_month)


@app.route('/getAlertPercent', methods=['POST'])
def get_alert_percent_first():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    return get_alert_percent_db(now_user_pk)


@app.route('/setAlertPercent', methods=['POST'])
def set_alert_percent():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"

    occ1 = request.form['occ1']
    occ2 = request.form['occ2']
    occ3 = request.form['occ3']
    occ4 = request.form['occ4']
    occ5 = request.form['occ5']
    occ6 = request.form['occ6']

    return set_alert_percent_db(now_user_pk, occ1, occ2, occ3, occ4, occ5, occ6)


@app.route('/checkHotelUser', methods=['POST'])
def check_hotel_user_in_category():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    return check_hotel_user(now_user_pk)


@app.route('/modifyhotel', methods=['POST'])
def hotel_modify_to_check():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"

    hotel_kor_name = request.form['hotel_kor_name']
    hotel_eng_name = request.form['hotel_eng_name']
    hotel_call = request.form['hotel_call']
    hotel_email = request.form['hotel_email']
    hotel_category = request.form['hotel_category']
    hotel_head_name = request.form['hotel_head_name']
    hotel_con1 = request.form['hotel_con1']
    hotel_con2 = request.form['hotel_con2']
    hotel_con3 = request.form['hotel_con3']
    hotel_addr = request.form['hotel_addr']
    print("MODIFY_HOTEL -> " + str((now_user_pk, hotel_kor_name, hotel_eng_name, hotel_call, hotel_email, hotel_category,
                                 hotel_head_name, hotel_con1, hotel_con2, hotel_con3, hotel_addr)))
    return modify_in_user_hotel(now_user_pk, hotel_kor_name, hotel_eng_name, hotel_call, hotel_email, hotel_category,
                     hotel_head_name, hotel_con1, hotel_con2, hotel_con3, hotel_addr)


@app.route('/cat9GetRoomNames', methods=['POST'])
def cat9_get_room_names():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    return get_room_names_db(now_user_pk)


@app.route('/addAssignDBDB', methods=['POST'])
def cat9_add_assign():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    assign_room_pk = request.form["assign_room_pk"]
    start_year = request.form["start_year"]
    start_month = request.form["start_month"]
    start_day = request.form["start_day"]
    price = request.form["price"]
    assign_person_name = request.form["assign_person_name"]
    end_year = request.form["end_year"]
    end_month = request.form["end_month"]
    end_day = request.form["end_day"]
    channel_created = request.form["channel_created"]
    how_day = request.form["how_day"]
    try:
        now = datetime.datetime.now()
        created_at = str(now.strftime('%Y%2m%2d%2H%2M'))
    except:
        created_at = "202001012240"

    return cat9_add_assign_db(
        assign_room_pk,start_year,start_month,start_day,
        price,assign_person_name,end_year,end_month,end_day,
        channel_created,how_day,created_at
    )


@app.route('/searchAssignDBDB', methods=['POST'])
def search_assign():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"

    time_pivot = request.form["time_pivot"]
    room_pk = request.form["room_name_pk"]  # 예약한 룸 pk
    start_year = request.form["start_year"]
    start_month = request.form["start_month"]
    start_day = request.form["start_day"]
    end_year = request.form["end_year"]
    end_month = request.form["end_month"]
    end_day = request.form["end_day"]
    search_keyword = request.form["search_keyword"]

    return search_assign_db(
        now_user_pk,
        time_pivot, room_pk,
        start_year, start_month, start_day,
        end_year, end_month, end_day,
        search_keyword
    )


@app.route('/delAssignDBDB', methods=['POST'])
def del_assign_in_table():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"

    del_assign_pk = request.form["del_assign_pk"]
    return del_assign_in_table_db(del_assign_pk)


@app.route('/sugiModifiOn', methods=['POST'])
def modify_assign_on_div():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    assign_pk = request.form['assign_pk']
    return get_assign_in_modify_on(assign_pk)


@app.route('/modifyAssignDBDB', methods=['POST'])
def modify_assign_dbdb_div():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    now_modify_assign_pk = request.form["now_modify_assign_pk"]

    start_year = request.form["start_year"]
    start_month = request.form["start_month"]
    start_day = request.form["start_day"]
    price = request.form["price"]
    assign_person_name = request.form["assign_person_name"]
    end_year = request.form["end_year"]
    end_month = request.form["end_month"]
    end_day = request.form["end_day"]
    channel_created = request.form["channel_created"]
    how_day = request.form["how_day"]
    try:
        now = datetime.datetime.now()
        created_at = str(now.strftime('%Y%2m%2d%2H%2M'))
    except:
        created_at = "202001012240"

    return cat9_modify_assign_db(
        now_modify_assign_pk,
        start_year, start_month, start_day,
        price, assign_person_name, end_year, end_month, end_day,
        channel_created, how_day, created_at
    )


@app.route('/getComp', methods=['POST'])
def get_comp_in_dashboard():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"

    returnstr = "9/01//9/02//9/03//9/04//9/05//9/06//9/07//9/08//9/09//9/10//9/11//9/12//9/13//9/14//"
    for i in range(10):
        returnstr += "<SPLIT>Example" + str(i+1) + "<NAME>"
        for t in range(14):
            returnstr += str(randrange(100)*1000 + 50000) + "//"
        returnstr = returnstr[:-2]
    return returnstr


@app.route('/addUserName', methods=['POST'])
def add_user_in_first_screen():
    user_id = request.form["user_id"]
    user_pw = request.form["user_pw"]
    user_name = request.form["user_name"]
    tel_num = request.form["tel_num"]
    hotel_name = request.form["hotel_name"]
    marketing = request.form["marketing"]
    return add_user_in_dbdb(user_id, user_pw, user_name, tel_num, hotel_name, marketing)


@app.route('/adminLogin', methods=['POST'])
def admin_login():
    str_id = request.form['user_id']
    str_pw = request.form['user_pw']
    result = check_id(str_id, str_pw)
    if len(result) > 0:
        now_user_status = result[0][6]
    else:
        return "FALSE"
    if str(now_user_status) != '3':
        return "FALSE"
    return getAllUserDB()


@app.route('/adminDelUser', methods=['POST'])
def admin_del_user():
    str_id = request.form['user_id']
    str_pw = request.form['user_pw']
    del_user_pk = request.form['del_user_pk']
    result = check_id(str_id, str_pw)
    if len(result) > 0:
        now_user_status = result[0][6]
    else:
        return "FALSE"
    if str(now_user_status) != '3':
        return "FALSE"

    return admin_del_user_dbdb(del_user_pk)


@app.route('/adminCheckUser', methods=['POST'])
def admin_check_user():
    str_id = request.form['user_id']
    str_pw = request.form['user_pw']
    check_user_pk = request.form['check_user_pk']
    result = check_id(str_id, str_pw)
    if len(result) > 0:
        now_user_status = result[0][6]
    else:
        return "FALSE"
    if str(now_user_status) != '3':
        return "FALSE"

    return admin_check_user_dbdb(check_user_pk)


@app.route('/searchALLAssignDBDB', methods=['POST'])
def search_all_assign():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"

    return search_all_assign_db(now_user_pk)


@app.route('/modifyRuleDB', methods=['POST'])
def modify_get_rule_db_select():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    modify_rule_pk = request.form['modify_rule_pk']
    return check_rule_with_rulepk(modify_rule_pk)


@app.route('/saveminmaxprice', methods=['POST'])
def save_min_max_price():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    minmax = request.form['minmax']
    minmax_overall_list = str(minmax).split("<SPLIT>")
    param_list = list()
    for mol in minmax_overall_list:
        param_list.append(mol.split("//"))
        print(mol.split("//"))
    return save_min_max_price_db(param_list)


@app.route('/modiRule', methods=['POST'])
def modi_rule_overall():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"

    try:
        now_modify_rule_pk = request.form["now_modify_rule_pk"]
        rule_type = request.form["rule_type"]
        rule_apply_room = request.form["rule_apply_room"]
        if(rule_apply_room.find("<SPLIT>")!=-1):
            if(rule_apply_room[-7:] == "<SPLIT>"):
                rule_apply_room = rule_apply_room[:-7]
        rule_name = request.form["rule_name"]
        rule_time_apply = request.form["rule_time_apply"]
        rule_before_day = request.form["rule_before_day"]
        rule_auto_apply = request.form["rule_auto_apply"]
        rule_sun = request.form["rule_sun"]
        rule_mon = request.form["rule_mon"]
        rule_tue = request.form["rule_tue"]
        rule_wed = request.form["rule_wed"]
        rule_thu = request.form["rule_thu"]
        rule_fri = request.form["rule_fri"]
        rule_sat = request.form["rule_sat"]
        rule_start_day = request.form["rule_start_day"]
        rule_end_day = request.form["rule_end_day"]
        overall_rule_process = request.form["overall_rule_process"]

        rule_uuid = str(uuid.uuid4())[:20]
        ar = modif_rule(now_modify_rule_pk, now_user_pk, rule_type, rule_name, rule_time_apply, rule_before_day, rule_auto_apply, rule_sun,
                      rule_mon, rule_tue,
                      rule_wed, rule_thu, rule_fri, rule_sat, rule_apply_room, rule_uuid, 'true', rule_start_day,
                      rule_end_day)
        if ar == "FAILED" : return "FALSE"
        rule_pk = now_modify_rule_pk

        # delete OCC/LT
        del_occ_and_lt(now_modify_rule_pk)

        overall_rule = str(overall_rule_process).split("<OCC>")[1:]
        for T in overall_rule:
            tmplist = T.split("<LT>")
            print("tmpList -> " + str(tmplist))
            if (len(tmplist) > 1):
                LT_toggle = 'true'
            else:
                LT_toggle = 'false'
            tmp_occ_uuid = str(uuid.uuid4())[:20]
            tmpListSplit = tmplist[0].split(",")
            ao = add_OCC(rule_pk, tmpListSplit[0], tmpListSplit[1], tmpListSplit[2], LT_toggle, tmpListSplit[4],
                         tmpListSplit[3], tmp_occ_uuid)
            if ao == "FAILED" : return "FALSE"
            print("OCC > " + tmplist[0])
            occ_pk = uuid_to_OCC_pk(tmp_occ_uuid)
            first = True
            for M in tmplist:
                if (first):
                    first = False
                    continue
                else:
                    tmpListSplit = M.split(",")
                    al = add_LT(occ_pk, tmpListSplit[0], tmpListSplit[1], tmpListSplit[2], tmpListSplit[4],
                                tmpListSplit[3])
                    if al == "FAILED" : return "FALSE"
                    # print("LT > " + M)
    except:
        return "FALSE"

    return "SUCCEES"


@app.route('/calendarOCCGetTypename', methods=['POST'])
def get_OCC_in_calendar_overall_typename():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    now_year= request.form['now_year']
    now_month= request.form['now_month']
    room_pk = request.form['room_pk']
    return get_calendar_at_val_table(int(now_user_pk), int(now_year), int(now_month), int(room_pk))



@app.route('/modifyTimeScope', methods=['POST'])
def modif_screen_on_scope():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    modif_scope_pk = request.form['modif_scope_pk']
    return modif_screen_on_db(modif_scope_pk)


@app.route('/modifScopeDB', methods=['POST'])
def mod_time_scope():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"

    return_overall_str = request.form['add_scope_str']
    overall_list = return_overall_str.split(",")
    start_days_db = overall_list[1] + "-" + overall_list[2] + "-" + overall_list[3]
    end_days_db = overall_list[4] + "-" + overall_list[5] + "-" + overall_list[6]
    now_add_room_pk = request.form["now_add_room_pk"]
    scope_price_sun = request.form["scope_price_sun"]
    scope_price_mon = request.form["scope_price_mon"]
    scope_price_tue = request.form["scope_price_tue"]
    scope_price_wed = request.form["scope_price_wed"]
    scope_price_thu = request.form["scope_price_thu"]
    scope_price_fri = request.form["scope_price_fri"]
    scope_price_sat = request.form["scope_price_sat"]

    modif_scope_pk = request.form["modif_scope_pk"]
    # start_days_db,end_days_db,scope_price_sun,scope_price_mon,scope_price_tue,scope_price_wed,scope_price_thu,scope_price_fri,scope_price_sat
    return mod_time_scope_db(overall_list[0],now_user_pk, overall_list[1], overall_list[2], overall_list[3], overall_list[4], overall_list[5], overall_list[6],now_add_room_pk,start_days_db,end_days_db,scope_price_sun,scope_price_mon,scope_price_tue,scope_price_wed,scope_price_thu,scope_price_fri,scope_price_sat,modif_scope_pk)

@app.route('/getHotelStoryData', methods=['POST'])
def getHotelStoryData():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"   

    #get hotel story data
    url = "https://api.hotelstory.com/Mindslab/property"
    authId = request.form["authId"]
    authPw = request.form["authPwd"]
    payLoad =   {   "AUTH_ID" : authId,
		            "AUTH_PW" : authPw
                }
    response = requests.post(url, data = json.dumps(payLoad))
    if "status" in response.json():
        return str(response.json())

    #get rooms information
    hotelStoryData = response.json()
    listHotelStory = []
    roomUrl = "https://api.hotelstory.com/Mindslab/roominfo"
    for data in hotelStoryData:
        authKey = data["AUTH_KEY"]
        hotelName = data["PropertyName"]
        listRoom = []
        for roomKey in data["rooms"]:
            roomPayload ={
                "AUTH_KEY" : authKey,
		        "room_key" : roomKey
            }
            roomRespone = requests.post(roomUrl, data = json.dumps(roomPayload))            
            roomData = roomRespone.json()
            if "status" in roomData:
                return str(roomData + " for roomKey: "+ roomKey)
                
            for room in roomData:
                listRoom.append(room)
        hotelStory = {
            "authKey": authKey,
            "hotelName": hotelName,
            "listRoom": listRoom
        }
        listHotelStory.append(hotelStory)
    return  str(listHotelStory)

@app.route('/getRoomConnections', methods=['POST'])
def getRoomConnections():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    connDataStr = get_connections_by_auth_id(request.form["authId"])
    if connDataStr is None or connDataStr == "":
        return ("FAILED: No data on CONNECTIONS table!")
    return connDataStr

@app.route('/saveMapping', methods=['POST'])
def saveMapping():
    now_user_pk = user_auth(request)
    if not now_user_pk: return "FAILED"
    connData = json.loads(request.form["connData"])
    listRecord = []
    for data in connData:
        result=""
        tmpStr = data["roomKey"].split("-")
        roomKey = tmpStr[0]
        authKey = tmpStr[1]
        hotelName = tmpStr[2]
        if data["flag"] == "I":
            result = insert_room_connections(data["roomPk"], roomKey, authKey, hotelName, data["authId"])
            record = {
                "data": data,
                "action": "INSERT",
                "result": result
            }
            listRecord.append(record)
        if data["flag"] == "U":
            result = update_room_connections(data["roomPk"], roomKey, authKey, hotelName, data["id"] )
            record = {
                "data": data,
                "action": "UPDATE",
                "result": result
            }
            listRecord.append(record)
        if data["flag"] == "D":
            result = delete_room_connections(data["id"])
            record = {
                "data": data,
                "action": "DELETE",
                "result": result
            }
            listRecord.append(record)
    return str(listRecord)

def schedule_run_pending_func():
    timer_count = 0
    while True:
        timer_count += 1
        time.sleep(1)
        if timer_count>3600:
            print("Timer count reset & schedular all room")
            schedular_all_room()
            timer_count = 0

def getBookingFromHotelStory():
    print("Get booking information from Hotel Story")
    #get yesterday date
    now = datetime.datetime.now()
    yesterday = now - datetime.timedelta(days=1)

    connDataStr = get_all_connections()
    print(connDataStr)
    if connDataStr is None or connDataStr == "":
        print("No data on CONNECTIONS table!")
        return 0

    connDataList = []
    connDataList = connDataStr.split("<SPLIT>")
    for connData in connDataList: #loop every item in connData
        data = connData.split(", ")
        roomPk = data[1]
        roomKey = data[2]
        authKey = data[3]

        #call API and get booking data
        #time = "2020-11-25" #for testing purpose,
        time = yesterday.strftime("%Y-%m-%d")
        roomKey = roomKey
        url = "https://api.hotelstory.com/Mindslab/bookinginfo"
        payLoad =   {
            "AUTH_KEY" : authKey,
            "room_key" : roomKey,
            "time" : time
        }
        response = requests.post(url, data = json.dumps(payLoad))
        resObjs = response.json()
        if "status" in resObjs:
            print("No booking information for room " + roomKey +" on "+ time)
        else:
            print(resObjs)
            for obj in resObjs:
                startDate = datetime.datetime.strptime(obj["start_date"], '%Y-%m-%d')
                endDate = datetime.datetime.strptime(obj["end_date"], '%Y-%m-%d')
                duration = endDate - startDate

                assign_room_pk = roomPk            
                start_year = startDate.year
                start_month = startDate.month
                start_day = startDate.day
                price = obj["price"]
                end_year = endDate.year
                end_month = endDate.month
                end_day = endDate.day
                channel_created = "Hotel Story"
                how_day = duration.days                
                created_at = str(now.strftime('%Y%m%d%H%M'))
                cat9_add_assign_db(assign_room_pk, start_year, start_month, start_day, price, "", end_year, end_month, end_day, channel_created, how_day, created_at) 

def schedule_run_insert_booking_information():
    schedule.every().day.at("01:00").do(getBookingFromHotelStory)
    #schedule.every(1).minutes.do(getBookingFromHotelStory) 

    while True:   
        # Checks whether a scheduled task  
        # is pending to run or not         
        schedule.run_pending() 
        time.sleep(1)

def server_start():
    app.run(host="0.0.0.0", port=33323)    


if __name__ == "__main__":
    schedular_all_room()
    mul_thread = Process(target=schedule_run_pending_func, args=())
    mul_thread_hotelStory = Process(target=schedule_run_insert_booking_information, args=())
    app_thread = Process(target=server_start, args=())
    mul_thread.start()
    mul_thread_hotelStory.start()
    app_thread.start()
    mul_thread.join()
    mul_thread_hotelStory.join()
    app_thread.join()

def toggle_to_binary(str_to_compare):
    if (str_to_compare == 'true') or (str_to_compare == 'True'):
        return 1
    else:
        return 0
