

function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
}

/* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
}

function dateInfo(){//Like Struct
    var date = 0;
    var day = "요일";
    var percentOCC = 10;
    var pay = 0.0;
    var onMouse = false;
    var color = 0;
}

function occInfo(){//Like Struct
    var startOCCPol = 0;
    var endOCCPol = 0;
    var percent = true;
    var calculationVal = 0;
    var indColor = 0;
    var ltList = [];
}

function leadTimeInfo(){
    var startLTPol = 0;
    var endLTPol = 0;
    var percent = true;
    var calculationVal = 0;
}
function pushOCCInfo(){
    pushOCC = new occInfo();
    pushOCC.startOCCPol = 0;
    pushOCC.endOCCPol = 0;
    pushOCC.percent = true;
    pushOCC.calculationVal = 0;
    pushOCC.indColor = 0;
    pushOCC.ltList = [];
    vm.policyInfo.push(pushOCC);
}

function pushLTInfo(OCCIndex){
    pushLT = new leadTimeInfo();
    pushLT.startLTPol = 0;
    pushLT.endLTPol = 0;
    pushLT.percent = true;
    pushLT.calculationVal = 0;
    vm.policyInfo[OCCIndex].ltList.push(pushLT);
}


function setMonth(setYear, setMonth){
    vm.year = setYear;
    vm.month = setMonth;
    vm.calData = getCalender(setYear,setMonth);
    vm.firstDayTsMonth =  (new Date(setYear,setMonth, 1)).getDay()
}

function toNextMonth(){
    tmpDateToFunc = (new Date(vm.year,vm.month+1, 1))
    setMonth(tmpDateToFunc.getFullYear(), tmpDateToFunc.getMonth())
}

function toPrevMonth(){
    tmpDateToFunc = (new Date(vm.year,vm.month-1, 1))
    setMonth(tmpDateToFunc.getFullYear(), tmpDateToFunc.getMonth())
}

function toCurrencyComma(integerVal){
    tmpVal = String(integerVal)
    retVal = ""

    for(var i = 0; i<tmpVal.length; i++){
        if ((tmpVal.length-i)%3==0 && i!=0 && i!=tmpVal.length-1){
            retVal += ","
        }
        retVal += tmpVal.charAt(i)
    }
    return retVal
}





function getCalender(nowYear, nowMonth){
    var tmpDate = new Date()
    tmpDate = new Date(nowYear, nowMonth, 1)
    var firstDay = tmpDate.getDay()
    //일-0 ~ 토-6
    var nxtMonthDate =  new Date(tmpDate.getFullYear(), tmpDate.getMonth()+1, 1)
    nxtMonthDate = new Date(nxtMonthDate.getFullYear(), nxtMonthDate.getMonth(), nxtMonthDate.getDate()-1)
    var lastDay = nxtMonthDate.getDate()

    var dateInfoArr = new Array();
    for(var i = 1; i<=lastDay; i++){
        dateInfoArr[i-1] = new dateInfo()
        dateInfoArr[i-1].date = i;
        dateInfoArr[i-1].day = ["일","월","화","수","목","금","토"][(firstDay+i-1)%7]
        dateInfoArr[i-1].percentOCC = Math.floor(100*Math.random());
        dateInfoArr[i-1].pay = 100*Math.ceil(Math.random()*500) + 10000;
    }
    return dateInfoArr;
}

function getDashboardCalander(warningDaysList){
    let tmpArr = getCalender(2020,10)
    let days =  ["일","월","화","수","목","금","토"]
    vm.nowDateInfoArr = []
    for(let iii = 0; iii<7; iii++){vm.nowDateInfoArr.push(""); if(tmpArr[0].day==days[iii]) break}
    for(let iii = 0; iii<tmpArr.length; iii++){vm.nowDateInfoArr.push(iii+1)}
}

function validateModifyValue(){
    var floatRegex = /^-?\d+(?:[.,]\d*?)?$/;
    var returnVal = true;
    var alertVal = "";
    for(var i = 0; i<vm.policyInfo.length; i++){
        vm.policyInfo[i].startOCCPol = vm.policyInfo[i].startOCCPol.toString().trim()
        vm.policyInfo[i].endOCCPol = vm.policyInfo[i].endOCCPol.toString().trim()
        vm.policyInfo[i].calculationVal = vm.policyInfo[i].calculationVal.toString().trim()
        if (!floatRegex.test(vm.policyInfo[i].startOCCPol)) {returnVal = false; alertVal = vm.policyInfo[i].startOCCPol}
        if (!floatRegex.test(vm.policyInfo[i].endOCCPol)) {returnVal = false; alertVal = m.policyInfo[i].endOCCPol}
        if (!floatRegex.test(vm.policyInfo[i].calculationVal)) {returnVal = false; alertVal = vm.policyInfo[i].calculationVal}
        if(returnVal==true){
            if(parseFloat(vm.policyInfo[i].startOCCPol) > parseFloat(vm.policyInfo[i].endOCCPol)){
                [vm.policyInfo[i].startOCCPol, vm.policyInfo[i].endOCCPol] = [vm.policyInfo[i].endOCCPol,vm.policyInfo[i].startOCCPol]
            }
        }
        for(var K = 0; K < vm.policyInfo[i].ltList.length; K++){
            vm.policyInfo[i].ltList[K].startLTPol = vm.policyInfo[i].ltList[K].startLTPol.toString().trim()
            vm.policyInfo[i].ltList[K].endLTPol = vm.policyInfo[i].ltList[K].endLTPol.toString().trim()
            vm.policyInfo[i].ltList[K].calculationVal = vm.policyInfo[i].ltList[K].calculationVal.toString().trim()
            if (!floatRegex.test(vm.policyInfo[i].ltList[K].startLTPol)) {returnVal = false; alertVal = vm.policyInfo[i].ltList[K].startLTPol}
            if (!floatRegex.test(vm.policyInfo[i].ltList[K].endLTPol)) {returnVal = false; alertVal = vm.policyInfo[i].ltList[K].endLTPol}
            if (!floatRegex.test(vm.policyInfo[i].ltList[K].calculationVal)) {returnVal = false; alertVal = vm.policyInfo[i].ltList[K].calculationVal}

            if(returnVal==true){
                if(parseFloat(vm.policyInfo[i].ltList[K].startLTPol) > parseFloat(vm.policyInfo[i].ltList[K].endLTPol)){
                    [vm.policyInfo[i].ltList[K].startLTPol, vm.policyInfo[i].ltList[K].endLTPol] = [vm.policyInfo[i].ltList[K].endLTPol,vm.policyInfo[i].ltList[K].startLTPol]
                }
            }
        }
    }
    if(returnVal==false){
        alert("잘못된 값이 있습니다. {" + alertVal.toString() + "}");
        return false;
    }
    else return true;
}

function delAlarm(index) {
    loadingOverlayOn()
    let url ="../delAlarm";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    formData.append("alarm_key", (vm.Alarm[index][0]).toString())
    xhr.open("POST" , url , false);
    xhr.onload = function () {
        let returnStatuscode = (xhr.responseText)
        if(returnStatuscode=="FALSE"){
            alert("알 수 없는 오류로 알람 삭제에 실패하였습니다.")
            loadingOverlayOff()
        }
        loadingOverlayOff()
        startAlarm()
    }
    xhr.send(formData);
}

function movCat(catindex){
    movCatFromCat8()
    if(catindex==1){
        document.getElementById("media_query_dashboard").style.display = 'block'
    }else{
        document.getElementById("media_query_dashboard").style.display = 'none'
    }
    if(catindex==6){
        hotelGetInUser()
    }
    if(catindex==3){
        hotelGetInRuleConfig()
        getRuleinConfigPage()
    }
    if(catindex==8){
        hotelGetInRoomConfig()
        roomConfigHotelOnchange()
    }
    if(catindex==7){
        getTimeScope()
        scopePageOn()
    }
    if(catindex==2){
        getOccAlertPercent()
        movCat2Reset()
    }
    if(catindex==9){
        vm.nowAssignSearchTable = []
        vm.nowSearched = false;
        cat9GetRoomNames()
        allAssignSearch()
    }
    vm.nowSideCat = catindex;
    vm.openedAlarm = false;
    vm.openedProfile = false;
    vm.hotelNowConfig = false;
}

var calDate = new Date()

var vm = new Vue({
  el: '#main',
  data: {
      optionsUp:[
          {text:"▲", value:true},
          {text:"▼", value:false}
      ],
      optionsPercent:[
          {text:"%", value:true},
          {text:"원", value:false}
      ],


      reloadVar:true,
      //Overall Category Block
      nowSideCat:1,
      window: false,
      LOGIN: true,
      ID:"__",
      KEY:"__",
      sideCatList:[
          //Category, Hover(0==no), menutext, icon
          [true,1,"대시보드","menu_1.png"],
          [true,0,"가격표","menu_2.png"],
          [false,2,"캘린더","menu_3.png"],
          [false,3,"룰설정","menu_4.png"],
          // [true,0,"경쟁사","menu_5.png"],
          // [false,4,"경쟁사 가격비교","menu_6.png"],
          // [false,5,"경쟁사 설정","menu_6.png"],
          [true,9,"예약생성","menu_9.png"],
          [true,0,"설정","menu_8.png"],
          [false,6,"숙소설정","menu_5.png"],
          [false,7,"성수기/기간관리","menu_7.png"],
          [false,8,"객실타입 설정","menu_6.png"],
          [false,10,"PMS 연동 설정","menu_10.png"],

      ],
      //Block - Timeline Alarm
      openedAlarm : false,
      openedProfile : false,
      Alarm: [],




      //Block - "Dashboard" - 1
      nowYear:0,
      nowMonth:0,

      dashboardTop:[
          ["","",""],
          ["","",""],
          ["","",""],
          ["","",""]
      ],//매출,숙박일,OCC,ADR 정보
      dashboardTable:[
          ["A","A","A","A","A","A"],
          ["A","A","A","A","A","A"],
          ["A","A","A","A","A","A"],
          ["A","A","A","A","A","A"],
          ["A","A","A","A","A","A"],
          ["A","A","A","A","A","A"]
      ],//element -> 년,월,매출,숙박일,점유율,ADR 순서
      dashboardOCClist:[],
      dashboardValsList:[],
      dashboardBottomLabel:[],

      //경쟁사차트트
      dashboardCompBotLabel:[],
      dashboardComp:[],



      nowDateInfoArr:[],


      //Block - "Calendar" - 2
      // calander & price
      year: calDate.getFullYear(),
      month:  calDate.getMonth(),
      calData : getCalender(calDate.getFullYear(), calDate.getMonth()),
      firstDayTsMonth : (new Date(calDate.getFullYear(), calDate.getMonth(), 1)).getDay(),
      toDay: new Date(calDate.getFullYear(), calDate.getMonth(), calDate.getDate()),
      hotel: [],
      nowHotelIndex: 0,
      room: [],
      nowRoomIndex: 0,
      priceWeekDay:50000,
      priceWeekend:70000,
      nowCalendarOn:false,

      occ_alert:[0,0,0,0,0,0],
      nowTypenameSelected:0,



      //Block - "Price Policy" - 3
      nowmodify:0,
      policyInfo:[],
      policyHotelList:[],




      //룰 추가/수정 Toggle
      nowRuleAddToggle : true,
      //Block - "Add Price Policy"
      RoomsPolicyApply:[],//HotelRoomPolicy
        //HOTEL INFO
      nowAllHotel:[],

        //세부설정
      nowRuleType: '',
      nowRuleName:'',
      nowRuleapplyRatio:0,
      nowRuleapplyRoom:[],
      nowRuleDisableCheckbox:false,
      nowApplyRoomsCategoryHotel:0,
      nowApplyRooms:[],
        //변동설정
      nowConfig:[[0,0,true,0,true,false,[[0,0,true,0,true],[0,0,true,0,true]]]],
                   // element -> [OCCstart_float,OCCend_float,OCCpriceUP_boolean, OCCPrice_float , OCCpercent_boolean, ltUse_boolean, leadtimelist()]
                   //           default element -> [0,0,true,0,true,false,[[0,0,true,0,true],[0,0,true,0,true]]]
                   // leadtimelist -> [LTstart_int,LTend_int,LTpriceUp_boolean, LTPrice_float, LTpercent_boolean]

        //적용일자설정
      nowScheduleRadio: false,
      nowStartDate:"",
      nowEndDate:"",
      nowDaysOfWeek:[false,false,false,false,false,false,false],
        //Activation
      nowRadio:0,
        //수정
      nowModifyRulePK:0,


      //경쟁사 가격비교


      //경쟁사 설정


        //숙소추가
      nowHotelKeyNum:"",
      hotelNameKor:"",
      hotelNameEng:"",
      hotelCall:"",
      hotelEmail:"",
      hotelCat:'0',
      hotelHeadName:"",
      hotelCon1:"",
      hotelCon2:"",
      hotelCon3:"",
      hotelAddr:"",

      //기간설정
      timeScope:[],
      nowTimePlus:false,
      nowPlus:[],//이름, 시작, 종료
      nowRoomNamesInScope:[],
      nowCategorySelectRoomPK:"",
      timescopeRoomDayPrice:[[false,false,false,false,false,false,false,0]],
      nowModifyScopePK:"",
      nowScopeMod:0 ,// 0-> none // 1->추가 // 2->삭제





      //객실설정
      nowHotels:[],
      nowHotel:0,
      nowRoomsname:"",
      nowRooms:[],
      nowRoomScreenStatus:0,//0 호텔선택 //1 숙소추가
        //객실추가
      nowAddHotelPK:0,

      nowRoomAdditionType:'0',
      roomTypeName:"",
            //일반객실
      pivotMan:"",
      maxMan:"",
            //도미토리
      howManyBeds:"",
        //객실추가 이어서
      howMany:"1",
      tmphowMany:"1",
      roomName:"",
        //가격
      roomDefaultPrice:"0",
      roomNums:[""],

      //요일별 객실타입 가격
      roomDayPrice:[[false,false,false,false,false,false,false,0]],


      //객실 수정
      nowmodifingRoomPK:'0',
      nowRoomnamesPK:[],

      //수기 예약
      nowAddRoomName:"",
      nowAddRoomPK:"",
      nowAddRoomStartDay:"",
      nowAddRoomPersonName:"",
      nowAddRoomPrice:0,




        //Rules And Alerts
      nowRulesList:[],


      //예약추가/수정 관련
      nowRoomToggle:0,//0 -> 추가 // 1-> 수정

      //예약관리
      nowRoomNamesList:[],//RoomPK, Room
      nowSelectTime:"0",
      nowSelectRoomNames:"DAFAULT_ROOMS_NAME",
      nowSelectRoomNameIndex:0,
      nowStartSearchDays:"",
      nowEndSearchDays:"",
      nowSearchKeyword:"",
      nowOverallSearchList:[],

      //예약생성
      nowAddHotelName:"DAFAULT_ROOMS_NAME",
      nowAddCheckin:"",
      nowAddCheckout:"",
      nowAddPersonName:"",
      nowAddChannel:"",
      nowAddPrice:"",
      nowAddROOMNAMEPK:0,

      //예약목록
      nowAssignSearchTable:[],
      //assign_pk, 판매채널, 예약자명, 객실, 체크인, 체크아웃, 금액, 생성일


      //예약수정
      nowSelectedModifiedAssignRoomName:"",
      nowSelectedModifiedAssignPK:"",

      //예약 검색 Toggle
      nowSearched:false,

      //declared attribute for connecting HotelStory
      authId:"",
      authPwd:"",
      listHotelPk:[],
      listHotelProRoom:[],
      hotelStoryData:[],
      roomConnections:[],
      connData:[],
      prevFlag:""
  }
})
vm.ID = document.getElementById("loginval_1").innerHTML.toString()
vm.KEY = document.getElementById("loginval_2").innerHTML.toString()
vm.ID = vm.ID.toString().substr(0,vm.ID.length-4)
vm.KEY = vm.KEY.toString().substr(0,vm.KEY.length-4)


let nowdate = new Date()
vm.nowYear = nowdate.getFullYear()
vm.nowMonth = nowdate.getMonth()+1





function policyOpen(){
    document.getElementById("right_policy").style.width="100%"
    document.getElementById("right_policy_top").style.width="600px"
    document.getElementById("right_policy_bot").style.width="600px"
    document.getElementById("right_policy_content").style.width="630px"
    document.getElementById("right_policy").style.visibility = "visible"
    vm.nowRuleAddToggle = true
}



function policyClose(){
    resetRulesAddingSidebar()
    document.getElementById("right_policy").style.width="0"
    document.getElementById("right_policy_top").style.width="0"
    document.getElementById("right_policy_bot").style.width="0"
    document.getElementById("right_policy_content").style.width="0"
    document.getElementById("right_policy").style.visibility = "hidden"
}


function daysOfWeekToggle(indexToDays){
    let inputVal = ((vm.nowDaysOfWeek[indexToDays])==false)
    vm.nowDaysOfWeek.splice(indexToDays, 1, inputVal);
}

function addOCCScope(){
    vm.nowConfig.push([0,0,true,0,vm.nowConfig[0][4],(vm.nowRuleType=="2"),[[0,0,true,0,vm.nowConfig[0][4]],[0,0,true,0,vm.nowConfig[0][4]]]])
}

function addLTScope(index){
    vm.nowConfig[index][6].push([0,0,true,0,vm.nowConfig[0][4]])
}

function resetRuleApplyRoom(){
    vm.RoomsPolicyApply = []
    for(let T = 0; T<vm.policyHotelList.length; T++){
        for(let l = 0; l<vm.policyHotelList[T][2].length; l++){
            vm.RoomsPolicyApply.push([false,vm.policyHotelList[T][1],vm.policyHotelList[T][2][l]])
        }
    }
    if(vm.nowRuleapplyRatio==0 && vm.nowRuleapplyRoom.length==0){
        vm.nowRuleapplyRatio = 1
        vm.nowRuleapplyRoom = []
        for(let k = 0; k<vm.room.length; k++){
            for (let P in vm.room[k]){
                vm.nowRuleapplyRoom.push([vm.hotel[k],vm.room[k][P],false])
            }
        }
    }
    if(vm.nowRuleapplyRatio==0){
        vm.nowRuleapplyRatio = 1
    }
}


function resetRulesAddingSidebar(){
      vm.nowRuleType= ''
      vm.nowRuleName=''
      vm.nowRuleapplyRatio=0
      vm.nowRuleapplyRoom=[]
      vm.nowRuleDisableCheckbox=false
      vm.nowApplyRoomsCategoryHotel=0
      vm.nowApplyRooms=[]
      vm.nowConfig=[[0,0,true,0,true,false,[[0,0,true,0,true],[0,0,true,0,true]]]]
      vm.nowScheduleRadio=false

      vm.nowDaysOfWeek=[false,false,false,false,false,false,false]
      vm.nowRadio=0
    vm.nowRuleAddToggle = true
}


function rulesToPage(){//CREATE NEW RULE SAVE버튼
    //Validation block
    let validation = true;
    for (let T = 0; T<vm.nowConfig.length; T++){
        if(isNaN(parseFloat(vm.nowConfig[T][0]))){validation = false;alert(vm.nowConfig[T][0].toString() + ":  잘못된 값이 있습니다.")}
        if(isNaN(parseFloat(vm.nowConfig[T][1]))){validation = false;alert(vm.nowConfig[T][1].toString() + ":  잘못된 값이 있습니다.")}
        if(isNaN(parseFloat(vm.nowConfig[T][3]))){validation = false;alert(vm.nowConfig[T][3].toString() + ":  잘못된 값이 있습니다.")}
        if(parseFloat(vm.nowConfig[T][1])<=parseFloat(vm.nowConfig[T][0])) {validation = false;alert("OCC 대소관계가 잘못되었습니다.")}
        if(vm.nowConfig[T][5]&& vm.nowRuleType=="2"){
             for (let M = 0; M<vm.nowConfig[T][6].length; M++){
                    if(isNaN(parseFloat(vm.nowConfig[T][6][M][0]))){validation = false;alert(vm.nowConfig[T][6][M][0].toString() + ":  잘못된 값이 있습니다.")}
                    if(isNaN(parseFloat(vm.nowConfig[T][6][M][1]))){validation = false;alert(vm.nowConfig[T][6][M][1].toString() + ":  잘못된 값이 있습니다.")}
                    if(isNaN(parseFloat(vm.nowConfig[T][6][M][3]))){validation = false;alert(vm.nowConfig[T][6][M][3].toString() + ":  잘못된 값이 있습니다.")}
                    if(parseFloat(vm.nowConfig[T][6][M][1])<=parseFloat(vm.nowConfig[T][6][M][0])){validation = false;alert(vm.nowConfig[T][0].toString() + "LT 대소관계가 잘못되었습니다.")}
             }
        }
    }
    if(!validation){
        return 0
    }
    else if(vm.nowRuleType!='1'&&vm.nowRuleType!='2'){alert("룰 기준이 설정되지 않았습니다."); return 0}
    else if(vm.nowRuleName.length==1){alert("Rule 이름이 너무 짧습니다."); return 0}
    else if(vm.nowRuleapplyRatio==1 && vm.nowRuleapplyRoom.length==0){alert("선택된 객실이 없습니다."); return 0}
    else if(vm.nowRuleDisableCheckbox && isNaN(parseInt(vm.nowApplyRoomsCategoryHotel))){alert("투숙일 기준 해당 룰 적용 날짜 제한 값이 유효하지 않습니다."); return 0}
    else if(vm.nowScheduleRadio && !comparisonTwoDays(vm.nowStartDate,vm.nowEndDate)){alert("활성화된 기간 설정이 유효하지 않습니다."); return 0}
    else if(!vm.nowDaysOfWeek[0]&&!vm.nowDaysOfWeek[1]&&!vm.nowDaysOfWeek[2]&&!vm.nowDaysOfWeek[3]&&!vm.nowDaysOfWeek[4]&&!vm.nowDaysOfWeek[5]&&!vm.nowDaysOfWeek[6]){alert("요일 설정이 되지 않았습니다."); return 0}


    loadingOverlayOn()
    let url ="../addrule";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    if(vm.nowRuleType=="1"){//점유율
        formData.append("rule_type" , "1");
    }else if(vm.nowRuleType=="2"){//점유율 + 리드타임
        formData.append("rule_type" , "2");
    }
    if(vm.nowRuleapplyRatio==0){//전체 객실 적용
        formData.append("rule_apply_room","all")
    }else{//선택 객실 적용
        let rule_apply_room_str = ""
        for(let RPA = 0; RPA<vm.RoomsPolicyApply.length; RPA++){
            if(vm.RoomsPolicyApply[RPA][0]){
                rule_apply_room_str += vm.RoomsPolicyApply[RPA][2][0]
                if(RPA!=vm.RoomsPolicyApply.length-1){
                    rule_apply_room_str += "<SPLIT>"
                }
            }
        }
        formData.append("rule_apply_room",rule_apply_room_str)
    }
    formData.append("rule_name",vm.nowRuleName)
    formData.append("rule_time_apply",vm.nowScheduleRadio.toString())
    if(vm.nowRuleDisableCheckbox) formData.append("rule_before_day",vm.nowApplyRoomsCategoryHotel)
    else formData.append("rule_before_day","-1")
    formData.append("rule_auto_apply",(!(vm.nowRadio)).toString())
    formData.append("rule_sun",vm.nowDaysOfWeek[0].toString())
    formData.append("rule_mon",vm.nowDaysOfWeek[1].toString())
    formData.append("rule_tue",vm.nowDaysOfWeek[2].toString())
    formData.append("rule_wed",vm.nowDaysOfWeek[3].toString())
    formData.append("rule_thu",vm.nowDaysOfWeek[4].toString())
    formData.append("rule_fri",vm.nowDaysOfWeek[5].toString())
    formData.append("rule_sat",vm.nowDaysOfWeek[6].toString())

    if(vm.nowScheduleRadio){//특정기간 활성화
        formData.append("rule_start_day", vm.nowStartDate.toString())
        formData.append("rule_end_day", vm.nowEndDate.toString())
    }else{
        formData.append("rule_start_day", "ALWAYS")
        formData.append("rule_end_day", "ALWAYS")
    }


    let overallOCCLTstr = ""
    for(let oct = 0; oct<vm.nowConfig.length; oct++){
        overallOCCLTstr += "<OCC>"
        for(let tmpoct = 0; tmpoct<5; tmpoct++){
            overallOCCLTstr += vm.nowConfig[oct][tmpoct].toString()
            if(tmpoct !=4) overallOCCLTstr += ","
        }
        if(vm.nowConfig[oct][5] && vm.nowRuleType=="2"){//LT가 있을 때

            for(let ttmpoct = 0; ttmpoct<vm.nowConfig[oct][6].length; ttmpoct++){
                overallOCCLTstr += "<LT>"
                for(let tmpoct = 0; tmpoct<5; tmpoct++){
                    overallOCCLTstr += vm.nowConfig[oct][6][ttmpoct][tmpoct].toString()
                    if(tmpoct !=4) overallOCCLTstr += ","
                }
            }
        }
    }
    formData.append("overall_rule_process",overallOCCLTstr)

    xhr.open("POST" , url , false);
    xhr.onload = function () {
        let returnStatuscode = (xhr.responseText)
        if(returnStatuscode=="FALSE"){
            alert("알 수 없는 오류로 룰 추가에 실패하였습니다.")
            loadingOverlayOff()
        }
        else{
            movCat(3)
        }
        policyClose()
        loadingOverlayOff()
    }
    xhr.send(formData);

}


function comparisonTwoDays(string1,string2){
    if (string1.toString().length<10){return false}
    if (string2.toString().length<10){return false}
    let str1words = string1.toString().split("-")
    let str2words = string2.toString().split("-")
    if(str1words[0]>str2words[0]) return false;
    if(str1words[1]>str2words[1]) return false;
    if(str1words[2]>str2words[2]) return false;
    return true
}


function dashboardReset(catitems){
    if(catitems==1){
        var ctx = document.getElementById('myChart');
        var myChart =new Chart(ctx, {
            type: 'bar',
            data: {
            datasets: [
                {
                label: '매출',
                data: vm.dashboardValsList,
                // Changes this dataset to become a line
                type: 'line',
                borderColor:'#ED7D31',
                backgroundColor: '#ED7D31',
                yAxisID: "y-axis-bar",
                fill:false
                },
                {
                label: 'OCC',
                data: vm.dashboardOCClist,
                backgroundColor:'#5B9BD5',
                yAxisID:"y-axis-percent"
            }
                ],
                labels: vm.dashboardBottomLabel
                },
                options: {
                        maintainAspectRatio: false,
                        scales: {
                            yAxes: [{
                                id:"y-axis-bar",
                                position:'left',
                                ticks: {
                                    beginAtZero: true
                                },
                                    callback: function(value, index, values) {
                                        return value + '만 원';
                                    }
                            },
                            {
                                id:"y-axis-percent",
                                position:'right',
                                ticks: {
                                    beginAtZero: true,
                                    max: 100,
                                    stepSize:20,
                                    callback: function(value, index, values) {
                                        return value + '%';
                                    }

                                }
                            }
                            ]
                        },
                    }
            },
        );




        // //경쟁사 차트
        //
        // vm.dashboardCompBotLabel=[]
        // vm.dashboardComp=[]
        // let TmpColors = [
        //     '#c5a6ff','#ffe3c7','#7f00b0','#70a364','#000000',
        //     '#a7aaa9','#2b504d','#094c0c','#ffd500','#ce1818'
        // ]
        // loadingOverlayOn()
        // let url ="../getComp";
        // let formData = new FormData();
        // let xhr = new XMLHttpRequest();
        // formData.append("id" , vm.ID);
        // formData.append("pw" , vm.KEY);
        // xhr.open("POST" , url , false);
        // xhr.onload = function () {
        //     let returnStatuscode = (xhr.responseText)
        //     if(returnStatuscode=="FALSE"){
        //         alert("알 수 없는 오류로 경쟁사 정보 갱신에 실패하였습니다.")
        //         loadingOverlayOff()
        //     }else{
        //         let rssSplit = returnStatuscode.split("<SPLIT>")
        //         vm.dashboardCompBotLabel = rssSplit[0].split("//")
        //         for(let t = 1; t<rssSplit.length; t++){
        //             let rssSplit2 = rssSplit[t].split("<NAME>")
        //
        //             vm.dashboardComp.push(
        //                 {
        //                     label: rssSplit2[0],
        //                     data : rssSplit2[1].split("//"),
        //                     type: 'line',
        //                     borderColor: TmpColors[t-1],
        //                     backgroundColor: TmpColors[t-1],
        //                     yAxisID: "y-axis-bar",
        //                     fill:false
        //                 }
        //             )
        //         }
        //     }
        //     loadingOverlayOff()
        // }
        // xhr.send(formData)
        //
        //
        // var ctx2 = document.getElementById('myCompChart');
        // var myChart2 =new Chart(ctx2, {
        //     type: 'bar',
        //     data: {
        //     datasets: vm.dashboardComp,
        //         labels: ["a","b","c","d","e","f","g","h","y","u"]
        //         },
        //         options: {
        //                 maintainAspectRatio: false,
        //                 legend: {
        //                     position : 'bottom',
        //                 },
        //                 scales: {
        //                     yAxes: [{
        //                         id:"y-axis-bar",
        //                         position:'left',
        //                         ticks: {
        //                             beginAtZero: true
        //                         },
        //                             callback: function(value, index, values) {
        //                                 return 'W' + value;
        //                             }
        //                     },
        //                     ]
        //                 },
        //             }
        //     },
        // );



    }


}

getDashboardCalander([])




function loadingOverlayOn(){
    document.getElementById("overlay").style.display = "block";
}

function loadingOverlayOff(){
    document.getElementById("overlay").style.display = "none";
}


function dashboardStart(){
    loadingOverlayOn()
    // Dashboard Profile DB check
    loadingOverlayOff()
}

function calendarStart(){
    loadingOverlayOn()
    // Dashboard Profile DB check
    loadingOverlayOff()
}

function ruleConfigStart(){
    loadingOverlayOn()
    // Dashboard Profile DB check
    loadingOverlayOff()
}

function dashboardStart(){
    loadingOverlayOn()
    // Dashboard Profile DB check
    loadingOverlayOff()
}

function compComparisonStart(){
    loadingOverlayOn()
    // Dashboard Profile DB check
    loadingOverlayOff()
}

function compConfigStart(){
    loadingOverlayOn()
    // Dashboard Profile DB check
    loadingOverlayOff()
}

function hotelConfigStart(){
    loadingOverlayOn()
    // Dashboard Profile DB check
    loadingOverlayOff()
}

function dayConfigStart(){
    loadingOverlayOn()
    // Dashboard Profile DB check
    loadingOverlayOff()
}

function roomConfigStart(){
    loadingOverlayOn()
    // Dashboard Profile DB check
    loadingOverlayOff()
}



function hotelAdd(){
    loadingOverlayOn()
    let url ="../addhotel";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    let checkValues = [
        ["id", vm.ID],
        ["pw", vm.KEY],
        ["hotel_kor_name" , vm.hotelNameKor],
        ["hotel_eng_name" , vm.hotelNameEng],
        ["hotel_call" , vm.hotelCall],
        ["hotel_email" , vm.hotelEmail],
        ["hotel_category" , vm.hotelCat],
        ["hotel_head_name" , vm.hotelHeadName],
        ["hotel_con1" , vm.hotelCon1],
        ["hotel_con2" , vm.hotelCon2],
        ["hotel_con3" , vm.hotelCon3],
        ["hotel_addr" , vm.hotelAddr]
    ]
    for (let cvt = 0; cvt<checkValues.length; cvt++){
        if(checkValues[cvt][1].length==0){
            alert(checkValues[cvt][0].toString() + " Field은 필수 사항입니다.")
            loadingOverlayOff()
            return 0
        }
        if(checkValues[cvt][1].indexOf("'")!=-1 || checkValues[cvt][1].indexOf('"')!=-1){
            alert("값에는 따옴표가 들어갈 수 없습니다.")
            loadingOverlayOff()
            return 0
        }
        if(checkValues[cvt][1].indexOf(",")!=-1){
            alert("값에는 쉼표가 들어갈 수 없습니다.")
            loadingOverlayOff()
            return 0
        }
    }

    for (let cvt = 0; cvt<checkValues.length; cvt++){
        formData.append(checkValues[cvt][0], checkValues[cvt][1])
    }

    xhr.open("POST" , url , false);
    xhr.onload = function () {
        let returnStatuscode = (xhr.responseText)
        if(returnStatuscode=="FALSE"){
            alert("알 수 없는 오류로 숙소 추가에 실패하였습니다.")
        }
        else{
            alert("숙소 추가에 성공하였습니다.")
            hotelGetInUser()
        }
        loadingOverlayOff()

    }
    xhr.send(formData);
}



function hotelGetInRuleConfig(){
    loadingOverlayOn()
    let url ="../gethotel";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);

    xhr.open("POST" , url , false);
    xhr.onload = function () {
        vm.policyHotelList = []
        let returnStatuscode = (xhr.responseText)
        if(returnStatuscode=="FALSE"){
            alert("알 수 없는 오류로 숙소 갱신에 실패하였습니다.")
        }
        else{
            let all_hotel = returnStatuscode.split("<SPLIT>")
            for (let i = 0 ; i < all_hotel.length; i++){
                let tmp_list = all_hotel[i].substr(0,all_hotel[i].length-1).split(",")
                let input_list = []
                for (let j = 0; j<tmp_list.length; j++){
                    input_list.push(tmp_list[j].replace("'",""))
                }
                input_list[6] = parseInt(input_list[6])
                if(input_list[2].length>0){
                    vm.policyHotelList.push([input_list[0],input_list[2], [], false])
                }
            }
            roomGetInRuleConfig()
        }

    }
    xhr.send(formData);
}
function roomGetInRuleConfig(){
    loadingOverlayOn()
    let url ="../getroom";
    for (let i = 0; i<vm.policyHotelList.length; i++){
        let formData = new FormData();
        let xhr = new XMLHttpRequest();
        formData.append("id" , vm.ID);
        formData.append("pw" , vm.KEY);
        formData.append("hotel_pk", vm.policyHotelList[i][0])

        xhr.open("POST" , url , false);
        xhr.onload = function () {
            let returnStatuscode = (xhr.responseText)
            // alert(returnStatuscode)
            if(returnStatuscode=="FALSE"){
                alert("알 수 없는 오류로 숙소목록 갱신에 실패하였습니다.")
                loadingOverlayOff()
            }
            else{
                let all_hotel = returnStatuscode.split("<SPLIT>")
                for(let ah = 0; ah<all_hotel.length; ah++){
                    if(all_hotel[ah].length>2){
                        // alert(all_hotel[ah])
                        vm.policyHotelList[i][2].push([all_hotel[ah].split(",")[0],all_hotel[ah].split(",")[3],all_hotel[ah].split(",")[8].toString().trim(),all_hotel[ah].split(",")[9].toString().trim()])
                    }

                }
                vm.policyHotelList[i][3] = true

            }
            let checkToggle = false
            for(let checkT = 0; checkT<vm.policyHotelList.length; checkT++){
                if(!(vm.policyHotelList[checkT][3])){
                    checkToggle = true
                }
            }
            if(!(checkToggle)){
                loadingOverlayOff()
            }

        }
        xhr.send(formData);
    }
}

function timeScopePlusOn(){
    vm.nowScopeMod = 1
    vm.nowTimePlus = true
    vm.nowPlus=["","",""]
    vm.timescopeRoomDayPrice = [[false,false,false,false,false,false,false,0]]
    vm.nowCategorySelectRoomPK=""
}

function timeScopePlus(){
    if(scopeCheckDays().toString()=="FALSE"){
        return 0
    }
    let checkDaysLet = scopeCheckDays()
    if((vm.nowPlus[1].toString()).length<5 || (vm.nowPlus[2].toString()).length<5){
        alert("날짜 선택이 잘못되었습니다.")
    }
    else if(vm.nowPlus[0].indexOf(",")!=-1||vm.nowPlus[0].indexOf("'")!=-1||vm.nowPlus[0].indexOf('"')!=-1){
        alert("명칭에는 쉼표나 따옴표가 들어갈 수 없습니다.")
    }
    else if(vm.nowPlus[0].length <1){
        alert("명칭의 이름이 적합하지 않습니다.")
    }
    else{
        let tmpStartStrip = vm.nowPlus[1].split("-")
        let tmpEndStrip = vm.nowPlus[2].split("-")
        let addTimeStr = vm.nowPlus[0] + "," + tmpStartStrip[0].toString() + "," + tmpStartStrip[1].toString()+ "," + tmpStartStrip[2].toString()+ "," + tmpEndStrip[0].toString()+ "," + tmpEndStrip[1].toString()+ "," + tmpEndStrip[2].toString()
        vm.nowTimePlus = false
        vm.nowPlus = []


        loadingOverlayOn()
        let url ="../addScope";
        let formData = new FormData();
        let xhr = new XMLHttpRequest();
        formData.append("id" , vm.ID);
        formData.append("pw" , vm.KEY);
        formData.append("add_scope_str", addTimeStr)
        formData.append("now_add_room_pk", vm.nowCategorySelectRoomPK.toString())
        formData.append("scope_price_sun",checkDaysLet[0]);
        formData.append("scope_price_mon",checkDaysLet[1]);
        formData.append("scope_price_tue",checkDaysLet[2]);
        formData.append("scope_price_wed",checkDaysLet[3]);
        formData.append("scope_price_thu",checkDaysLet[4]);
        formData.append("scope_price_fri",checkDaysLet[5]);
        formData.append("scope_price_sat",checkDaysLet[6]);
        xhr.open("POST" , url , false);
        xhr.onload = function () {
            let returnStatuscode = (xhr.responseText)
            if(returnStatuscode=="FALSE"){
                alert("알 수 없는 오류로 숙소목록 갱신에 실패하였습니다.")
                loadingOverlayOff()
            }
            getTimeScope()
            loadingOverlayOff()
        }
        xhr.send(formData)
    }


}

function timeScopePlusOff(){
    vm.nowTimePlus = false
    vm.nowPlus = []
}



function hotelGetInRoomConfig(){
    loadingOverlayOn()
    let url ="../gethotel";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);

    xhr.open("POST" , url , false);
    xhr.onload = function () {
        vm.nowHotels = []
        let returnStatuscode = (xhr.responseText)
        if(returnStatuscode=="FALSE"){
            alert("알 수 없는 오류로 숙소목록 갱신에 실패하였습니다.")
            loadingOverlayOff()
        }
        else{
            let all_hotel = returnStatuscode.split("<SPLIT>")
            for (let i = 0 ; i < all_hotel.length; i++){
                let tmp_list = all_hotel[i].substr(0,all_hotel[i].length-1).split(",")
                let input_list = []
                for (let j = 0; j<tmp_list.length; j++){
                    input_list.push(tmp_list[j].replace("'",""))
                }
                input_list[6] = parseInt(input_list[6])
                vm.nowHotels.push([input_list[0],input_list[2]])
            }
        }
        loadingOverlayOff()

    }
    xhr.send(formData);
}



function roomGetInRoomConfig(hotel_pk){
    loadingOverlayOn()
    let url ="../getroom";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    formData.append("hotel_pk", hotel_pk)

    xhr.open("POST" , url , false);
    xhr.onload = function () {
        vm.nowRooms = []
        vm.nowRoomScreenStatus = 0
        let returnStatuscode = (xhr.responseText)
        if(returnStatuscode=="FALSE"){
            alert("알 수 없는 오류로 숙소목록 갱신에 실패하였습니다.")
            loadingOverlayOff()
        }
        else{
            let all_hotel = returnStatuscode.split("<SPLIT>")
            for(let ah = 0; ah<all_hotel.length; ah++){
                if(all_hotel[ah].length>4){
                    vm.nowRooms.push(all_hotel[ah].split(","))
                }
            }

        }
        for (let M = 0; M<vm.nowRooms.length; M++){
            if(vm.nowRooms[M][7].split(".")[1]=="0"){
                vm.nowRooms[M][7] = vm.nowRooms[M][7].replace(".0","")
            }
        }

        loadingOverlayOff()

    }
    xhr.send(formData);
}


function roomConfigHotelOnchange(){
    // let nowHotelRoomConfig = document.getElementById("selectHotel")
    vm.nowAddHotelPK = vm.nowHotels[0][0]
    roomGetInRoomConfig(vm.nowAddHotelPK)
}

function movCatFromCat8(){
      vm.nowHotels=[]
      vm.nowHotel=0
      vm.nowRoomsname=""
      vm.nowRooms=[]
      vm.nowRoomScreenStatus=0//0 호텔선택 //1 숙소추가
      vm.roomTypeName=""
      vm.pivotMan=""
      vm.maxMan=""
      vm.howManyBeds=""
      vm.howMany="1"
      vm.roomName=""
      vm.roomDefaultPrice="0"
      vm.roomNums=[""]
      vm.roomDayPrice = [[false,false,false,false,false,false,false,0]]
}

function isInt(value) {

    var er = /^-?[0-9]+$/;

    return er.test(value);
}
function isFloat(value) {

    var er = /^-?[0-9.]+$/;

    return er.test(value);
}

function addRoom(){
    if(checkDays().toString()=="FALSE"){
        return 0
    }
    let checkDaysLet = checkDays()

    let tmpList = [
        [vm.roomTypeName,"객실타입명", false],
        [vm.roomName, "객실명", false],
        [vm.roomDefaultPrice, "기본가격", true]
    ]
    if(vm.nowRoomAdditionType=='0'){
        tmpList.push([vm.pivotMan, "기준인원", true])
        tmpList.push([vm.pivotMan, "최대인원", true])
    }else {
        tmpList.push([vm.pivotMan, "침대개수", true])
    }
    for(let T = 0; T<tmpList.length; T++){
        if(tmpList[T][0].length<1){
             alert(tmpList[T][1] + "는 필수 내용입니다.")
            return 0
        }
        if(tmpList[T][0].indexOf("'")!=-1|| tmpList[T][0].indexOf('"')!=-1|| tmpList[T][0].indexOf(",")!=-1){
            alert(tmpList[T][1] + "에는 쉼표나 따옴표가 들어가지 못 합니다.")
            return 0
        }
        if(tmpList[T][2]){//숫자만
            if(!(isInt(tmpList[T][0]))){
                alert(tmpList[T][1] + "에는 숫자만 들어가야 합니다.")
                return 0
            }
        }
    }
    let roomNumsStr = ""
    loadingOverlayOn()
    let url ="../addroom";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    formData.append("add_hotel_pk", vm.nowAddHotelPK)
    formData.append("room_category" , vm.nowRoomAdditionType);
    formData.append("room_typename" , vm.roomTypeName);
    if(vm.nowRoomAdditionType == "0"){
        formData.append("room_people" , vm.pivotMan);
        formData.append("room_max_people" , vm.maxMan);
    }else{
        formData.append("room_people" , vm.pivotMan);
        formData.append("room_max_people" , vm.pivotMan);
    }

    formData.append("room_how_many" , vm.howMany);
    formData.append("room_default_price" , vm.roomDefaultPrice);

    formData.append("roomName",vm.roomName)

    formData.append("room_names" , roomNumsStr);

    formData.append("room_price_sun",checkDaysLet[0]);
    formData.append("room_price_mon",checkDaysLet[1]);
    formData.append("room_price_tue",checkDaysLet[2]);
    formData.append("room_price_wed",checkDaysLet[3]);
    formData.append("room_price_thu",checkDaysLet[4]);
    formData.append("room_price_fri",checkDaysLet[5]);
    formData.append("room_price_sat",checkDaysLet[6]);

    xhr.open("POST" , url , false);
    xhr.onload = function () {
        let returnStatuscode = (xhr.responseText)
        // alert(returnStatuscode)
        if(returnStatuscode=="FALSE"){
            alert("알 수 없는 오류로 숙소추가에 실패하였습니다.")
            loadingOverlayOff()
        }
        else{
            movCat(8)
        }
        loadingOverlayOff()

    }
    xhr.send(formData);
}

function cancelRoomAdd(){
    let confirmTmp = confirm("작성중인 내용이 초기화됩니다. 진행하시겠습니까?")
    if(confirmTmp){
        movCat(8)
    }
}

function remRooms(){
    if(vm.roomNums.length>1){
        vm.roomNums.splice(vm.roomNums.length-1,1)
    }else{
        alert("객실이 이미 1개입니다.")
    }
    vm.howMany = vm.roomNums.length.toString()
}
function addRooms(){
    vm.roomNums.push('')
    vm.howMany = vm.roomNums.length.toString()
}

startAlarm()
function startAlarm(){
    loadingOverlayOn()
    let url ="../startAlarm";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    xhr.open("POST" , url , false);
    xhr.onload = function () {
        vm.Alarm = []
        let returnStatuscode = (xhr.responseText)
        // alert(returnStatuscode)
        if(returnStatuscode=="FALSE"){
            alert("알 수 없는 오류로 알람 갱신에 실패하였습니다.")
            loadingOverlayOff()
        }
        loadingOverlayOff()
        let gotAlarm = returnStatuscode.split("<SPLIT>")
        for(let M = 0; M<gotAlarm.length; M++){
            let splitedAlarm = gotAlarm[M].split("//")
            vm.Alarm.push([splitedAlarm[0], splitedAlarm[1], splitedAlarm[2]])
        }
    }
    xhr.send(formData);
}

function getTimeScope(){
    loadingOverlayOn()
    let url ="../getTimeScope";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    xhr.open("POST" , url , false);
    xhr.onload = function () {
        vm.timeScope = []
        let returnStatuscode = (xhr.responseText)
        let overalldata = returnStatuscode.toString().split("<SPLIT>")
        for (let T = 0; T<overalldata.length; T++){
            let tmpData = overalldata[T].split(",")
            vm.timeScope.push([tmpData[0], tmpData[1], tmpData[3], tmpData[4], tmpData[5], tmpData[6], tmpData[7], tmpData[8]])
        }

        loadingOverlayOff()
        }
    xhr.send(formData);

}

function delScope(indexToDel){
    let checkconfirm = confirm("기간을 삭제합니다. 계속 하시겠습니까?")
    if(checkconfirm){
            loadingOverlayOn()
            let url ="../deleteTimeScope";
            let formData = new FormData();
            let xhr = new XMLHttpRequest();
            formData.append("id" , vm.ID);
            formData.append("pw" , vm.KEY);
            formData.append("del_scope_pk",vm.timeScope[indexToDel][0].toString() )
            xhr.open("POST" , url , false);
            xhr.onload = function () {
                let returnStatuscode = (xhr.responseText)
                if(returnStatuscode=="FALSE") alert("알 수 없는 오류로 삭제하지 못했습니다.")
                loadingOverlayOff()
                getTimeScope()
            }
            xhr.send(formData);
    }

}

function delHotel(hotel_pk){
    let checkconfirm = confirm("해당 호텔을 삭제합니다. 계속 하시겠습니까?")
    if(checkconfirm){
            loadingOverlayOn()
            let url ="../deleteHotel";
            let formData = new FormData();
            let xhr = new XMLHttpRequest();
            formData.append("id" , vm.ID);
            formData.append("pw" , vm.KEY);
            formData.append("del_hotel_pk", hotel_pk.toString() )
            xhr.open("POST" , url , false);
            xhr.onload = function () {
                let returnStatuscode = (xhr.responseText)
                if(returnStatuscode=="FALSE") alert("알 수 없는 오류로 삭제하지 못했습니다.")
                loadingOverlayOff()
                hotelGet()
            }
            xhr.send(formData);
    }
}

function delRoom(room_pk){
    let checkconfirm = confirm("해당 객실을 삭제합니다. 계속 하시겠습니까?")
    if(checkconfirm){
            loadingOverlayOn()
            let url ="../deleteRoom";
            let formData = new FormData();
            let xhr = new XMLHttpRequest();
            formData.append("id" , vm.ID);
            formData.append("pw" , vm.KEY);
            formData.append("del_room_pk", room_pk.toString() )
            xhr.open("POST" , url , false);
            xhr.onload = function () {
                let returnStatuscode = (xhr.responseText)
                // alert(returnStatuscode)
                if(returnStatuscode=="FALSE") alert("알 수 없는 오류로 삭제하지 못했습니다.")
                loadingOverlayOff()
                roomConfigHotelOnchange()
            }
            xhr.send(formData);
    }
}

function modifyRoomAcceptInfo(room_pk){
    let checkconfirm = confirm("해당 객실을 수정합니다. 계속 하시겠습니까?")
    if(checkconfirm){
        loadingOverlayOn()
        let url ="../modifyGet";
        let formData = new FormData();
        let xhr = new XMLHttpRequest();
        formData.append("id" , vm.ID);
        formData.append("pw" , vm.KEY);
        formData.append("check_room_pk", room_pk.toString() )
        xhr.open("POST" , url , false);
        xhr.onload = function () {
            let returnStatuscode = (xhr.responseText)
            if(returnStatuscode=="FALSE"){
                alert("알 수 없는 오류로 수정하지 못했습니다.")
                loadingOverlayOff()
                return 0
            }
            // alert(returnStatuscode)
            movCatFromCat8()
            vm.nowmodifingRoomPK = '0'
            vm.nowRoomnamesPK = []
            vm.roomNums = []
            vm.roomDayPrice = []
            let overall_return = returnStatuscode.split("<RN>")
            for (let T = 0; T<overall_return.length; T++){
                if(T==0){
                    let overall_zeroindex = overall_return[0].split(",")
                    vm.nowmodifingRoomPK = overall_zeroindex[0].trim()
                    vm.nowRoomAdditionType = overall_zeroindex[2].trim()
                    vm.roomTypeName = overall_zeroindex[3].trim()
                    if(vm.nowRoomAdditionType == "0"){
                        vm.pivotMan = overall_zeroindex[4].trim()
                        vm.maxMan = overall_zeroindex[5].trim()
                    }else{
                        vm.pivotMan = overall_zeroindex[4].trim()
                        vm.howManyBeds = overall_zeroindex[4].trim()
                    }
                    vm.howMany = overall_zeroindex[6].trim()
                    vm.roomDefaultPrice = "0"

                    if(vm.roomDefaultPrice.split(".")[1]=="0"){
                        vm.roomDefaultPrice = vm.roomDefaultPrice.replace(".0","")
                    }




                    vm.roomName = overall_zeroindex[10].trim()



                    let nowlistDAYSprice = [overall_zeroindex[11].trim(),overall_zeroindex[12].trim(),
                        overall_zeroindex[13].trim(),overall_zeroindex[14].trim(),overall_zeroindex[15].trim(),
                        overall_zeroindex[16].trim(),overall_zeroindex[17].trim()]
                    // alert(nowlistDAYSprice)
                    for(let nld = 0; nld<7; nld++){
                        if(nowlistDAYSprice[nld].split(".")[1]=="0"){
                            nowlistDAYSprice[nld] = nowlistDAYSprice[nld].replace(".0","")
                        }
                    }
                    for(let nn = 0; nn<7; nn++){
                        if(nowlistDAYSprice[nn]!='-1'){
                            let pushlist = [false,false,false,false,false,false,false,nowlistDAYSprice[nn]]
                            pushlist[nn] = true
                            for(let p = nn+1; p<7; p++){
                                if(nowlistDAYSprice[p] == nowlistDAYSprice[nn]){
                                    pushlist[p] = true
                                    nowlistDAYSprice[p] = '-1'
                                }
                            }
                            nowlistDAYSprice[nn] = '-1'

                            vm.roomDayPrice.push(pushlist)
                        }
                    }


                }else{
                    let tmpoverall = overall_return[T].trim().split("<R>")
                    vm.nowRoomnamesPK.push(tmpoverall[0].trim(), tmpoverall[1].trim())
                    vm.roomNums.push(tmpoverall[1].trim())
                }
            }

            vm.nowRoomScreenStatus = 2;
            loadingOverlayOff()
        }
        xhr.send(formData)
    }
}

function modifyRoomModifyComplete(){
    if(checkDays().toString()=="FALSE"){
        return 0
    }
    let checkDaysLet = checkDays()

    let tmpList = [
        [vm.roomTypeName,"객실타입명", false],
        [vm.roomName, "객실명", false],
        [vm.roomDefaultPrice, "기본가격", true]
    ]
    if(vm.nowRoomAdditionType=='0'){
        tmpList.push([vm.pivotMan, "기준인원", true])
        tmpList.push([vm.pivotMan, "최대인원", true])
    }else {
        tmpList.push([vm.pivotMan, "침대개수", true])
    }
    for(let T = 0; T<tmpList.length; T++){
        if(tmpList[T][0].length<1){
             alert(tmpList[T][1] + "는 필수 내용입니다.")
            return 0
        }
        if(tmpList[T][0].indexOf("'")!=-1|| tmpList[T][0].indexOf('"')!=-1|| tmpList[T][0].indexOf(",")!=-1){
            alert(tmpList[T][1] + "에는 쉼표나 따옴표가 들어가지 못 합니다.")
            return 0
        }
        if(tmpList[T][2]){//숫자만
            if(!(isInt(tmpList[T][0]))){
                alert(tmpList[T][1] + "에는 숫자만 들어가야 합니다.")
                return 0
            }
        }
    }
    let roomNumsStr = ""
    loadingOverlayOn()
    let url ="../realModifyRoom";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    formData.append("del_room_pk",vm.nowmodifingRoomPK)
    formData.append("add_hotel_pk", vm.nowAddHotelPK)
    formData.append("room_category" , vm.nowRoomAdditionType);
    formData.append("room_typename" , vm.roomTypeName);
    if(vm.nowRoomAdditionType == "0"){
        formData.append("room_people" , vm.pivotMan);
        formData.append("room_max_people" , vm.maxMan);
    }else{
        formData.append("room_people" , vm.howManyBeds);
        formData.append("room_max_people" , vm.howManyBeds);
    }

    formData.append("room_how_many" , vm.howMany);
    // formData.append("room_default_price" , vm.roomDefaultPrice);

    formData.append("roomName",vm.roomName)

    formData.append("room_names" , roomNumsStr);

    formData.append("room_price_sun",checkDaysLet[0]);
    formData.append("room_price_mon",checkDaysLet[1]);
    formData.append("room_price_tue",checkDaysLet[2]);
    formData.append("room_price_wed",checkDaysLet[3]);
    formData.append("room_price_thu",checkDaysLet[4]);
    formData.append("room_price_fri",checkDaysLet[5]);
    formData.append("room_price_sat",checkDaysLet[6]);

    xhr.open("POST" , url , false);
    xhr.onload = function () {
        let returnStatuscode = (xhr.responseText)
        // alert(returnStatuscode)
        if(returnStatuscode=="FALSE"){
            alert("알 수 없는 오류로 숙소 수정에 실패하였습니다.")
            loadingOverlayOff()
        }
        else{
            movCat(8)
        }
        loadingOverlayOff()

    }
    xhr.send(formData);
}


function getRuleinConfigPage(){
    loadingOverlayOn()
    let url ="../getRulesAtRulesConfigPages";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    xhr.open("POST" , url , false);
    xhr.onload = function () {
        vm.nowRulesList = []
        let returnStatuscode = (xhr.responseText)
        // alert(returnStatuscode)
        if(returnStatuscode=="FALSE"){
            alert("알 수 없는 오류로 룰 불러오기에 실패하였습니다.")
            loadingOverlayOff()
        }
        else if(returnStatuscode.length<2){
            loadingOverlayOff()
        }
        else{
            // alert(returnStatuscode)
            let overallSplitReal = returnStatuscode.split("<OVERALL>")
            for(let osr = 0; osr<overallSplitReal.length; osr++){
                let resplit = overallSplitReal[osr].split("<STRSPLIT>")
                let resplit2 = resplit[1].split(",")
                let resplit3 = resplit[2].split(",")//Rule names
                let typestr = []
                if(resplit3=="all") typestr.push("all")
                else{
                    // alert(resplit3)
                    for(let count3 = 0; count3<resplit3.length; count3++){
                        let nowTmpRoomTypename = ""
                        for(let countP = 0; countP<vm.policyHotelList[0][2].length; countP++){
                            // alert("compare -> " + vm.policyHotelList[0][2][countP][0].toString() + "  AND  " + resplit3[count3].toString())
                            if(vm.policyHotelList[0][2][countP][0].toString()==resplit3[count3]){
                                nowTmpRoomTypename = vm.policyHotelList[0][2][countP][1]
                            }
                        }
                        if (!(nowTmpRoomTypename == "")){
                            typestr.push(nowTmpRoomTypename)
                        }else{
                            // typestr.push("ERROR")
                        }
                    }
                }
                let resplit4 = resplit[3].split(",")
                let resplit5 = resplit[4].split(",")
                let active = false
                if(resplit2[0].toString()=='1') active = true

                vm.nowRulesList.push([resplit[0].toString(),active,resplit2[1].toString(),resplit2[2].toString(),typestr,resplit4[0].toString().split("<ENTER>"),resplit4[1].toString(),resplit4[2].toString(),resplit5.toString()])
            }
            loadingOverlayOff()
            // alert(vm.nowRulesList)
        }
    }
    xhr.send(formData);
}

function delRules(delRulePK){
    let checkconfirm = confirm("해당 룰을 삭제합니다. 계속 하시겠습니까?")
    if(checkconfirm){
        loadingOverlayOn()
        let url ="../deleteRuleInConfigPage";
        let formData = new FormData();
        let xhr = new XMLHttpRequest();
        formData.append("id" , vm.ID);
        formData.append("pw" , vm.KEY);
        formData.append("del_rule_pk", delRulePK.toString())
        xhr.open("POST" , url , false);
        xhr.onload = function () {
            let returnStatuscode = (xhr.responseText)
            if(returnStatuscode=="FALSE"){
                alert("알 수 없는 오류로 숙소 수정에 실패하였습니다.")
                loadingOverlayOff()
            }
            loadingOverlayOff()
            movCat(3)
        }
        xhr.send(formData)
    }
}

function clickActiveToggleBut(clickedIndex){
    if(vm.nowRulesList[clickedIndex][1]==true){
        offRule(vm.nowRulesList[clickedIndex][0], clickedIndex)
    }else{
        onRule(vm.nowRulesList[clickedIndex][0], clickedIndex)
    }
}

function onRule(rulePK, nowRuleIndex){
    loadingOverlayOn()
    let url ="../onrule";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    formData.append("on_rule_pk", rulePK.toString())
    xhr.open("POST" , url , false);
    xhr.onload = function () {
        let returnStatuscode = (xhr.responseText)
        if(returnStatuscode=="FALSE"){
            alert("알 수 없는 오류로 활성화에 실패하였습니다.")
            movCat(3)
            loadingOverlayOff()
        }
        vm.nowRulesList[nowRuleIndex][1] = true;
        loadingOverlayOff()
    }
    xhr.send(formData)
}

function offRule(rulePK, nowRuleIndex){
    loadingOverlayOn()
    let url ="../offrule";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    formData.append("off_rule_pk", rulePK.toString())
    xhr.open("POST" , url , false);
    xhr.onload = function () {
        let returnStatuscode = (xhr.responseText)
        if(returnStatuscode=="FALSE"){
            alert("알 수 없는 오류로 비활성화에 실패하였습니다.")
            movCat(3)
            loadingOverlayOff()
        }
        vm.nowRulesList[nowRuleIndex][1] = false;
        loadingOverlayOff()
    }
    xhr.send(formData)
}

function sugiOn(assign_room){
    document.getElementById("overlayAddSugi").style.display = "block";
    vm.nowAddRoomName=assign_room[3]
    vm.nowAddRoomPK=assign_room[0].toString()
    vm.nowAddRoomStartDay=""
    vm.nowAddRoomPersonName=""
    vm.nowAddRoomPrice=0
}

function sugiOff(){
    document.getElementById("overlayAddSugi").style.display = "none";
}

function sugiAdd(){
    if(vm.nowAddRoomStartDay.toString().length<5){
        alert("날짜 설정이 잘못되었습니다.")
    }
    else if(vm.nowAddRoomPersonName.length<1){
        alert("예약인 이름을 입력해주십시오.")
    }else if(!isInt(vm.nowAddRoomPrice)){
        alert("가격을 제대로 입력해 주십시오.")
    }else if(vm.nowAddRoomName.indexOf(",")!=-1 ||vm.nowAddRoomName.indexOf("'")!=-1 ||vm.nowAddRoomName.indexOf('"')!=-1){
        alert("예약인 이름에 따옴표나 쉼표가 들어갈 수 없습니다.")
    }else{
        loadingOverlayOn()
        let splitedDays = vm.nowAddRoomStartDay.toString().split("-")
        let startyear = splitedDays[0]
        let startmonth = splitedDays[1]
        let startday = splitedDays[2]

        let url ="../addAssign";
        let formData = new FormData();
        let xhr = new XMLHttpRequest();
        formData.append("id" , vm.ID);
        formData.append("pw" , vm.KEY);
        formData.append("room_pk", vm.nowAddRoomPK)
        formData.append("start_year", startyear)
        formData.append("start_month", startmonth)
        formData.append("start_day", startday)
        formData.append("assign_person_name", vm.nowAddRoomPersonName)
        formData.append("price", vm.nowAddRoomPrice)

        xhr.open("POST" , url , false);
        xhr.onload = function () {
            let returnStatuscode = (xhr.responseText)
            if(returnStatuscode=="FALSE"){
                alert("알 수 없는 오류로 활성화에 실패하였습니다.")
                loadingOverlayOff()
                sugiOff()
            }
            else{
                alert("예약정보가 추가되었습니다.")
                loadingOverlayOff()
                sugiOff()
                movCat(9)
            }
        }
        xhr.send(formData)
    }
}

function dashboardTop(){
    loadingOverlayOn()
    let url ="../dashboardTop";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    formData.append("now_year", vm.nowYear.toString());
    formData.append("now_month", vm.nowMonth.toString());
    xhr.open("POST" , url , false);
        xhr.onload = function () {
            let returnStatuscode = (xhr.responseText)
            if(returnStatuscode=="FALSE"){
                alert("알 수 없는 오류로 대시보드 활성화에 실패하였습니다.")
                vm.dashboardTop = [
                    ["1","1","3"],//매출   //현재, 1(up)2(down) 변동폭
                    ["4","1","6"],//숙박일 //현재, 1(up)2(down) 변동폭
                    ["7","2","9"],//OCC    //현재, 1(up)2(down) 변동폭
                    ["10","2","12"]//ADR   //현재, 1(up)2(down) 변동폭
                ]
                loadingOverlayOff()
            }
            else{
                vm.dashboardTop = []
                let rsc = returnStatuscode
                rscSplited = rsc.split("//")
                for(let countlet = 0; countlet<4; countlet++){
                    vm.dashboardTop.push([rscSplited[3*countlet + 0],rscSplited[3*countlet + 1],rscSplited[3*countlet + 2]])
                }
                loadingOverlayOff()
            }
        }
        xhr.send(formData)
}

function dashboardTableChart(){
    loadingOverlayOn()
    let url ="../dashboardTableChart";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    formData.append("now_year", vm.nowYear.toString());
    formData.append("now_month", vm.nowMonth.toString());
    xhr.open("POST" , url , false);
        xhr.onload = function () {
            let returnStatuscode = (xhr.responseText)
            if(returnStatuscode=="FALSE"){
                alert("알 수 없는 오류로 대시보드 활성화에 실패하였습니다.")
                loadingOverlayOff()
            }
            else{
                vm.dashboardTable = []
                let returnStatusSplit = returnStatuscode.split("<LINE><DASHBOARDSPLIT>")
                let tableSplit = returnStatusSplit[0].split("<LINE>")
                for(let T = 0; T<6; T++){
                    vm.dashboardTable.push(tableSplit[T].split("//"))
                }
                vm.dashboardOCClist=[]
                vm.dashboardValsList=[]
                vm.dashboardBottomLabel=[]
                let chartSplit = returnStatusSplit[1].split("<LINE>")
                for(let T = 0; T<chartSplit.length; T++){
                    let tmpchartsplit = chartSplit[T].split("//")
                    vm.dashboardBottomLabel.push(tmpchartsplit[1]+"월")
                    vm.dashboardValsList.push(parseFloat(tmpchartsplit[2]))
                    vm.dashboardOCClist.push(parseFloat(tmpchartsplit[3]))
                }
                loadingOverlayOff()
            }

        }
        xhr.send(formData)
}

function dashboardTopToNextMonth(){
    if(vm.nowMonth==12){
        vm.nowYear = vm.nowYear + 1
        vm.nowMonth = 1
    }else{
        vm.nowMonth += 1
    }
    dashboardTop()
}

function dashboardTopToPrevMonth(){
    if(vm.nowMonth==1){
        vm.nowYear = vm.nowYear - 1
        vm.nowMonth = 12
    }else{
        vm.nowMonth += -1
    }
    dashboardTop()
}


function calendarFirstRoomGet(){
    loadingOverlayOn()
    let url ="../calendarGetRooms";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    xhr.open("POST" , url , false);
        xhr.onload = function () {
            let returnStatuscode = (xhr.responseText)
            if(returnStatuscode=="FALSE"){
                alert("알 수 없는 오류로 캘린더 활성화에 실패하였습니다.")
                loadingOverlayOff()
            }
            else{
                vm.hotel = []
                vm.room = []
                // alert(returnStatuscode)
                let returnStatusSplit = returnStatuscode.split("<SPLIT>")
                for(let T = 0; T<returnStatusSplit.length; T++){
                    let splitedTwo = returnStatusSplit[T].split("//")
                    // alert(splitedTwo)
                    let pushlist = []
                    for(let M = 0; M<splitedTwo.length; M++){
                        if(M==0){
                            vm.hotel.push(splitedTwo[M])
                        }else{
                            let splitedThree = splitedTwo[M].split(",")
                            pushlist.push([splitedThree[0].toString(), splitedThree[1]])
                        }
                    }
                    vm.room.push(pushlist)
                }
                loadingOverlayOff()
            }
        }
        xhr.send(formData)
}


function calendarHotelChange(hotelIndex){
    vm.nowHotelIndex = hotelIndex
    vm.nowRoomIndex = 0
    vm.nowCalendarOn = false
    if(vm.room[vm.nowHotelIndex].length!=0){
        calendarOccGet()
    }
}

function calendarRoomChange(roomIndex){
    vm.nowRoomIndex = roomIndex
    calendarOccGet()
}

function cal_next_month(){
    if(vm.month==11){
        vm.year += 1
        vm.month = 0
    }else{
        vm.month += 1
    }
    calenderChange()
}

function cal_prev_month(){
    if(vm.month==0){
        vm.year += -1
        vm.month = 11
    }else{
        vm.month += -1
    }
    calenderChange()
}

function calendarOccGet(){
    let room_pk = vm.room[vm.nowHotelIndex][vm.nowRoomIndex][0]
    vm.nowCalendarOn = true

    loadingOverlayOn()
    let url ="../calendarOCCGet";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    formData.append("now_year", vm.year)
    formData.append("now_month", vm.month + 1)
    xhr.open("POST" , url , false);
        xhr.onload = function () {
            let returnStatuscode = (xhr.responseText)
            if(returnStatuscode=="FALSE"){
                alert("알 수 없는 오류로 캘린더 활성화에 실패하였습니다.")
                vm.nowCalendarOn = false;
                loadingOverlayOff()
            }
            else{
                //  rss -> 0.0//0<SPLIT>0.0//0<SPLIT>0.0//0<SPLIT>.......<SPLIT>0.0//0
                let splitedStatuscode = returnStatuscode.split("<SPLIT>")
                let newOccPercentArray = []
                for (let ssc = 0; ssc<splitedStatuscode.length; ssc++){
                    let tmpssc = splitedStatuscode[ssc].split("//")
                    newOccPercentArray.push([tmpssc[0], tmpssc[1]])
                }

                var tmpDate = new Date()
                tmpDate = new Date(vm.nowYear, vm.nowMonth, 1)
                var firstDay = tmpDate.getDay()
                //일-0 ~ 토-6
                var nxtMonthDate =  new Date(tmpDate.getFullYear(), tmpDate.getMonth()+1, 1)
                nxtMonthDate = new Date(nxtMonthDate.getFullYear(), nxtMonthDate.getMonth(), nxtMonthDate.getDate()-1)
                var lastDay = nxtMonthDate.getDate()

                var dateInfoArr = new Array();

                vm.firstDayTsMonth = (new Date(vm.year, vm.month, 1)).getDay()
                for(var i = 1; i<=newOccPercentArray.length; i++){
                    // alert("i = " + i.toString())
                    dateInfoArr[i-1] = new dateInfo()
                    dateInfoArr[i-1].date = i;
                    dateInfoArr[i-1].day = ["일","월","화","수","목","금","토"][(firstDay+i-1)%7]
                    dateInfoArr[i-1].percentOCC = newOccPercentArray[i-1][0];
                    dateInfoArr[i-1].pay = newOccPercentArray[i-1][1];
                }
                vm.calData = dateInfoArr
                loadingOverlayOff()
            }
        }
        xhr.send(formData)
}

function getOccAlertPercent(){
    loadingOverlayOn()
    let url ="../getAlertPercent";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    xhr.open("POST" , url , false);
        xhr.onload = function () {
            let returnStatuscode = (xhr.responseText)
            if(returnStatuscode=="FALSE"){
                alert("알 수 없는 오류로 OCC 알림 비율 받아오기에 실패하였습니다.")
                loadingOverlayOff()
            }
            else{
                let rsssplit = returnStatuscode.split(",")

                vm.occ_alert = [parseFloat(rsssplit[0]),parseFloat(rsssplit[1]),parseFloat(rsssplit[2]),parseFloat(rsssplit[3]),parseFloat(rsssplit[4]),parseFloat(rsssplit[5])]
                loadingOverlayOff()
            }
        }
    xhr.send(formData)
}

function setOccAlertPercent(){
    for(let i = 0; i<6; i++){
        if(!(isFloat(vm.occ_alert[i]))){
            alert("알림 비율이 잘못되었습니다.")
            return 0;
        }
    }

    loadingOverlayOn()
    let url ="../setAlertPercent";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    formData.append("occ1", vm.occ_alert[0].toString())
    formData.append("occ2", vm.occ_alert[1].toString())
    formData.append("occ3", vm.occ_alert[2].toString())
    formData.append("occ4", vm.occ_alert[3].toString())
    formData.append("occ5", vm.occ_alert[4].toString())
    formData.append("occ6", vm.occ_alert[5].toString())
    xhr.open("POST" , url , false);
        xhr.onload = function () {
            let returnStatuscode = (xhr.responseText)
            if(returnStatuscode=="FALSE"){
                alert("알 수 없는 오류로 OCC 알림 비율 저장하기에 실패하였습니다.")
                loadingOverlayOff()
            }
            else{
                alert("알림비율을 저장하였습니다.")
                loadingOverlayOff()
            }
        }
        xhr.send(formData)
}

function hotelGetInUser(){
    loadingOverlayOn()
    let url ="../checkHotelUser";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);

    xhr.open("POST" , url , false);
    xhr.onload = function () {
        vm.hotelVals = []
        let returnStatuscode = (xhr.responseText)
        if(returnStatuscode=="FALSE"){
            alert("알 수 없는 오류로 숙소 추가에 실패하였습니다.")
            loadingOverlayOff()
        }
        else if(returnStatuscode=="NO_HOTEL"){
            alert("호텔 등록이 아직 되지 않았습니다. 호텔 등록을 시작합니다.")
            vm.nowHotelKeyNum = "NOTYET"
        }else{
            let overallSplited = returnStatuscode.split("<SPLIT>")
            vm.nowHotelKeyNum = overallSplited[0]
            vm.hotelNameKor = overallSplited[2]
            vm.hotelNameEng = overallSplited[3]
            vm.hotelCall = overallSplited[4]
            vm.hotelEmail = overallSplited[5]
            vm.hotelCat = overallSplited[6]
            vm.hotelHeadName = overallSplited[7]
            vm.hotelCon1 = overallSplited[8]
            vm.hotelCon2 = overallSplited[9]
            vm.hotelCon3 = overallSplited[10]
            vm.hotelAddr = overallSplited[11]
            // alert(overallSplited)
        }
        loadingOverlayOff()

    }
    xhr.send(formData);
}

function HotelModify(){
    loadingOverlayOn()
    let url ="../modifyhotel";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    let checkValues = [
        ["id", vm.ID],
        ["pw", vm.KEY],
        ["hotel_kor_name" , vm.hotelNameKor],
        ["hotel_eng_name" , vm.hotelNameEng],
        ["hotel_call" , vm.hotelCall],
        ["hotel_email" , vm.hotelEmail],
        ["hotel_category" , vm.hotelCat],
        ["hotel_head_name" , vm.hotelHeadName],
        ["hotel_con1" , vm.hotelCon1],
        ["hotel_con2" , vm.hotelCon2],
        ["hotel_con3" , vm.hotelCon3],
        ["hotel_addr" , vm.hotelAddr]
    ]
    for (let cvt = 0; cvt<checkValues.length; cvt++){
        if(checkValues[cvt][1].length==0){
            alert(checkValues[cvt][0].toString() + " Field은 필수 사항입니다.")
            loadingOverlayOff()
            return 0
        }
        if(checkValues[cvt][1].indexOf("'")!=-1 || checkValues[cvt][1].indexOf('"')!=-1){
            alert("값에는 따옴표가 들어갈 수 없습니다.")
            loadingOverlayOff()
            return 0
        }
        if(checkValues[cvt][1].indexOf(",")!=-1){
            alert("값에는 쉼표가 들어갈 수 없습니다.")
            loadingOverlayOff()
            return 0
        }
    }

    for (let cvt = 0; cvt<checkValues.length; cvt++){
        formData.append(checkValues[cvt][0], checkValues[cvt][1])
    }

    xhr.open("POST" , url , false);
    xhr.onload = function () {
        let returnStatuscode = (xhr.responseText)
        if(returnStatuscode=="FALSE"){
            alert("알 수 없는 오류로 숙소 수정에 실패하였습니다.")
        }
        else{
            alert("저장되었습니다.")
        }
        loadingOverlayOff()

    }
    xhr.send(formData);
}

function hotelAddOrModify(){
    if(vm.nowHotelKeyNum == "NOTYET"){
        hotelAdd()
    }else{
        HotelModify()
    }
}

function movCat2Reset(){
    calendarFirstRoomGet()
    if(vm.room[0].length > 0){
        calendarOccGet()
    }
    vm.nowTypenameSelected = 0
}

function cat9ResetStatus(){
    vm.nowRoomNamesList=[]//RoomPK, Room,
    vm.nowSelectTime="0"
    vm.nowSelectRoomNames="DAFAULT_ROOMS_NAME"
    vm.nowStartSearchDays=""
    vm.nowEndSearchDays=""
    vm.nowSearchKeyword=""
    vm.nowSelectRoomNameIndex = 0
    vm.nowOverallSearchList=[]
}

function cat9GetRoomNames(){
    loadingOverlayOn()
    let url ="../cat9GetRoomNames";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    xhr.open("POST" , url , false);
        xhr.onload = function () {
            let returnStatuscode = (xhr.responseText)
            if(returnStatuscode=="FALSE"){
                alert("호텔 등록이 안 되어 있거나 다른 오류가 발생하였습니다.")
                loadingOverlayOff()
            }
            else{
                cat9ResetStatus()
                // alert("cat9GetRoomNames -> " + returnStatuscode.toString())
                let nowSta = returnStatuscode.split("<SPLIT>")
                for (let nowStacount = 0; nowStacount<nowSta.length; nowStacount++){
                    vm.nowRoomNamesList.push(nowSta[nowStacount].split("//"))
                }
                loadingOverlayOff()
            }
        }
        xhr.send(formData)
}
function sugiAddOn(){
    vm.nowAddHotelName="DAFAULT_ROOMS_NAME"
    vm.nowAddROOMNAMEPK=0
    vm.nowAddCheckin=""
    vm.nowAddCheckout=""
    vm.nowAddPersonName=""
    vm.nowAddChannel=""
    vm.nowAddPrice=""
    vm.nowRoomToggle = 0
    document.getElementById("overlayAddSugiRecent").style.display = "block";
}

function sugiAddOFF(){
    document.getElementById("overlayAddSugiRecent").style.display = "none";
}

function sugiAddDBAdd(){
    if(vm.nowAddHotelName=="DAFAULT_ROOMS_NAME") {alert("예약할 객실을 선택해 주십시오"); return 0;}
    if(vm.nowAddCheckin.toString().length<5){alert("체크인 날짜를 입력해 주십시오"); return 0;}
    if(vm.nowAddCheckout.toString().length<5){alert("체크아웃 날짜를 입력해 주십시오"); return 0;}
    let checkinDays = new Date(vm.nowAddCheckin)
    let checkoutDays = new Date(vm.nowAddCheckout)
    let diff = (checkoutDays - checkinDays)/(24*3600*1000)
    if(diff==0) {alert("체크인 날짜와 체크아웃 날짜는 달라야 합니다."); return 0;}
    if(diff<0) { alert("체크아웃 날짜는 체크인 날짜보다 뒤에 있어야 합니다."); return 0;}

    let tmpList = [
        [vm.nowAddPersonName,"예약자", false],
        [vm.nowAddChannel, "판매채널", false],
        [vm.nowAddPrice, "금액", true],
    ]
    for(let T = 0; T<tmpList.length; T++){
        if(tmpList[T][0].length<1){
             alert(tmpList[T][1] + "는 필수 내용입니다.")
            return 0
        }
        if(tmpList[T][0].indexOf("'")!=-1|| tmpList[T][0].indexOf('"')!=-1|| tmpList[T][0].indexOf(",")!=-1){
            alert(tmpList[T][1] + "에는 쉼표나 따옴표가 들어가지 못 합니다.")
            return 0
        }
        if(tmpList[T][2]){//숫자만
            if(!(isInt(tmpList[T][0]))){
                alert(tmpList[T][1] + "에는 숫자만 들어가야 합니다.")
                return 0
            }
        }
    }

    let checkinSplit = vm.nowAddCheckin.toString().split("-")
    let checkoutSplit = vm.nowAddCheckout.toString().split("-")
    loadingOverlayOn()
    let url ="../addAssignDBDB";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    formData.append("assign_room_pk", ((vm.nowRoomNamesList[vm.nowAddROOMNAMEPK - 1])[0]).toString())
    formData.append("start_year", checkinSplit[0].toString())
    formData.append("start_month", checkinSplit[1].toString())
    formData.append("start_day", checkinSplit[2].toString())
    formData.append("price", vm.nowAddPrice.toString())
    formData.append("assign_person_name", vm.nowAddPersonName.toString())
    formData.append("end_year", checkoutSplit[0].toString())
    formData.append("end_month", checkoutSplit[1].toString())
    formData.append("end_day", checkoutSplit[2].toString())
    formData.append("channel_created", vm.nowAddChannel.toString())
    formData.append("how_day", diff.toString())


    xhr.open("POST" , url , false);
    xhr.onload = function () {
        let returnStatuscode = (xhr.responseText)
        if(returnStatuscode=="FALSE"){
            alert("알 수 없는 오류로 예약 생성에 실패하였습니다.")
            loadingOverlayOff()
            document.getElementById("overlayAddSugiRecent").style.display = "none";
        }else{
            alert("예약이 생성 되었습니다.")

            loadingOverlayOff()
            document.getElementById("overlayAddSugiRecent").style.display = "none";

            //예약 불러오기
            movCat(9)
        }


    }
    xhr.send(formData);
}


function assignSearch(){
    vm.nowAssignSearchTable = []
    vm.nowSearched = true
    let url ="../searchAssignDBDB";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    if(vm.nowStartSearchDays.toString().length<10){alert("검색 시작일 선택이 잘 못 되었습니다.");return 0;}
    if(vm.nowEndSearchDays.toString().length<10){alert("검색 종료일 선택이 잘 못 되었습니다.");return 0;}
    let nowStart = vm.nowStartSearchDays.toString().split("-")
    let nowEnd = vm.nowEndSearchDays.toString().split("-")
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    formData.append("time_pivot",vm.nowSelectTime);
    if(vm.nowSelectRoomNameIndex==0){formData.append("room_name_pk","all")}
    else{formData.append("room_name_pk",vm.nowRoomNamesList[vm.nowSelectRoomNameIndex-1][0].toString())}
    formData.append("start_year",nowStart[0]);
    formData.append("start_month",nowStart[1]);
    formData.append("start_day",nowStart[2]);
    formData.append("end_year",nowEnd[0]);
    formData.append("end_month",nowEnd[1]);
    formData.append("end_day",nowEnd[2]);
    formData.append("search_keyword",vm.nowSearchKeyword);

    loadingOverlayOn()
    xhr.open("POST" , url , false);
    xhr.onload = function () {
        let returnStatuscode = (xhr.responseText)
        if(returnStatuscode=="FALSE" || returnStatuscode.length<4){
            alert("결과가 없습니다.")
            loadingOverlayOff()
            document.getElementById("overlayAddSugiRecent").style.display = "none";
        }else{
            // alert(returnStatuscode)
            // vm.nowRoomNamesList=[]//RoomPK, RoomNamePK, Room, RoomName
            vm.nowAssignSearchTable=[]
            let searchSplit = returnStatuscode.split("<SPLIT>")
            for (let sscount = 0; sscount<searchSplit.length; sscount++){
                let tmpss = searchSplit[sscount].split("//")
                if(tmpss[6].split(".")[1]=="0"){
                    tmpss[6] = tmpss[6].replace(".0","")
                }
                vm.nowAssignSearchTable.push(tmpss)
            }
            loadingOverlayOff()
            document.getElementById("overlayAddSugiRecent").style.display = "none";
        }


    }
    xhr.send(formData);
}

function allAssignSearch(){
    vm.nowAssignSearchTable = []
    loadingOverlayOn()
    let url ="../searchALLAssignDBDB";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);

    xhr.open("POST" , url , false);
    xhr.onload = function () {
        let returnStatuscode = (xhr.responseText)
        if(returnStatuscode=="FALSE" || returnStatuscode.length<4){
            loadingOverlayOff()
            document.getElementById("overlayAddSugiRecent").style.display = "none";
        }else{
            // vm.nowRoomNamesList=[]//RoomPK, RoomNamePK, Room, RoomName
            vm.nowAssignSearchTable=[]
            let searchSplit = returnStatuscode.split("<SPLIT>")
            for (let sscount = 0; sscount<searchSplit.length; sscount++){
                let tmpss = searchSplit[sscount].split("//")
                if(tmpss[6].split(".")[1]=="0"){
                    tmpss[6] = tmpss[6].replace(".0","")
                }
                vm.nowAssignSearchTable.push(tmpss)
            }
            loadingOverlayOff()
            document.getElementById("overlayAddSugiRecent").style.display = "none";
        }


    }
    xhr.send(formData);
}


function delAssign(delAssignPK){
    let confirmTmp = confirm("해당 예약이 삭제 됩니다. 진행하시겠습니까?")
    if(!(confirmTmp)){
        return 0;
    }

    loadingOverlayOn()
    let url ="../delAssignDBDB";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    formData.append("del_assign_pk",delAssignPK.toString())
    xhr.open("POST" , url , false);
        xhr.onload = function () {
            let returnStatuscode = (xhr.responseText)
            if(returnStatuscode=="FALSE"){
                alert("오류가 발생하였습니다.")
                loadingOverlayOff()
            }
            else{
                if(vm.nowSearched){
                    assignSearch()
                }else{
                    allAssignSearch()
                }
                loadingOverlayOff()
            }
        }
        xhr.send(formData)
}


function sugiModifyOn(){
    vm.nowRoomToggle = 1
    document.getElementById("overlayAddSugiRecent").style.display = "block";

    loadingOverlayOn()
    let url ="../sugiModifiOn";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    formData.append("assign_pk" , vm.nowSelectedModifiedAssignPK);
    xhr.open("POST" , url , false);
        xhr.onload = function () {
            let returnStatuscode = (xhr.responseText)
            if(returnStatuscode=="FALSE"){
                alert("해당 예약 불러오기에 실패했습니다. ")
                loadingOverlayOff()
            }
            else{
                // alert(returnStatuscode)
                let splitedRSC = returnStatuscode.split("//")
                //년,월,일,년,월,일,이름,채널,가격
                if(splitedRSC[1].length==1) splitedRSC[1] = "0" + splitedRSC[1]
                if(splitedRSC[4].length==1) splitedRSC[4] = "0" + splitedRSC[4]
                if(splitedRSC[2].length==1) splitedRSC[2] = "0" + splitedRSC[2]
                if(splitedRSC[5].length==1) splitedRSC[5] = "0" + splitedRSC[5]
                vm.nowAddCheckin=splitedRSC[0]+"-"+splitedRSC[1]+"-"+splitedRSC[2]
                vm.nowAddCheckout=splitedRSC[3]+"-"+splitedRSC[4]+"-"+splitedRSC[5]
                document.getElementById('sugiStart').valueAsDate= new Date(splitedRSC[0]+"/"+splitedRSC[1]+"/"+splitedRSC[2]+'/00:00:00')
                document.getElementById('sugiEnd').valueAsDate= new Date(splitedRSC[3]+"/"+splitedRSC[4]+"/"+splitedRSC[5]+'/00:00:00')
                vm.nowAddPersonName=splitedRSC[6]
                vm.nowAddChannel=splitedRSC[7]
                vm.nowAddPrice=splitedRSC[8]
                if(vm.nowAddPrice.split(".")[1]=="0"){
                    vm.nowAddPrice = vm.nowAddPrice.replace(".0","")
                }
                loadingOverlayOff()
            }
        }
        xhr.send(formData)
}

function sugiModify(){
    if(vm.nowAddCheckin.toString().length<5){alert("체크인 날짜를 입력해 주십시오"); return 0;}
    if(vm.nowAddCheckout.toString().length<5){alert("체크아웃 날짜를 입력해 주십시오"); return 0;}
    let checkinDays = new Date(vm.nowAddCheckin)
    let checkoutDays = new Date(vm.nowAddCheckout)
    let diff = (checkoutDays - checkinDays)/(24*3600*1000)
    if(diff==0) {alert("체크인 날짜와 체크아웃 날짜는 달라야 합니다."); return 0;}
    if(diff<0) { alert("체크아웃 날짜는 체크인 날짜보다 뒤에 있어야 합니다."); return 0;}

    let tmpList = [
        [vm.nowAddPersonName,"예약자", false],
        [vm.nowAddChannel, "판매채널", false],
        [vm.nowAddPrice, "금액", true],
    ]
    for(let T = 0; T<tmpList.length; T++){
        if(tmpList[T][0].length<1){
             alert(tmpList[T][1] + "는 필수 내용입니다.")
            return 0
        }
        if(tmpList[T][0].indexOf("'")!=-1|| tmpList[T][0].indexOf('"')!=-1|| tmpList[T][0].indexOf(",")!=-1){
            alert(tmpList[T][1] + "에는 쉼표나 따옴표가 들어가지 못 합니다.")
            return 0
        }
        if(tmpList[T][2]){//숫자만
            if(!(isInt(tmpList[T][0]))){
                alert(tmpList[T][1] + "에는 숫자만 들어가야 합니다.")
                return 0
            }
        }
    }

    let checkinSplit = vm.nowAddCheckin.toString().split("-")
    let checkoutSplit = vm.nowAddCheckout.toString().split("-")
    loadingOverlayOn()
    let url ="../modifyAssignDBDB";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    formData.append("start_year", checkinSplit[0].toString())
    formData.append("start_month", checkinSplit[1].toString())
    formData.append("start_day", checkinSplit[2].toString())
    formData.append("price", vm.nowAddPrice.toString())
    formData.append("assign_person_name", vm.nowAddPersonName.toString())
    formData.append("end_year", checkoutSplit[0].toString())
    formData.append("end_month", checkoutSplit[1].toString())
    formData.append("end_day", checkoutSplit[2].toString())
    formData.append("channel_created", vm.nowAddChannel.toString())
    formData.append("how_day", diff.toString())
    formData.append("now_modify_assign_pk",  vm.nowSelectedModifiedAssignPK.toString())

    xhr.open("POST" , url , false);
    xhr.onload = function () {
        let returnStatuscode = (xhr.responseText)
        if(returnStatuscode=="FALSE"){
            alert("알 수 없는 오류로 예약 수정에 실패하였습니다.")
            loadingOverlayOff()
        }else{
            alert("예약이 수정 되었습니다.")

            loadingOverlayOff()
            document.getElementById("overlayAddSugiRecent").style.display = "none";
            if(vm.nowSearched){
                    assignSearch()
                }else{
                    allAssignSearch()
                }
        }
    }
    xhr.send(formData);
}


function ondeactiveHowManyRoomsNames(){
    if(isInt(vm.howMany)){
        if(parseInt(vm.howMany)>100){
            alert("수량이 너무 큽니다. 100 이하의 숫자로 설정해주세요.")
            vm.howMany = vm.tmphowMany
        }else{
            if(vm.roomNums.length<parseInt(vm.howMany)){
                while(parseInt(vm.howMany) > vm.roomNums.length){
                    vm.roomNums.push('')
                }
            }
            if(vm.roomNums.length>parseInt(vm.howMany)){
                vm.roomNums.splice(parseInt(vm.howMany), vm.roomNums.length-parseInt(vm.howMany))
            }
        }

    }else{
        alert("수량에는 항상 숫자만 들어가야 합니다.")
        vm.howMany = vm.tmphowMany
    }
}

function openRuleModifyTable(modifyRulePK){
    loadingOverlayOn()
    let url ="../modifyRuleDB";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    formData.append("modify_rule_pk",modifyRulePK.toString())
    xhr.open("POST" , url , false);
        xhr.onload = function () {
            let returnStatuscode = (xhr.responseText)
            if(returnStatuscode=="FALSE"){
                alert("오류가 발생하였습니다.")
                loadingOverlayOff()
            }
            else{
                policyOpen()
                vm.nowRuleAddToggle = false
                //값 받아오기

                let returnsplit = returnStatuscode.split("<SUPER>")
                let ruleInformation = returnsplit[0].split("//")
                vm.nowRuleType = ruleInformation[2]
                vm.nowRuleName = ruleInformation[3]
                if(ruleInformation[14]=="all")vm.nowRuleapplyRatio = 0
                else vm.nowRuleapplyRatio = 1

                if(ruleInformation[5]!="-1") {
                    vm.nowRuleDisableCheckbox= true
                    vm.nowApplyRoomsCategoryHotel = ruleInformation[5]
                }
                else {
                    vm.nowRuleDisableCheckbox= false
                    vm.nowApplyRoomsCategoryHotel = "0"
                }
                vm.nowConfig = []
                let configSplit = returnsplit[1].split("<OCC>")
                for (let cscount = 1; cscount<configSplit.length; cscount++){
                    let nowtmp = configSplit[cscount].split("<LT>")[0].split("//")
                    let nowConfigElement = [nowtmp[2], nowtmp[3], nowtmp[4]=="1",nowtmp[7], nowtmp[6]=="1", false, [] ]
                    let nowLTtmp = configSplit[cscount].split("<LT>")
                    for (let ltcount = 1; ltcount<nowLTtmp.length; ltcount++){
                        nowConfigElement[5] = true
                        let nowLTtElem = nowLTtmp[ltcount].split("//")
                        nowConfigElement[6].push([nowLTtElem[2], nowLTtElem[3], nowLTtElem[4]=="1", nowLTtElem[6], nowLTtElem[5]=="1"])
                    }
                    vm.nowConfig.push(nowConfigElement)

                }
                if(ruleInformation[17]=="ALWAYS") vm.nowScheduleRadio = false
                else vm.nowScheduleRadio = true
                vm.nowStartDate = ruleInformation[17]
                vm.nowEndDate = ruleInformation[18]
                vm.nowDaysOfWeek = [ruleInformation[7]=="1",ruleInformation[8]=="1",ruleInformation[9]=="1",ruleInformation[10]=="1",ruleInformation[11]=="1",ruleInformation[12]=="1",ruleInformation[13]=="1"]
                vm.nowRadio = parseInt(ruleInformation[6])
                vm.nowModifyRulePK = ruleInformation[0]

                if(vm.nowRuleType=='2'){
                     for(let i = 0; i<vm.nowConfig.length; i++){
                        vm.nowConfig[i][5] = true
                    }
                }


                loadingOverlayOff()
            }
        }
        xhr.send(formData)
}


function modifyRulesToPage(){//룰 변경에서 저장 버튼
    let validation = true;
    for (let T = 0; T<vm.nowConfig.length; T++){
        if(isNaN(parseFloat(vm.nowConfig[T][0])))validation = false;
        if(isNaN(parseFloat(vm.nowConfig[T][1])))validation = false;
        if(isNaN(parseFloat(vm.nowConfig[T][3])))validation = false;
        if(parseFloat(vm.nowConfig[T][1])<=parseFloat(vm.nowConfig[T][0])) validation = false;
        if(vm.nowConfig[T][5]){
             for (let M = 0; M<vm.nowConfig[T][6].length; M++){
                    if(isNaN(parseFloat(vm.nowConfig[T][6][M][0])))validation = false;
                    if(isNaN(parseFloat(vm.nowConfig[T][6][M][1])))validation = false;
                    if(isNaN(parseFloat(vm.nowConfig[T][6][M][3])))validation = false;
                    if(parseFloat(vm.nowConfig[T][6][M][1])<=parseFloat(vm.nowConfig[T][6][M][0]))validation = false;
             }
        }
    }
    if(!validation){
        alert("OCC Rule에 잘못된 값이 있습니다.")
        return 0
    }
    else if(vm.nowRuleType!='1'&&vm.nowRuleType!='2'){alert("룰 기준이 설정되지 않았습니다."); return 0}
    else if(vm.nowRuleName.length==1){alert("Rule 이름이 너무 짧습니다."); return 0}
    else if(vm.nowRuleapplyRatio==1 && vm.nowRuleapplyRoom.length==0){alert("선택된 객실이 없습니다."); return 0}
    else if(vm.nowRuleDisableCheckbox && isNaN(parseInt(vm.nowApplyRoomsCategoryHotel))){alert("투숙일 기준 해당 룰 적용 날짜 제한 값이 유효하지 않습니다."); return 0}
    else if(vm.nowScheduleRadio && !comparisonTwoDays(vm.nowStartDate,vm.nowEndDate)){alert("활성화된 기간 설정이 유효하지 않습니다."); return 0}
    else if(!vm.nowDaysOfWeek[0]&&!vm.nowDaysOfWeek[1]&&!vm.nowDaysOfWeek[2]&&!vm.nowDaysOfWeek[3]&&!vm.nowDaysOfWeek[4]&&!vm.nowDaysOfWeek[5]&&!vm.nowDaysOfWeek[6]){alert("요일 설정이 되지 않았습니다."); return 0}


    loadingOverlayOn()
    let url ="../modiRule";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    formData.append("now_modify_rule_pk", vm.nowModifyRulePK);
    if(vm.nowRuleType=="1"){//점유율
        formData.append("rule_type" , "1");
    }else if(vm.nowRuleType=="2"){//점유율 + 리드타임
        formData.append("rule_type" , "2");
    }
    if(vm.nowRuleapplyRatio==0){//전체 객실 적용
        formData.append("rule_apply_room","all")
    }else{//선택 객실 적용
        let rule_apply_room_str = ""
        for(let RPA = 0; RPA<vm.RoomsPolicyApply.length; RPA++){
            if(vm.RoomsPolicyApply[RPA][0]){
                rule_apply_room_str += vm.RoomsPolicyApply[RPA][2][0]
                if(RPA!=vm.RoomsPolicyApply.length-1){
                    rule_apply_room_str += "<SPLIT>"
                }
            }
        }
        formData.append("rule_apply_room",rule_apply_room_str)
    }
    formData.append("rule_name",vm.nowRuleName)
    formData.append("rule_time_apply",vm.nowScheduleRadio.toString())
    if(vm.nowRuleDisableCheckbox) formData.append("rule_before_day",vm.nowApplyRoomsCategoryHotel)
    else formData.append("rule_before_day","-1")
    formData.append("rule_auto_apply",(!(vm.nowRadio)).toString())
    formData.append("rule_sun",vm.nowDaysOfWeek[0].toString())
    formData.append("rule_mon",vm.nowDaysOfWeek[1].toString())
    formData.append("rule_tue",vm.nowDaysOfWeek[2].toString())
    formData.append("rule_wed",vm.nowDaysOfWeek[3].toString())
    formData.append("rule_thu",vm.nowDaysOfWeek[4].toString())
    formData.append("rule_fri",vm.nowDaysOfWeek[5].toString())
    formData.append("rule_sat",vm.nowDaysOfWeek[6].toString())

    if(vm.nowScheduleRadio){//특정기간 활성화
        formData.append("rule_start_day", vm.nowStartDate.toString())
        formData.append("rule_end_day", vm.nowEndDate.toString())
    }else{
        formData.append("rule_start_day", "ALWAYS")
        formData.append("rule_end_day", "ALWAYS")
    }


    let overallOCCLTstr = ""
    for(let oct = 0; oct<vm.nowConfig.length; oct++){
        overallOCCLTstr += "<OCC>"
        for(let tmpoct = 0; tmpoct<5; tmpoct++){
            overallOCCLTstr += vm.nowConfig[oct][tmpoct].toString()
            if(tmpoct !=4) overallOCCLTstr += ","
        }
        if(vm.nowConfig[oct][5] && vm.nowRuleType=="2"){//LT가 있을 때

            for(let ttmpoct = 0; ttmpoct<vm.nowConfig[oct][6].length; ttmpoct++){
                overallOCCLTstr += "<LT>"
                for(let tmpoct = 0; tmpoct<5; tmpoct++){
                    overallOCCLTstr += vm.nowConfig[oct][6][ttmpoct][tmpoct].toString()
                    if(tmpoct !=4) overallOCCLTstr += ","
                }
            }
        }
    }
    formData.append("overall_rule_process",overallOCCLTstr)

    xhr.open("POST" , url , false);
    xhr.onload = function () {
        let returnStatuscode = (xhr.responseText)
        if(returnStatuscode=="FALSE"){
            alert("알 수 없는 오류로 룰 수정에 실패하였습니다.")
            loadingOverlayOff()
        }
        else{
            alert("수정하였습니다")
            movCat(3)
        }
        policyClose()
        loadingOverlayOff()
    }
    xhr.send(formData);

}

function minmaxPriceSave(){
    let postString = ""
    for(let ps = 0; ps<vm.policyHotelList[0][2].length; ps++){
        if(vm.policyHotelList[0][2][ps][2]!="" && vm.policyHotelList[0][2][ps][2]!="None" && vm.policyHotelList[0][2][ps][2]!="-" && !(isFloat(vm.policyHotelList[0][2][ps][2]))){alert("값이 잘못 되었습니다. : " + vm.policyHotelList[0][2][ps][2].toString()); return 0}
        if(vm.policyHotelList[0][2][ps][2]!="" && vm.policyHotelList[0][2][ps][3]!="None" && vm.policyHotelList[0][2][ps][3]!="-" && !(isFloat(vm.policyHotelList[0][2][ps][3]))){alert("값이 잘못 되었습니다. : " + vm.policyHotelList[0][2][ps][3].toString()); return 0}
        postString += vm.policyHotelList[0][2][ps][0] + "//" + vm.policyHotelList[0][2][ps][2] + "//" + vm.policyHotelList[0][2][ps][3]
        if(ps != vm.policyHotelList[0][2].length-1){
            postString += "<SPLIT>"
        }
    }


    loadingOverlayOn()
    let url ="../saveminmaxprice";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    formData.append("minmax", postString)

    xhr.open("POST" , url , false);
    xhr.onload = function () {
        let returnStatuscode = (xhr.responseText)
        if(returnStatuscode=="FALSE"){
            alert("알 수 없는 오류로 인해 값이 수정되지 않았습니다.")
            loadingOverlayOff()
        }
        else{
            alert("저장되었습니다")
            hotelGetInRuleConfig()
            getRuleinConfigPage()
            loadingOverlayOff()
        }

    }
    xhr.send(formData);
}

function ruleConfigToLTType(catVal){
    if(catVal.toString()=="1"){
        return 0
    }else{
        for(let i = 0; i<vm.nowConfig.length; i++){
            vm.nowConfig[i][5] = true
        }
    }
}

function calendarGetTypenameOCC(roomIndex){
    vm.nowTypenameSelected = roomIndex+1
    loadingOverlayOn()
    let url ="../calendarOCCGetTypename";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    formData.append("now_year", vm.year)
    formData.append("now_month", vm.month + 1)
    formData.append("room_pk", vm.room[0][vm.nowTypenameSelected-1][0].toString())
    xhr.open("POST" , url , false);
        xhr.onload = function () {
            let returnStatuscode = (xhr.responseText)
            if(returnStatuscode=="FALSE"){
                alert("알 수 없는 오류로 캘린더 활성화에 실패하였습니다.")
                vm.nowCalendarOn = false;
                loadingOverlayOff()
            }
            else{
                //  rss -> 0.0//0<SPLIT>0.0//0<SPLIT>0.0//0<SPLIT>.......<SPLIT>0.0//0
                let splitedStatuscode = returnStatuscode.split("<SPLIT>")
                let newOccPercentArray = []
                for (let ssc = 0; ssc<splitedStatuscode.length; ssc++){
                    let tmpssc = splitedStatuscode[ssc].split("//")
                    newOccPercentArray.push([tmpssc[0], tmpssc[1]])
                }

                var tmpDate = new Date()
                tmpDate = new Date(vm.nowYear, vm.nowMonth, 1)
                var firstDay = tmpDate.getDay()
                //일-0 ~ 토-6
                var nxtMonthDate =  new Date(tmpDate.getFullYear(), tmpDate.getMonth()+1, 1)
                nxtMonthDate = new Date(nxtMonthDate.getFullYear(), nxtMonthDate.getMonth(), nxtMonthDate.getDate()-1)
                var lastDay = nxtMonthDate.getDate()

                var dateInfoArr = new Array();

                vm.firstDayTsMonth = (new Date(vm.year, vm.month, 1)).getDay()
                for(var i = 1; i<=newOccPercentArray.length; i++){
                    dateInfoArr[i-1] = new dateInfo()
                    dateInfoArr[i-1].date = i;
                    dateInfoArr[i-1].day = ["일","월","화","수","목","금","토"][(firstDay+i-1)%7]
                    dateInfoArr[i-1].percentOCC = newOccPercentArray[i-1][0];
                    dateInfoArr[i-1].pay = newOccPercentArray[i-1][1];
                }
                vm.calData = dateInfoArr

                loadingOverlayOff()
            }
        }
        xhr.send(formData)
}

function addDaysPrice(){
    vm.roomDayPrice.push([false,false,false,false,false,false,false,0])
}

function addScopeDaysPrice(){
    vm.timescopeRoomDayPrice.push([false,false,false,false,false,false,false,0])
}

function delDaysPrice(delIndex){
    if(vm.roomDayPrice.length>1){
        vm.roomDayPrice.splice(delIndex,1)
    }
    else{
        alert("요일 단위는 최소 한 개 이상 필요합니다.")
    }
}

function delScopeDaysPrice(delIndex){
    if(vm.timescopeRoomDayPrice.length>1){
        vm.timescopeRoomDayPrice.splice(delIndex,1)
    }
    else{
        alert("요일 단위는 최소 한 개 이상 필요합니다.")
    }
}

function checkDays(){
    let checklist = [-1,-1,-1,-1,-1,-1,-1]
    for(let rdp = 0; rdp<vm.roomDayPrice.length; rdp++){
        if(!(isFloat(vm.roomDayPrice[rdp][7]))){
            alert("요일별 기준 가격 정보가 잘못되었습니다. : " + vm.roomDayPrice[rdp][7].toString())
            return "FALSE"
        }else{
            if(parseFloat(vm.roomDayPrice[rdp][7])<0){
                alert("가격에 음수는 사용될 수 없습니다.")
            }
            for(let t = 0; t<7; t++){
                if(vm.roomDayPrice[rdp][t]){
                    if(checklist[t]!=-1){
                        alert(["일","월","화","수","목","금","토"][t] + "요일이 다른 구간에 겹쳐있습니다.")
                        return "FALSE"
                    }else{
                        checklist[t] = parseFloat(vm.roomDayPrice[rdp][7])
                    }
                }
            }
        }
    }
    for(let t = 0; t<7; t++){
        if(checklist[t]==-1){
            alert(["일","월","화","수","목","금","토"][t] + "요일이 선택되지 않았습니다.")
            return "FALSE"
        }
    }
    return checklist
}


function scopeCheckDays(){
    let checklist = [-1,-1,-1,-1,-1,-1,-1]
    for(let rdp = 0; rdp<vm.timescopeRoomDayPrice.length; rdp++){
        if(!(isFloat(vm.timescopeRoomDayPrice[rdp][7]))){
            alert("요일별 기준 가격 정보가 잘못되었습니다. : " + vm.timescopeRoomDayPrice[rdp][7].toString())
            return "FALSE"
        }else{
            if(parseFloat(vm.timescopeRoomDayPrice[rdp][7])<0){
                alert("가격에 음수는 사용될 수 없습니다.")
            }
            for(let t = 0; t<7; t++){
                if(vm.timescopeRoomDayPrice[rdp][t]){
                    if(checklist[t]!=-1){
                        alert(["일","월","화","수","목","금","토"][t] + "요일이 다른 구간에 겹쳐있습니다.")
                        return "FALSE"
                    }else{
                        checklist[t] = parseFloat(vm.timescopeRoomDayPrice[rdp][7])
                    }
                }
            }
        }
    }
    for(let t = 0; t<7; t++){
        if(checklist[t]==-1){
            alert(["일","월","화","수","목","금","토"][t] + "요일이 선택되지 않았습니다.")
            return "FALSE"
        }
    }
    return checklist
}

function changePercentValue(val){
    if(val.toString()=="true"){// %로 변경
        for (let i = 0; i<vm.nowConfig.length; i++){
            vm.nowConfig[i][4] = true
            for(let j = 0; j<vm.nowConfig[i][6].length; j++){
                 vm.nowConfig[i][6][j][4] = true
            }
        }
    }
    if(val.toString()=="false"){// 원 으로 변경
        for (let i = 0; i<vm.nowConfig.length; i++){
            vm.nowConfig[i][4] = false
            for(let j = 0; j<vm.nowConfig[i][6].length; j++){
                 vm.nowConfig[i][6][j][4] = false
            }
        }
    }
}


function scopePageOn(){
    loadingOverlayOn()
    let url ="../scopePageOn";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    xhr.open("POST" , url , false);
    vm.nowRoomNamesInScope=[]
    xhr.onload = function () {
        let returnStatuscode = (xhr.responseText)
        if(returnStatuscode=="FALSE"){
            alert("알 수 없는 오류로 인해 룸 정보를 받지 못했습니다.")
            loadingOverlayOff()
        }
        else{
            let rss = returnStatuscode.split("<SPLIT>")
            for(let t = 0; t<rss.length; t++){
                vm.nowRoomNamesInScope.push(rss[t].split("//"))
            }
            loadingOverlayOff()
        }

    }
    xhr.send(formData);
}


function modifScope(scopeIndex){
    loadingOverlayOn()
    let url ="../modifyTimeScope";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    formData.append("modif_scope_pk",vm.timeScope[scopeIndex][0].toString() )
    xhr.open("POST" , url , false);
    xhr.onload = function () {
        vm.nowScopeMod = 2
        vm.nowTimePlus = true
        vm.nowPlus=["","",""]
        vm.timescopeRoomDayPrice = [[false,false,false,false,false,false,false,0]]
        vm.nowCategorySelectRoomPK=""
        let returnStatuscode = (xhr.responseText)
        if(returnStatuscode=="FALSE") {
            alert("알 수 없는 오류로 수정을 시작하지 못했습니다.")
            loadingOverlayOff()
        }else{
            let overallRSSSplit = returnStatuscode.split("//")
            vm.nowModifyScopePK=overallRSSSplit[0]
            vm.nowPlus = [overallRSSSplit[1], overallRSSSplit[10], overallRSSSplit[11]]
            vm.nowCategorySelectRoomPK = overallRSSSplit[9]
            vm.timescopeRoomDayPrice = []
            let nowDaysPrice = [
                overallRSSSplit[12],overallRSSSplit[13],overallRSSSplit[14],
                overallRSSSplit[15],overallRSSSplit[16],overallRSSSplit[17],
                overallRSSSplit[18]
            ]
            for(let nn = 0; nn<7; nn++){
                        if(nowDaysPrice[nn]!='-1'){
                            let pushlist = [false,false,false,false,false,false,false,nowDaysPrice[nn]]
                            pushlist[nn] = true
                            for(let p = nn+1; p<7; p++){
                                if(nowDaysPrice[p] == nowDaysPrice[nn]){
                                    pushlist[p] = true
                                    nowDaysPrice[p] = '-1'
                                }
                            }
                            nowDaysPrice[nn] = '-1'

                            vm.timescopeRoomDayPrice.push(pushlist)
                        }
                    }
                loadingOverlayOff()
        }
    }
    xhr.send(formData);
}

function logout(){
    location.href = location.href;
}

function timeScopeModifyAdd(){
    if(scopeCheckDays().toString()=="FALSE"){
        return 0
    }
    let checkDaysLet = scopeCheckDays()
    if((vm.nowPlus[1].toString()).length<5 || (vm.nowPlus[2].toString()).length<5){
        alert("날짜 선택이 잘못되었습니다.")
    }
    else if(vm.nowPlus[0].indexOf(",")!=-1||vm.nowPlus[0].indexOf("'")!=-1||vm.nowPlus[0].indexOf('"')!=-1){
        alert("명칭에는 쉼표나 따옴표가 들어갈 수 없습니다.")
    }
    else if(vm.nowPlus[0].length <1){
        alert("명칭의 이름이 적합하지 않습니다.")
    }
    else{
        let tmpStartStrip = vm.nowPlus[1].split("-")
        let tmpEndStrip = vm.nowPlus[2].split("-")
        let addTimeStr = vm.nowPlus[0] + "," + tmpStartStrip[0].toString() + "," + tmpStartStrip[1].toString()+ "," + tmpStartStrip[2].toString()+ "," + tmpEndStrip[0].toString()+ "," + tmpEndStrip[1].toString()+ "," + tmpEndStrip[2].toString()
        vm.nowTimePlus = false
        vm.nowPlus = []


        loadingOverlayOn()
        let url ="../modifScopeDB";
        let formData = new FormData();
        let xhr = new XMLHttpRequest();
        formData.append("id" , vm.ID);
        formData.append("pw" , vm.KEY);
        formData.append("add_scope_str", addTimeStr)
        formData.append("modif_scope_pk",vm.nowModifyScopePK.toString())
        formData.append("now_add_room_pk", vm.nowCategorySelectRoomPK.toString())
        formData.append("scope_price_sun",checkDaysLet[0]);
        formData.append("scope_price_mon",checkDaysLet[1]);
        formData.append("scope_price_tue",checkDaysLet[2]);
        formData.append("scope_price_wed",checkDaysLet[3]);
        formData.append("scope_price_thu",checkDaysLet[4]);
        formData.append("scope_price_fri",checkDaysLet[5]);
        formData.append("scope_price_sat",checkDaysLet[6]);
        xhr.open("POST" , url , false);
        xhr.onload = function () {
            let returnStatuscode = (xhr.responseText)
            if(returnStatuscode=="FALSE"){
                alert("알 수 없는 오류로 숙소목록 수정에 실패하였습니다.")
                loadingOverlayOff()
            }
            getTimeScope()
            loadingOverlayOff()
        }
        xhr.send(formData)
    }
}

function calenderChange(){
    if(vm.nowTypenameSelected==0){
        calendarOccGet()
    }
    else{
        calendarGetTypenameOCC(vm.nowTypenameSelected - 1)
    }

}

dashboardTableChart()
dashboardTop()
dashboardReset(1)
calendarFirstRoomGet()

function connectHotelStory(){
    vm.listHotelPk =[];
    vm.listHotelProRoom = [];
    vm.hotelStoryData = [];
    vm.roomConnections=[];

    //get hotel pk
    if(vm.authId == null || vm.authId == "" || vm.authPwd == null || vm.authPwd == ""){
        alert("Please enter Authentication ID or Authentication Password");
        return 0;
    }
    
    let url = "../gethotel";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    xhr.open("POST" , url , false);
    xhr.onload = function (){
        let returnStatuscode = (xhr.responseText)
        if(returnStatuscode=="FAILED"){
            alert("Unknow Error! Failed to retreived hotel data!");
            return 0;
        }
        else{
            let all_hotel = returnStatuscode.split("<SPLIT>")
            for (let i = 0 ; i < all_hotel.length; i++){
                let hotelData =  all_hotel[i].split(",");
                vm.listHotelPk.push(hotelData[0]);
            }
        }
    }
    xhr.send(formData);
    
    //get hotel prop room information
    url ="../getroom";
    for(let i = 0; i < vm.listHotelPk.length; i++){        
        formData.append("hotel_pk", vm.listHotelPk[i])
        xhr.open("POST" , url , false);
        xhr.onload = function () {
            let returnStatuscode = (xhr.responseText)
            if(returnStatuscode=="FAILED"){
                alert("Unknow Error! Failed to retreived room data!");
                return 0;
            }else{
                vm.listHotelProRoom = returnStatuscode.split("<SPLIT>");
            }
        }
        xhr.send(formData);
    }

    //get hotel story room information    
    url = "../getHotelStoryData";
    formData.append("authId" , vm.authId);
    formData.append("authPwd" , vm.authPwd);
    xhr.open("POST" , url , false);
    xhr.onload = function (){
        let returnStatuscode = (xhr.responseText)
        //if(returnStatuscode =="FAILED"){
        if(returnStatuscode.includes("status")){        
            alert(returnStatuscode);
        }else{
            let data = JSON.parse(returnStatuscode.replaceAll('\'','\"'));
            for(var i = 0; i< data.length; i++ ){
                let authKey = data[i]["authKey"];
                let hotelName = data[i]["hotelName"];
                for(var j = 0; j< data[i]["listRoom"].length; j++ )
                {
                    data[i]["listRoom"][j]["authKey"] = authKey;
                    data[i]["listRoom"][j]["hotelName"] = hotelName;
                    vm.hotelStoryData.push(data[i]["listRoom"][j]);
                }
            }
        }
    }
    xhr.send(formData);    
    
    getRoomConnections(formData);
    showDataOnUI();
}

function getRoomConnections(formData){
    let url = "../getRoomConnections";
    let xhr = new XMLHttpRequest();
    xhr.open("POST" , url , false);
    xhr.onload = function (){
        let returnStatuscode = (xhr.responseText)
        if(returnStatuscode.includes("FAILED")){        
            alert(returnStatuscode);
        }else{
            vm.roomConnections = returnStatuscode.split("<SPLIT>");
        }
    }
    xhr.send(formData);
}

function showDataOnUI(){
    document.getElementById("dataTableDiv").style.display = "block";
    //show data for hotel prop rooms
    var hotelPropTable = document.getElementById("hotelPropDataTable");    
    hotelPropTable.tBodies[0].innerHTML ="";
    var tableContent = "";
    for(var i = 0; i< vm.listHotelProRoom.length; i++){
        //console.log(vm.listHotelProRoom[i]);
        var tableData = vm.listHotelProRoom[i].split(",");
        tableContent =  tableContent    + "<tr>"
                                        + "<td style=\"border: 1px black solid;\">" + tableData[0] +"</td>"
                                        + "<td style=\"border: 1px black solid;\">" + tableData[1] +"</td>"
                                        + "<td style=\"border: 1px black solid;\">" + tableData[2] +"</td>"
                                        + "<td style=\"border: 1px black solid;\">" + tableData[3] +"</td>"
                                        + "<td style=\"border: 1px black solid;\">" + tableData[4] +"</td>"
                                        + "<td style=\"border: 1px black solid;\">" + tableData[5] +"</td>"
                                        + "<td style=\"border: 1px black solid;\">" + tableData[6] +"</td>"
                                        + "<td style=\"border: 1px black solid;\">" + tableData[7] +"</td>"
                                        + "<td style=\"border: 1px black solid;\">" + tableData[8] +"</td>"
                                        + "<td style=\"border: 1px black solid;\">" + tableData[9] +"</td>"
                                        + "<td style=\"border: 1px black solid;\">" + tableData[10] +"</td>"
                                        + "<td style=\"border: 1px black solid;\">" + tableData[11] +"</td>"
                                        + "<td style=\"border: 1px black solid;\">" + tableData[12] +"</td>"
                                        + "<td style=\"border: 1px black solid;\">" + tableData[13] +"</td>"
                                        + "<td style=\"border: 1px black solid;\">" + tableData[14] +"</td>"
                                        + "<td style=\"border: 1px black solid;\">" + tableData[15] +"</td>"
                                        + "<td style=\"border: 1px black solid;\">" + tableData[16] +"</td>"
                                        + "<td style=\"border: 1px black solid;\">" + tableData[17] +"</td>"
                                        + "</tr>";
    }
    hotelPropTable.tBodies[0].innerHTML = tableContent;

    //show data for hotel Story rooms
    var hotelStoryTable = document.getElementById("hotelStoryDataTable"); 
    tableContent = "";
    for(var i = 0; i < vm.hotelStoryData.length; i++){
        tableContent =  tableContent    + "<tr>"
                                        + "<td style=\"border: 1px black solid;\">" + vm.hotelStoryData[i]["authKey"] +"</td>"
                                        + "<td style=\"border: 1px black solid;\">" + vm.hotelStoryData[i]["hotelName"] +"</td>"
                                        + "<td style=\"border: 1px black solid;\">" + vm.hotelStoryData[i]["room_key"] +"</td>"
                                        + "<td style=\"border: 1px black solid;\">" + vm.hotelStoryData[i]["room_name"] +"</td>"
                                        + "<td style=\"border: 1px black solid;\">" + vm.hotelStoryData[i]["room_count"] +"</td>"
                                        + "<td style=\"border: 1px black solid;\">" + vm.hotelStoryData[i]["room_size"] +"</td>"
                                        + "<td style=\"border: 1px black solid;\">" + vm.hotelStoryData[i]["min_persons"] +"</td>"
                                        + "<td style=\"border: 1px black solid;\">" + vm.hotelStoryData[i]["max_persons"] +"</td>"
                                        + "<td style=\"border: 1px black solid;\">" + vm.hotelStoryData[i]["room_rate"] +"</td>"
                                        + "</tr>";
    }
    hotelStoryTable.tBodies[0].innerHTML = tableContent;    

    //Show Data for room connections
    showConnectionTable();
}

function showConnectionTable(){
    var roomConnTbl = document.getElementById("connTable");
    roomConnTbl.tBodies[0].innerHTML = "";
    let tableContent = "";
    for(var i = 0; i< vm.roomConnections.length; i++){
        var tableData = vm.roomConnections[i].split(", ");
        tableContent    += `<tr>
                        <td style="border: 1px black solid;">
                        <select id="row${i+1}roomPK" onchange="handleUpdate(${i+1})">
                        ${generateHotelPropOpt(tableData[1])}
                        </select>▼
                        </td>
                        <td style="border: 1px black solid;">
                        <select id="row${i+1}roomKey" onchange="handleUpdate(${i+1})">
                        ${generateHotelStoryOpt(tableData[2])}
                        </select>▼
                        </td>
                        <td style="border: 1px black solid;"><input type="checkbox" id="row${i+1}checkBox"  onchange="handleRemove(${i+1})"></td>
                        <td style="border: 1px black solid; display: none"> <input type="text" id="row${i+1}flag" value=""></td>
                        <td style="border: 1px black solid; display: none"> <input type="text" id="row${i+1}recordId" value="${tableData[0]}"></td>
                        </tr>`
    }
    roomConnTbl.tBodies[0].innerHTML = tableContent;
}

function handleUpdate(rowIndex){
    document.getElementById("row"+rowIndex+"flag").value = "U";
    vm.prevFlag = "U";
}

function handleRemove(rowIndex){
    var flag = "";
    removeChecked = document.getElementById("row"+rowIndex+"checkBox").checked;
    if(removeChecked){
        flag = "D";
    }else{
        flag = vm.prevFlag;
    }     
    document.getElementById("row"+rowIndex+"flag").value = flag;
}

function generateHotelStoryOpt(roomKey){
    var optContent = "<option value= \"\"> Room Key </option>";
    for(var i = 0; i < vm.hotelStoryData.length; i++){
        if(roomKey == vm.hotelStoryData[i]["room_key"]){
            optContent += "<option value=\""+ vm.hotelStoryData[i]["room_key"] + "-" + vm.hotelStoryData[i]["authKey"] +"-"+ vm.hotelStoryData[i]["hotelName"] + "\" selected>"+ vm.hotelStoryData[i]["room_key"] +"-"+ vm.hotelStoryData[i]["hotelName"] +"</option>"
        }else{
            optContent += "<option value=\""+ vm.hotelStoryData[i]["room_key"] + "-" + vm.hotelStoryData[i]["authKey"] +"-"+ vm.hotelStoryData[i]["hotelName"] + "\">"+ vm.hotelStoryData[i]["room_key"] +"-"+ vm.hotelStoryData[i]["hotelName"] +"</option>"
        }
    }
    return optContent;
}

function generateHotelPropOpt(roomPk){
    var optContent = "<option value= \"\"> Room PK </option>";
    for(var i = 0; i < vm.listHotelProRoom.length; i++){
        var data = vm.listHotelProRoom[i].split(", ");
        if(roomPk == data[0]){
            optContent += "<option value=\""+ data[0] +"\" selected>"+ data[0] +"</option>"
        }else{
            optContent += "<option value=\""+ data[0] +"\" > "+ data[0] +"</option>"
        }
    }
    return optContent;
}

function addTableRow(){    
    var table = document.getElementById("connTable");
    var rowCount = table.rows.length;

    var row = table.insertRow(rowCount);
    row.setAttribute("id", rowCount);
    var cell0 = row.insertCell(0);
    var hotelPropSelect = document.createElement("select");
    var roomPkOpt = document.createElement("option");
    roomPkOpt.selected = "selected";
    roomPkOpt.setAttribute("value", "");
    roomPkOpt.text = "Room PK";
    hotelPropSelect.appendChild(roomPkOpt);
    for(var i = 0; i < vm.listHotelProRoom.length; i++){
        var tableData = vm.listHotelProRoom[i].split(",");
        var option = document.createElement("option");
        option.setAttribute("value", tableData[0]);
        option.text = tableData[0];
        hotelPropSelect.appendChild(option);
    }
    hotelPropSelect.setAttribute("id", "row"+ rowCount + "roomPK");
    cell0.appendChild(hotelPropSelect);
    cell0.innerHTML += "▼" ;
    cell0.setAttribute("style", "border: 1px black solid;");
    

    var cell1 = row.insertCell(1);
    var hotelStorySelect = document.createElement("select");
    var roomKeyOpt = document.createElement("option");
    roomKeyOpt.selected = "selected";
    roomKeyOpt.setAttribute("value", "");
    roomKeyOpt.text = "Room Key";
    hotelStorySelect.appendChild(roomKeyOpt);
    for(var i = 0; i < vm.hotelStoryData.length; i++){
        var roomKey = vm.hotelStoryData[i]["room_key"];
        var hotelName = vm.hotelStoryData[i]["hotelName"];
        var authKey = vm.hotelStoryData[i]["authKey"];
        var option = document.createElement("option");
        option.setAttribute("value", roomKey + "-"+ authKey +"-" +hotelName);
        option.text = roomKey + "-" + hotelName;
        hotelStorySelect.appendChild(option);
    }
    hotelStorySelect.setAttribute("id", "row"+ rowCount + "roomKey");
    cell1.appendChild(hotelStorySelect);
    cell1.innerHTML += "▼" ;
    cell1.setAttribute("style", "border: 1px black solid;");

    var cell2 = row.insertCell(2);
    var removeCheckBx = document.createElement("INPUT");
    removeCheckBx.setAttribute("type", "checkbox");
    removeCheckBx.disabled = true;
    cell2.appendChild(removeCheckBx);
    cell2.setAttribute("style", "border: 1px black solid;");

    var cell3 = row.insertCell(3);
    var flagField = document.createElement("INPUT");
    flagField.setAttribute("id", "row"+ rowCount+ "flag");
    flagField.setAttribute("value", "I");
    cell3.appendChild(flagField);
    cell3.setAttribute("style", "display: none");

    var cell4 = row.insertCell(4);
    var idField = document.createElement("INPUT");
    idField.setAttribute("id", "row"+ rowCount+ "recordId");
    idField.setAttribute("value", "new");
    cell4.appendChild(idField);
    cell4.setAttribute("style", "display: none");
}


function saveMapping(){
    vm.connData = [];
    var table = document.getElementById("connTable");
    var rowCount = table.rows.length;
    if(rowCount == 1){
        alert("No data to save!!!");
        return 0;
    }
    for(var i = 1; i < rowCount; i++){
        var roomPk = document.getElementById("row"+ i + "roomPK").value;
        var roomKey = document.getElementById("row"+ i + "roomKey").value;
        var flag = document.getElementById("row"+ i + "flag").value;
        var id = document.getElementById("row"+ i + "recordId").value;
        //console.log(`roomPk: ${roomPk}, roomKey: ${roomKey}, flag: ${flag}, id: ${id}`);
        if(roomPk != null && roomPk != "" && roomKey != null && roomKey != ""){
            var jsonData = {
                "roomPk": roomPk,
                "roomKey": roomKey,
                "flag": flag,
                "id": id,
                "authId": vm.authId
            };
            vm.connData.push(jsonData);
        }else{
            alert("Invalid data at row: "+ i);
            return 0;
        }
    }    
    console.log(JSON.stringify(vm.connData));
    let url = "../saveMapping";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    formData.append("connData" , JSON.stringify(vm.connData));
    xhr.open("POST" , url , false);
    xhr.onload = function (){
        let returnStatuscode = (xhr.responseText);
        if(returnStatuscode =="FAILED"){        
            alert("Save connection data to DB failed");
        }else{
            console.log(returnStatuscode);  
            reloadConnTbl()            
        }
    }
    xhr.send(formData);    
}

function reloadConnTbl(){
    let formData = new FormData();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    formData.append("authId" , vm.authId);
    getRoomConnections(formData);
    showConnectionTable();
}

/*
function saveMapping(){
    vm.connData = [];
    var table = document.getElementById("connTable");
    var rowCount = table.rows.length;
    if(rowCount == 1){
        alert("No data to save!!!");
        return 0;
    }

    for(var i = 1; i < rowCount; i++){
        var roomPkItem = document.getElementById("row"+ i + "roomPK");
        var roomPk = roomPkItem.value;
        var roomKeyItem = document.getElementById("row"+ i + "roomKey");
        var roomKey = roomKeyItem.value;
        //console.log("vm.hotelStoryData: "+JSON.stringify(vm.hotelStoryData[i]) );
        if(roomPk != null && roomPk != "" && roomKey != null && roomKey != ""){
            //console.log("Room PK: "+ vm.listHotelProRoom[roomPk].split(",")[0]);
            //console.log("Hotel Story data: "+JSON.stringify(vm.hotelStoryData[roomKey]) );
            vm.hotelStoryData[roomKey]["roomPk"] = vm.listHotelProRoom[roomPk].split(",")[0];
            vm.connData.push(vm.hotelStoryData[roomKey]);
        }else{
            alert("Invalid data at row: "+ i);
        }
    }
    //console.log(JSON.stringify(vm.connData));
    
    //post data to server
    let url = "../saveMapping";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("id" , vm.ID);
    formData.append("pw" , vm.KEY);
    formData.append("connData" , JSON.stringify(vm.connData));
    xhr.open("POST" , url , false);
    xhr.onload = function (){
        let returnStatuscode = (xhr.responseText);
        if(returnStatuscode =="FAILED"){        
            alert("Save connection data to DB failed");
        }else{
            console.log(returnStatuscode);  
            let resultStr = "";
            let data = JSON.parse(returnStatuscode.replaceAll('\'','\"'));    
            for(var i = 0; i< data.length; i++ ){
                if("FAILED" == data[i]["result"]){
                    resultStr += "Data insert for room PK: " + data[i]["insertData"]["roomPk"] + " and room key: "+ data[i]["insertData"]["room_key"] +" of " + data[i]["insertData"]["hotelName"] + " failed!\n"
                }
            }
        }
    }
    xhr.send(formData);
}
*/