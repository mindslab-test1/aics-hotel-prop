var loginalert= document.getElementById("message_to_alert").innerHTML.toString()
if(loginalert.length>2){alert(loginalert)}
var loginvm = new Vue({
  el: '#login_main_ul',
  data: {
      nowAddAccount:false,

      email_id:"",
      password:"",
      password_recheck:"",
      person_name:"",
      person_phone:"",
      hotel_name:"",

      //이용약관, 개인정보 보호정책, 마케팅 수신동의
      check_box1:false,
      check_box2:false,
      check_box3:false,
      //checkbox expand
      expand_box1:false,
      expand_box2:false,
      expand_box3:false,

      text_box1:[],
      text_box2:[],
      text_box3:[],

  }
})

function start_text_box(){
    loginvm.text_box1 = relLawToList(1)
    loginvm.text_box2 = relLawToList(2)
    loginvm.text_box3 = relLawToList(3)
}



function addAccountToggle(){
    loginvm.nowAddAccount = true;
}

function resetAccountTable(){
    loginvm.nowAddAccount=false
    loginvm.email_id=""
    loginvm.password=""
    loginvm.password_recheck=""
    loginvm.person_name=""
    loginvm.person_phone=""
    loginvm.hotel_name=""
    loginvm.check_box1=false
    loginvm.check_box2=false
    loginvm.check_box3=false
    loginvm.expand_box1=false
    loginvm.expand_box2=false
    loginvm.expand_box3=false
}

function cancelAccountadd(){
    let confirmTmp = confirm("작성중인 내용이 초기화됩니다. 진행하시겠습니까?")
    if(confirmTmp){
        resetAccountTable()
    }
}

function accountAddBtn(){
    if(!(loginvm.check_box1)){alert("이용약관에 동의해 주십시오."); return 0;}
    if(!(loginvm.check_box2)){alert("개인정보 보호정책에 동의해 주십시오."); return 0;}
    let regExp = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;
    if (loginvm.email_id.match(regExp) != null) {}
    else{alert("이메일의 형식이 적당하지 않습니다."); return 0;}
    if(loginvm.email_id.length>58){alert("이메일 주소가 너무 깁니다."); return 0;}
    if(loginvm.password.length>18){alert("비밀번호가 너무 깁니다."); return 0;}
    if(loginvm.person_name.length>8){alert("관리자 이름이 너무 깁니다."); return 0;}
    if(loginvm.person_phone.length>28){alert("관리자 연락처가 너무 깁니다."); return 0;}
    if(loginvm.hotel_name.length>28){alert("숙소 이름이 너무 깁니다."); return 0;}
    if(loginvm.password!=loginvm.password_recheck){alert("비밀번호 확인이 잘못 되었습니다."); return 0;}

    let tmpList = [
        [loginvm.email_id,"이메일 주소", false],
        [loginvm.password, "비밀번호", false],
        [loginvm.person_name, "관리자 이름", false],
        [loginvm.person_phone, "관리자 연락처", false],
        [loginvm.hotel_name, "숙소 이름", false],
    ]
    for(let T = 0; T<tmpList.length; T++){
        if(tmpList[T][0].length<1){
             alert(tmpList[T][1] + "는 필수 내용입니다.")
            return 0
        }
        if(tmpList[T][0].indexOf("'")!=-1|| tmpList[T][0].indexOf('"')!=-1|| tmpList[T][0].indexOf(",")!=-1){
            alert(tmpList[T][1] + "에는 쉼표나 따옴표가 들어가지 못 합니다.")
            return 0
        }
        if(tmpList[T][2]){//숫자만
            if(!(isInt(tmpList[T][0]))){
                alert(tmpList[T][1] + "에는 숫자만 들어가야 합니다.")
                return 0
            }
        }
    }



    let url ="../addUserName";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("user_id",loginvm.email_id)
    formData.append("user_pw",loginvm.password)
    formData.append("user_name",loginvm.person_name)
    formData.append("tel_num",loginvm.person_phone)
    formData.append("hotel_name",loginvm.hotel_name)
    if(loginvm.check_box3==true)formData.append("marketing","True")
    else{formData.append("marketing","False")}
    xhr.open("POST" , url , false);
    xhr.onload = function () {
        let returnStatuscode = (xhr.responseText)
        if(returnStatuscode=="FALSE"){
            alert("알 수 없는 오류로 계정 가입 신청에 실패하였습니다.")
        }else if( returnStatuscode=="VAL NOT"){
            alert("중복된 아이디입니다. 다른 아이디로 시도해 주십시오.")
        }
        else{
            alert("가입 신청이 완료되었습니다. 호텔프롭 측의 확인 뒤에 사용하실 수 있습니다.")
            resetAccountTable()
        }

    }
    xhr.send(formData);
}

start_text_box()