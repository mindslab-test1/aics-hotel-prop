var loginID= document.getElementById("login_admin_id").innerHTML.toString()
document.getElementById("login_admin_id").innerHTML = "none"
var loginPW= document.getElementById("login_admin_pw").innerHTML.toString()
document.getElementById("login_admin_pw").innerHTML = "none"

var adminvm = new Vue({
      el: '#admin_ul',
      data: {
            overall_table : [],
      }
    })



function adminlogin(){
    let url ="../adminLogin";
    let formData = new FormData();
    let xhr = new XMLHttpRequest();
    formData.append("user_id",loginID)
    formData.append("user_pw",loginPW)
    xhr.open("POST" , url , false);
    xhr.onload = function () {
        let returnStatuscode = (xhr.responseText)
        if(returnStatuscode=="FALSE"){
            alert("알 수 없는 오류로 어드민 로그인에 실패하였습니다.")
        }
        else{
            adminvm.overall_table= []
            let rss = returnStatuscode.split("<SPLIT>")
            for(let t = 0; t<rss.length; t++){
                adminvm.overall_table.push(rss[t].split("//"))
            }
        }
    }
    xhr.send(formData);
}

adminlogin()

function delUser(username, userpk){
    let tmpconfirm = confirm(username + " 계정을 삭제합니다. 계속 하시겠습니까?")
    if(tmpconfirm){
        let url ="../adminDelUser";
        let formData = new FormData();
        let xhr = new XMLHttpRequest();
        formData.append("user_id",loginID)
        formData.append("user_pw",loginPW)
        formData.append("del_user_pk", userpk.toString())
        xhr.open("POST" , url , false);
        xhr.onload = function () {
            let returnStatuscode = (xhr.responseText)
            if(returnStatuscode=="FALSE"){
                alert("알 수 없는 오류로 계정 삭제에 실패하였습니다.")
            }
            else{
                alert(username + " 계정을 삭제하였습니다.")
                adminlogin()
            }
        }
        xhr.send(formData);
    }
}

function checkUser(username, userpk){
    let tmpconfirm = confirm(username + " 계정의 가입신청을 동의합니다. 계속 하시겠습니까?")
    if(tmpconfirm){
        let url ="../adminCheckUser";
        let formData = new FormData();
        let xhr = new XMLHttpRequest();
        formData.append("user_id",loginID)
        formData.append("user_pw",loginPW)
        formData.append("check_user_pk", userpk.toString())
        xhr.open("POST" , url , false);
        xhr.onload = function () {
            let returnStatuscode = (xhr.responseText)
            if(returnStatuscode=="FALSE"){
                alert("알 수 없는 오류로 가입 동의에 실패하였습니다.")
            }
            else{
                alert(username + " 계정의 가입을 확인하였습니다.")
                adminlogin()
            }
        }
        xhr.send(formData);
    }
}

